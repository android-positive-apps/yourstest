package com.positiveapps.pelecardsdk;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.positiveapps.pelecardsdk.fragments.BaseFragment;
import com.positiveapps.pelecardsdk.fragments.DealDetailsFragment;
import com.positiveapps.pelecardsdk.fragments.PaymentFragment;
import com.positiveapps.pelecardsdk.object.PelecardConfiguration;
import com.positiveapps.pelecardsdk.object.PelecardDeal;
import com.positiveapps.pelecardsdk.util.AppUtil;
import com.positiveapps.pelecardsdk.util.FragmentsUtil;
import com.positiveapps.pelecardsdk.util.ToastUtil;

import android.support.v4.app.FragmentActivity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceActivity.Header;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class PelecardContainerActivity extends FragmentActivity {

	public static final String EXTRA_CONFIGURATION = "ExtraConfiguration";
	public static final String EXTRA_DEAL_BUSINESS = "ExtravDealBusiness";
	
	public static Context appContext;
	public static BaseFragment currentFragment;
	public static TextView fragmentName;
	public static PelecardConfiguration pelecardConfiguration;
	public static PelecardDeal pelecardDeal;
	public static Handler payHendler;
	public static boolean payStart;
	public static boolean payDone;
	public static TextView timerTxt;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeNoTitleActivity(this);
		setContentView(R.layout.activity_container);
		fragmentName = (TextView)findViewById(R.id.fragment_name);
		timerTxt = (TextView)findViewById(R.id.timer_txt);
		appContext = this;
		checkForExtra(getIntent().getExtras());
		payStart = false;
		payDone = false;
	}

	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (!currentFragment.onBackPressed()){
			super.onBackPressed();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		pelecardConfiguration = null;
		pelecardDeal = null;
		
	}
	/**
	 * 
	 */
	private void checkForExtra(Bundle extras) {
		if (extras == null){
			/*setPelecardConfiguration();
			openSetDealDetailsFragment();*/
			ToastUtil.toster(appContext, getString(R.string.invalid_config), false);
			finish();
			return;
		}
		
		pelecardConfiguration = (PelecardConfiguration) extras.getSerializable(EXTRA_CONFIGURATION);
		pelecardDeal = (PelecardDeal)extras.getSerializable(EXTRA_DEAL_BUSINESS);
		if (pelecardConfiguration == null){
			ToastUtil.toster(appContext, getString(R.string.invalid_config), false);
			finish();
		}else if(!pelecardConfiguration.isValidConfig()){
			ToastUtil.toster(appContext, getString(R.string.invalid_config), false);
			finish();
		}
		
		startPayTimeOut ();
		if (pelecardDeal == null){
			openSetDealDetailsFragment();
			return;
		}
		
		openPaymentFragment();
	}

	
	boolean stop;
	int counter;
	private void startPayTimeOut() {
		stop = false;
		counter = 0;
		
		final long milles = pelecardConfiguration.getPayTimeout();
		Log.e("timerlog", "milii = " + milles);
		if (milles<=0){
			setTimer(null);
			return;
		}
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while (!stop){
					counter++;
					long temp = counter*1000;
					Log.e("timerlog", "milii = " + temp);
					if (temp>=milles){
						if (!payDone&&!payStart&&!stop){
							stop = true;
							ToastUtil.toster(PelecardContainerActivity.this,"מועד השלמת הפעולה תם. אנא התחל שוב", true);
							finish();
							if (payHendler != null){
								Message message = new Message();
								message.what = 2;
								PelecardContainerActivity.payHendler.dispatchMessage(message);
							}
						}else{
							stop = true;
						}
					}else{
						if (payDone||payStart){
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									try {
										setTimer(null);
									} catch (Exception e) {}
								}
							});
							stop = true;
							return;
						}
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								try {
									setTimer(getTimeByMillis(milles - (counter*1000)));
								} catch (Exception e) {}
							}
						});
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
		
	}

	private String getTimeByMillis (long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return new SimpleDateFormat("mm:ss").format(calendar.getTime());
	}

	private void openPaymentFragment() {
		// TODO Auto-generated method stub
		FragmentsUtil.addFragment(getSupportFragmentManager(),
				PaymentFragment.newInstance(), R.id.main_container);
	}

	
	private void openSetDealDetailsFragment() {
		// TODO Auto-generated method stub
		FragmentsUtil.addFragment(getSupportFragmentManager(),
				DealDetailsFragment.newInstance(), R.id.main_container);
	}

	public static void setFragmentName (String name){
		fragmentName.setText(name);
	}
	public static void setTimer (String time){
		if (time != null&&!time.equals("")){
			timerTxt.setVisibility(View.VISIBLE);
			timerTxt.setText(time);
		}else{
			timerTxt.setVisibility(View.GONE);
		}
		
	}
	
	
	/*private void setPelecardConfiguration() {
		pelecardConfiguration = new PelecardConfiguration();
		pelecardConfiguration.setUserName("ami142Fe");
		pelecardConfiguration.setPassword("kf457Qwc");
		pelecardConfiguration.setTerminalNumber("0962210");
	}*/
	
	
	public static void setPelecardDeal (String businessName,String productName,String productCost) {
		pelecardDeal = new PelecardDeal();
		pelecardDeal.setBusinessName(businessName);
		pelecardDeal.setProductName(productName);
		pelecardDeal.setProductCost(Float.parseFloat(productCost));
		pelecardDeal.setBusinessImageUrl("https://c2.staticflickr.com/6/5243/5229878018_1919f50a83_z.jpg");
		
	}

	public static void setPayHandler (Handler handler){
		payHendler = handler;
	}
	
}
