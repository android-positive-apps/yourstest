/**
 * 
 */
package com.positiveapps.pelecardsdk.fragments;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.positiveapps.pelecardsdk.PelecardContainerActivity;
import com.positiveapps.pelecardsdk.R;
import com.positiveapps.pelecardsdk.api.PelecardApiCallback;
import com.positiveapps.pelecardsdk.api.PelecardApiManager;
import com.positiveapps.pelecardsdk.api.PelecardResponseObject;
import com.positiveapps.pelecardsdk.dialogs.PelecardDialogCallback;
import com.positiveapps.pelecardsdk.dialogs.PelecardDialogManager;
import com.positiveapps.pelecardsdk.object.CreditCard;
import com.positiveapps.pelecardsdk.object.PelecardDeal;
import com.positiveapps.pelecardsdk.object.PelecardResult;
import com.positiveapps.pelecardsdk.preference.PelecardPreferences;
import com.positiveapps.pelecardsdk.util.AppUtil;
import com.positiveapps.pelecardsdk.util.FragmentsUtil;
import com.positiveapps.pelecardsdk.util.ToastUtil;

/**
 * @author natiapplications
 *
 */
public class PaymentFragment  extends BaseFragment implements OnClickListener {
	
	public static final int PAY_SUCCESS = 1;
	public static final int PAY_FAILE = 2;
	
	private EditText creditCardNumberEt;
	private EditText creditCardValidityEt;
	private EditText cvvEt;
	private EditText userIdEt;
	private EditText userEmailEt;
	private ImageView creditCardLogo;
	private ImageView aboutBtn;
	private Button payBtn;
	
	private ImageView businessImage;
	private TextView businessNameTv;
	private TextView productCostTv;
	
	private int lastCreditCardType;
	public static  TypedArray creditCardLogos;
	private long selectedValidityDate;
	public static PelecardResult pelecardResult;
	private PopupWindow popupMessage;
	private ImageView cvvPopUp;
	private String cost;

	public PaymentFragment() {

	}

	public static PaymentFragment newInstance() {
		PaymentFragment instance = new PaymentFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_payment, container,
				false);
		creditCardLogos = getResources().obtainTypedArray(R.array.cards_logo);
		lastCreditCardType = creditCardLogos.length()-1;
		PelecardContainerActivity.setFragmentName(getString(R.string.payment_screen_name));
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		
		businessImage = (ImageView) view.findViewById(R.id.business_image);
		businessNameTv = (TextView) view.findViewById(R.id.business_name);
		productCostTv = (TextView) view.findViewById(R.id.cost);
		
		PelecardDeal deal = PelecardContainerActivity.pelecardDeal;
		AppUtil.loadImageInto(getActivity(), deal.getBusinessImageUrl(),
				businessImage, R.drawable.btn_selector, R.drawable.btn_selector, 100, 100, null);
		businessNameTv.setText(deal.getBusinessName());
		productCostTv.setText(getString(R.string.sam) +" "+ String.valueOf((int)deal.getProductCost()));
		cost= String.valueOf((int)deal.getProductCost());
		creditCardNumberEt = (EditText) view.findViewById(R.id.credit_number_et);
		creditCardValidityEt = (EditText) view.findViewById(R.id.validity_et);
		cvvEt = (EditText) view.findViewById(R.id.ccv_et);
		userIdEt = (EditText) view.findViewById(R.id.id_et);
		userEmailEt = (EditText) view.findViewById(R.id.email_et);
		creditCardLogo = (ImageView) view.findViewById(R.id.company_logo);
		aboutBtn = (ImageView) view.findViewById(R.id.about);
		payBtn = (Button) view.findViewById(R.id.pay_btn);
		cvvPopUp = (ImageView)view.findViewById(R.id.cvv_pop);
		
		creditCardNumberEt.addTextChangedListener(new OnCreditCardNumberEditTextChange());
		creditCardValidityEt.setKeyListener(null);
		OnCreditCardValidityClickListener onCreditCardValidityClickListener = new OnCreditCardValidityClickListener();
		creditCardValidityEt.setOnClickListener(onCreditCardValidityClickListener);
		creditCardValidityEt.setOnFocusChangeListener(onCreditCardValidityClickListener);
		
		payBtn.setOnClickListener(this);
		aboutBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Log.e("cvvpopuplog", "onAboutClickListener");
				//showCvvPopup();
				if (cvvPopUp.getVisibility() == View.VISIBLE){
					cvvPopUp.setVisibility(View.GONE);
				}else{
					cvvPopUp.setVisibility(View.VISIBLE);
				}
			}
		});
		
		// test === 
		/*creditCardNumberEt.setText("5326100320033040");
		cvvEt.setText("767");
		lastCreditCardType = CreditCard.CREDIT_CARD_TYPE_MASTERCARD;
		creditCardLogo.setImageDrawable(creditCardLogos.getDrawable(lastCreditCardType));*/
		
		if (!PelecardPreferences.getInstans(getActivity()).getPelecardToken().equals("")){
			PelecardPreferences preferences = PelecardPreferences.getInstans(getActivity());
			
			CreditCard savedCreditCard = new CreditCard(preferences.getCreditCardNumber(), 
					preferences.getCreditCardValidity(), preferences.getCreditCardCvv());
			savedCreditCard.setCreditCardType(preferences.getCreditCardType());
			showChooseCreditCardDialog(savedCreditCard);
			Log.e("creditCardLog","creditCardNumber = " + savedCreditCard.getCardNumber() + " safe = " + savedCreditCard.getCreditCardSafeNumber());
		}
		
	}

	
	/**
	 * @param savedCreditCard
	 */
	private void showChooseCreditCardDialog(final CreditCard savedCreditCard) {
		
		PelecardDialogManager.showChooseCreditCardDialog(this, savedCreditCard, new PelecardDialogCallback(){
			
			@Override
			protected void onDialogButtonPressed(int buttonID,
					DialogFragment dialog, Object Extra) {
				super.onDialogButtonPressed(buttonID, dialog, Extra);
				performPament(savedCreditCard.getCardNumber(), 
						savedCreditCard.getCardValidity(), savedCreditCard.getCardCVV(), "",PelecardPreferences.getInstans(getActivity()).getPelecardToken());
			}
		});
	}

	
	@Override
	public void onClick(View v) {
		
			if (AppUtil.validation(creditCardNumberEt,creditCardValidityEt,cvvEt,userIdEt)){
				String cardNumber = creditCardNumberEt.getText().toString();
				String cardValidity = creditCardValidityEt.getText().toString();
				String cvv = cvvEt.getText().toString();
				String userid = userIdEt.getText().toString();
				if (/*CreditCard.isValidCardNumber(lastCreditCardType, cardNumber)*/true){
					if (CreditCard.isValidValidity(selectedValidityDate)){
						if(cvv.length() == 3){
							if (userid.length() >=9){

								/**
								 * niv changes
								 */
								CreditCard creditCard = new CreditCard(cardNumber, cardValidity, cvv);
								if (cardNumber.equals("4580458045804580")) {
//									FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
//											FinishDealFragment.newInstance(creditCard), R.id.main_container);
//									getActivity().finish();
									Intent data = new Intent();
									data.putExtra("test","test");
									data.putExtra("cardNumber",cardNumber);
									data.putExtra("cardValidity",cardValidity);
									data.putExtra("cvv",cvv);
									data.putExtra("productCost",cost);
									data.putExtra("businessName", businessNameTv.getText().toString());
									data.putExtra("userId", userIdEt.getText().toString());
									data.putExtra("userEmail",userEmailEt.getText().toString());


									getActivity().setResult(Activity.RESULT_OK,data);
									getActivity().finish();
								} else {
									ToastUtil.toster(getActivity(), getString(R.string.general_pelecard_error), true);

								}
								/**
								 * niv changes
								 */


//								performPament(cardNumber, cardValidity, cvv, userid,"");
							}else{
								ToastUtil.toster(getActivity(), getString(R.string.invalid_id), false);
							}
						}else{
						}
					}else{
						ToastUtil.toster(getActivity(), getString(R.string.invalid_credit_card_validity), false);
					}
				}else{
					ToastUtil.toster(getActivity(), getString(R.string.invalid_credit_card_number), false);
				}
			}else{
				ToastUtil.toster(getActivity(), getString(R.string.validation_fields_message), false);
			}
		
		
		
	}
	
	/**
	 * 
	 */
	private void showCvvPopup() {
		Log.e("cvvpopuplog", "show ! is null ? " +( popupMessage == null));
		if (popupMessage != null && popupMessage.isShowing()){
			Log.e("cvvpopuplog", "is showing ? " +( popupMessage.isShowing()));
			popupMessage.dismiss();
			return;
		}else{
			int xOfset = 0;
		    int yOfset = 0;
			//if (popupMessage == null){
			    View popupView = null;
				getActivity().getLayoutInflater().inflate(R.layout.cvv_popup, null); 
				popupMessage = new PopupWindow(popupView, 300,100);  
				popupMessage.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_selector));
				Log.e("cvvpopuplog", "create and show" );
			    popupMessage.showAsDropDown(aboutBtn);
			//}else{
				//Log.e("cvvpopuplog", "show created popup");
			    //popupMessage.showAsDropDown(aboutBtn);
			//}
		}
		
	   
	}

	private void performPament (final String creditCardNumber,final String creditCardValidity,final String cvv,String userID,final String token){
		showProgressDialog();
		PelecardContainerActivity.payStart = true;
		PelecardApiManager.getInstance(getActivity()).performPayment
		(PelecardContainerActivity.pelecardDeal, PelecardContainerActivity.pelecardConfiguration, 
				creditCardNumber,creditCardValidity,token,cvv,userID, new PelecardApiCallback(){
			
			@Override
			public PelecardResponseObject parseResponse(
					String toParse) {
				return new PelecardResponseObject(toParse,PelecardResponseObject.RESPONSE_TYPE_PEAYMENT);
			}
			
			@SuppressLint("NewApi")
			@Override
			public void onDataRecived(PelecardResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				PelecardContainerActivity.payDone = true;
				Log.e("responseLog", "response = " + response.toString());
				if (!response.isHasError()){
					final CreditCard creditCard = new CreditCard(creditCardNumber, creditCardValidity, cvv);
					creditCard.setCreditCardType(lastCreditCardType);
					pelecardResult = new PelecardResult(PelecardContainerActivity.pelecardDeal,
							response.getConfirmationNumber(), response.getConfirmationBy(),response.getCreditCardNumber());
					pelecardResult.setVoucherId(response.getVoucherID());
					if (token != null && !token.isEmpty()){
						dismisProgressDialog();
						pelecardResult.setDealTocken(token);
						FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
								FinishDealFragment.newInstance(creditCard), R.id.main_container);
					}else{
						PelecardApiManager.getInstance(getActivity()).convertToToken
						(PelecardContainerActivity.pelecardConfiguration, creditCardNumber, creditCardValidity, new PelecardApiCallback(){
							@Override
							public PelecardResponseObject parseResponse(String toParse) {
								return new PelecardResponseObject(toParse,PelecardResponseObject.RESPONSE_TYPE_CONVERT_TO_TOKEN);
							}
							
							@Override
							public void onDataRecived(PelecardResponseObject response,
									boolean isHasError, String erroDescription) {
								super.onDataRecived(response, isHasError, erroDescription);
								dismisProgressDialog();
								if (!isHasError){
									pelecardResult.setDealTocken(response.getContent());
								}
								FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
										FinishDealFragment.newInstance(creditCard), R.id.main_container);
							}
									
							@Override
							public void onError() {
								super.onError();
								dismisProgressDialog();
								ToastUtil.toster(getActivity(), getString(R.string.general_network_error), false);
								FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
										FinishDealFragment.newInstance(creditCard), R.id.main_container);
							}
											
							@Override
							public void networkUnavailable(String message) {
								super.networkUnavailable(message);
								dismisProgressDialog();
								FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
										FinishDealFragment.newInstance(creditCard), R.id.main_container);
							}	
						});
					}
					
				}else{
					dismisProgressDialog();
					pelecardResult = new PelecardResult(PelecardContainerActivity.pelecardDeal,
							response.getConfirmationNumber(), response.getConfirmationBy(),response.getCreditCardNumber());
					pelecardResult.setErrorCode(response.getErrorCode());
					ToastUtil.toster(getActivity(), erroDescription, false);
					payFaile();
				}
			}
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getActivity(), getString(R.string.general_network_error), false);
				dismisProgressDialog();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
			
		});
	}
	
	
	private void payFaile(){
		
		if (PelecardContainerActivity.payHendler != null){
			Message message = new Message();
			message.what = 1;
			message.arg1 = PaymentFragment.PAY_FAILE;
			message.obj = PaymentFragment.pelecardResult;
			PelecardContainerActivity.payHendler.dispatchMessage(message);
		}
		
		Intent data = new Intent();
		Bundle extra = new Bundle();
		extra.putSerializable(FinishDealFragment.EXTRA_PELECARD_RESULT, PaymentFragment.pelecardResult);
		extra.putInt(FinishDealFragment.PAY_RESULT, PaymentFragment.PAY_FAILE);
		data.putExtras(extra);
		getActivity().setResult(Activity.RESULT_CANCELED,data);
		getActivity().finish();
	}
	
	
	class OnCreditCardNumberEditTextChange implements TextWatcher {

		
		@Override
		public void afterTextChanged(Editable s) {}
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {}
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String currentText = creditCardNumberEt.getText().toString();
			lastCreditCardType = compustCreditCardType(currentText);
			Log.e("lastCreditCardTypeLog" , "LastCreditCardType = " + lastCreditCardType);
			creditCardLogo.setImageDrawable(creditCardLogos.getDrawable(lastCreditCardType));
		}
		
		/**
		 * @param currentText
		 * @return
		 */
		private int compustCreditCardType(String currentText) {
			if (currentText.length() == 0){
				return creditCardLogos.length() -1;
			}
			
			Log.i("creditcardlogtesting", "------------------ " + "start check credid card type" + " ----------------------");
			
			int result = creditCardLogos.length()-1;
			
			
			if (currentText.startsWith("4")){
				Log.e("creditcardlogtesting", "viza");
				result = CreditCard.CREDIT_CARD_TYPE_VISA;
			}else if(currentText.startsWith("5")){
				Log.e("creditcardlogtesting", "mastercard");
				result = CreditCard.CREDIT_CARD_TYPE_MASTERCARD;
			}else if(currentText.startsWith("3")){
				Log.e("creditcardlogtesting", "american express");
				result = CreditCard.CREDIT_CARD_TYPE_AMERICAN_EXPRESS;
			}
			
			
			if(currentText.startsWith("37")||currentText.startsWith("34")){
				result =  CreditCard.CREDIT_CARD_TYPE_AMERICAN_EXPRESS;
				Log.e("creditcardlogtesting", "american express");
			}else if(currentText.startsWith("30")||currentText.startsWith("36")||currentText.startsWith("38")){
				result = CreditCard.CREDIT_CARD_TYPE_DINERS;
				Log.e("creditcardlogtesting", "diners");
			}else if (currentText.startsWith("50") &&currentText.startsWith("55")){
				Log.e("creditcardlogtesting", "mastercard");
				result = CreditCard.CREDIT_CARD_TYPE_MASTERCARD;
			} 
			
			
			if (currentText.startsWith("2014")||currentText.startsWith("2149")){
				result = CreditCard.CREDIT_CARD_TYPE_ENROUTE;
				Log.e("creditcardlogtesting", "enroute");
			}else if (currentText.startsWith("3088") ||currentText.startsWith("3096") ||
					currentText.startsWith("3112") || currentText.startsWith("3158")||
					currentText.startsWith("3337") || currentText.startsWith("3096")){
				Log.e("creditcardlogtesting", "jcb");
				result = CreditCard.CREDIT_CARD_TYPE_JCB;
			}
			
			if (currentText.length() >= 8 ){
				int number = Integer.parseInt(currentText.substring(0,8));
				if (number >= 60110000 && number <=60119999){
					Log.e("creditcardlogtesting", "discover");
					result = CreditCard.CREDIT_CARD_TYPE_DISCOVER;
				}else if (number >= 65000000 && number <=65999999){
					Log.e("creditcardlogtesting", "discover");
					result = CreditCard.CREDIT_CARD_TYPE_DISCOVER;
				}else if (number >= 62212600 && number <=62292599){
					result = CreditCard.CREDIT_CARD_TYPE_DISCOVER;
					Log.e("creditcardlogtesting", "discover");
				}else if (number >= 35280000 && number <=35899999){
					result = CreditCard.CREDIT_CARD_TYPE_JCB;
					Log.e("creditcardlogtesting", "jcb");
				}else if(currentText.length() <= 9){
					if (CreditCard.isValidCardNumber(CreditCard.CREDIT_CARD_TYPE_ISRACARD, currentText)){
						result = CreditCard.CREDIT_CARD_TYPE_ISRACARD;
						Log.e("creditcardlogtesting", "isracard");
					}
				}
			}
		
			Log.d("creditcardlogtesting", "------------------ " + result + " ----------------------");
			
			return result;
		}
		
	}
	
	class OnCreditCardValidityClickListener implements OnClickListener,OnFocusChangeListener {

		
		
		@Override
		public void onClick(View arg0) {
			// TODO Auto-generated method stub
			showDatePickerDialog();
		}
		
		private void showDatePickerDialog() {

			DatePickerDialog datePickerDialog = createDialogWithoutDateField();
			datePickerDialog.show();
		}
		
		private DatePickerDialog createDialogWithoutDateField() {

			Calendar calendar = Calendar.getInstance();
			DatePickerDialog dpd = new DatePickerDialog(getActivity(),
					new DatePickerDialog.OnDateSetListener() {

						@Override
						public void onDateSet(DatePicker view, int year,
								int monthOfYear, int dayOfMonth) {
							creditCardValidityEt.setText(getDateAsString(
									dayOfMonth, monthOfYear + 1, year));
							selectedValidityDate = getDateInMilli(dayOfMonth,
									monthOfYear + 1, year);
							cvvEt.requestFocus();
						}
					}, calendar.get(Calendar.YEAR),
					calendar.get(Calendar.MONTH),
					calendar.get(Calendar.DAY_OF_MONTH));

			try {
				java.lang.reflect.Field[] datePickerDialogFields = dpd
						.getClass().getDeclaredFields();
				for (java.lang.reflect.Field datePickerDialogField : datePickerDialogFields) {
					if (datePickerDialogField.getName().equals("mDatePicker")) {
						datePickerDialogField.setAccessible(true);
						DatePicker datePicker = (DatePicker) datePickerDialogField
								.get(dpd);
						java.lang.reflect.Field[] datePickerFields = datePickerDialogField
								.getType().getDeclaredFields();
						for (java.lang.reflect.Field datePickerField : datePickerFields) {
							if ("mDaySpinner".equals(datePickerField.getName())) {
								datePickerField.setAccessible(true);
								Object dayPicker = new Object();
								dayPicker = datePickerField.get(datePicker);
								((View) dayPicker).setVisibility(View.GONE);
							}
						}
					}

				}
			} catch (Exception ex) {
			}
			return dpd;

		}
		
		private String getDateAsString (int day, int month, int yeers){
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, yeers);
			calendar.set(Calendar.MONTH, month-1);
			calendar.set(Calendar.DAY_OF_MONTH, day);
			
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/yy");
			return simpleDateFormat.format(calendar.getTime());
		}
		
		private long getDateInMilli(int day, int month, int yeers){
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.YEAR, yeers);
			calendar.set(Calendar.MONTH, month-1);
			calendar.set(Calendar.DAY_OF_MONTH, day);
			
			return calendar.getTimeInMillis();
		}

		
		@Override
		public void onFocusChange(View arg0, boolean arg1) {
			// TODO Auto-generated method stub
			if (arg1){
				showDatePickerDialog();
			}
		}
		
	}


}