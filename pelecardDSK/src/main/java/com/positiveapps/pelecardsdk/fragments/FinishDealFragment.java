/**
 * 
 */
package com.positiveapps.pelecardsdk.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import com.positiveapps.pelecardsdk.PelecardContainerActivity;
import com.positiveapps.pelecardsdk.R;
import com.positiveapps.pelecardsdk.api.PelecardApiCallback;
import com.positiveapps.pelecardsdk.api.PelecardApiManager;
import com.positiveapps.pelecardsdk.api.PelecardResponseObject;
import com.positiveapps.pelecardsdk.object.CreditCard;
import com.positiveapps.pelecardsdk.object.PelecardDeal;
import com.positiveapps.pelecardsdk.preference.PelecardPreferences;
import com.positiveapps.pelecardsdk.util.AppUtil;
import com.positiveapps.pelecardsdk.util.ToastUtil;

/**
 * @author natiapplications
 *
 */
public class FinishDealFragment extends BaseFragment implements OnClickListener,OnCheckedChangeListener {
	
	public static final String PAY_RESULT = "PayResult";
	public static final String EXTRA_PELECARD_RESULT = "ExtraPelecardResult";
	private static final String EXTRA_CREDIT_CARD = "ExtraCreditCard";
	
	
	
	private ImageView businessImage;
	private TextView businessNameTv;
	private TextView productCostTv;
	private CheckBox saveDetailsChackBox;
	private Button doneBtn;
	
	private boolean isHasChacked;
	private boolean removeSeaved;
	private String savedToken;
	private CreditCard creditCard;

	public FinishDealFragment() {

	}

	public static FinishDealFragment newInstance(CreditCard creditCard) {
		FinishDealFragment instance = new FinishDealFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_CREDIT_CARD, creditCard);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_finish_payment, container,
				false);
//		creditCard = (CreditCard) getArguments().getSerializable(EXTRA_CREDIT_CARD);
		PelecardContainerActivity.setFragmentName(getString(R.string.finish_deal_screen_name));
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		businessImage = (ImageView) view.findViewById(R.id.business_image);
		businessNameTv = (TextView) view.findViewById(R.id.business_name);
		productCostTv = (TextView) view.findViewById(R.id.cost);
		saveDetailsChackBox = (CheckBox) view.findViewById(R.id.save_details_cb);
		doneBtn = (Button) view.findViewById(R.id.done_btn);
		
		doneBtn.setOnClickListener(this);
//		saveDetailsChackBox.setOnCheckedChangeListener(this);
		
		fillScreenData();
//		if (PelecardContainerActivity.payHendler != null){
//			Message message = new Message();
//			message.what = 1;
//			message.arg1 = PaymentFragment.PAY_SUCCESS;
//			PaymentFragment.pelecardResult.setCreditCard(creditCard);
//			message.obj = PaymentFragment.pelecardResult;
//			PelecardContainerActivity.payHendler.dispatchMessage(message);
//		}
	}

	
	private void fillScreenData () {



//		PelecardDeal deal = PelecardContainerActivity.pelecardDeal;
//		AppUtil.loadImageInto(getActivity(), deal.getBusinessImageUrl(),
//				businessImage, R.drawable.btn_selector, R.drawable.btn_selector, 100, 100, null);
//		businessNameTv.setText(deal.getBusinessName());
//		productCostTv.setText(getString(R.string.sam) + String.valueOf((int)deal.getProductCost()));
//		savedToken = PelecardPreferences.getInstans(getActivity()).getPelecardToken();
//		if (!savedToken.equals("")){
//			if (PelecardPreferences.getInstans(getActivity()).getCreditCardNumber().equalsIgnoreCase(creditCard.getCardNumber())){
//				isHasChacked = true;
//				saveDetailsChackBox.setChecked(true);
//			}else{
//				removeSeaved = false;
//				saveDetailsChackBox.setChecked(false);
//			}
//		}else{
//			removeSeaved = true;
//			saveDetailsChackBox.setChecked(false);
//		}
	}
	
	@Override
	public void onClick(View v) {
		Intent data = new Intent();
//		Bundle extra = new Bundle();
//		PaymentFragment.pelecardResult.setCreditCard(creditCard);
//		extra.putSerializable(EXTRA_PELECARD_RESULT, PaymentFragment.pelecardResult);
//		extra.putInt(PAY_RESULT, PaymentFragment.PAY_SUCCESS);
//		data.putExtras(extra);
		getActivity().setResult(Activity.RESULT_OK,data);
		getActivity().finish();
	}

	
	@Override
	public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
		
		if (arg1){
			if (isHasChacked){
				SaveTokenToPreference(savedToken);
				return;
			}
//			performConvertToToken();
		}else{
			isHasChacked = false;
			if (removeSeaved){
				removeTokenFromPreference();
			}else{
				removeSeaved = true;
			}
			
		}
	}
	
	private void performConvertToToken () {
		if (creditCard == null){
			return;
		}
		showProgressDialog();
		PelecardApiManager.getInstance(getActivity()).convertToToken
		(PelecardContainerActivity.pelecardConfiguration, 
				creditCard.getCardNumber(), creditCard.getCardValidity(), new PelecardApiCallback(){
			
			@Override
			public PelecardResponseObject parseResponse(String toParse) {
				return new PelecardResponseObject(toParse,PelecardResponseObject.RESPONSE_TYPE_CONVERT_TO_TOKEN);
			}
			
			@Override
			public void onDataRecived(PelecardResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				if (!isHasError){
					SaveTokenToPreference(response.getContent());
				}else{
					ToastUtil.toster(getActivity(), erroDescription, false);
					saveDetailsChackBox.setChecked(false);
				}
			}
					
			@Override
			public void onError() {
				super.onError();
				dismisProgressDialog();
				ToastUtil.toster(getActivity(), getString(R.string.general_network_error), false);
			}
							
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}				
									
			
		});
		
	}
	
	private void SaveTokenToPreference (String token) {
		
		Log.e("creditcardlog", "savedcreditcard = " + creditCard.toString());
		PelecardPreferences.getInstans(getActivity()).setPelecardToken(token);
		PelecardPreferences.getInstans(getActivity()).setCreditCardNumber(creditCard.getCardNumber());
		PelecardPreferences.getInstans(getActivity()).setCreditCardValidity(creditCard.getCardValidity());
		PelecardPreferences.getInstans(getActivity()).setCreditCardCvv(creditCard.getCardCVV());
		PelecardPreferences.getInstans(getActivity()).setCreditCardType(creditCard.getCreditCardType());
	}
	
	private void removeTokenFromPreference () {
		PelecardPreferences.getInstans(getActivity()).setPelecardToken("");
		PelecardPreferences.getInstans(getActivity()).setCreditCardNumber("");
		PelecardPreferences.getInstans(getActivity()).setCreditCardValidity("");
		PelecardPreferences.getInstans(getActivity()).setCreditCardCvv("");
		PelecardPreferences.getInstans(getActivity()).setCreditCardType(0);
	}

	
	@Override
	public boolean onBackPressed() {
		// TODO Auto-generated method stub
		Intent data = new Intent();
//		Bundle extra = new Bundle();
//		extra.putSerializable(EXTRA_PELECARD_RESULT, PaymentFragment.pelecardResult);
//		data.putExtras(extra);
		getActivity().setResult(Activity.RESULT_OK,data);
		getActivity().finish();
		return true;
	}

}