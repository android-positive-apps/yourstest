/**
 * 
 */
package com.positiveapps.pelecardsdk.fragments;



import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import com.positiveapps.pelecardsdk.PelecardContainerActivity;
import com.positiveapps.pelecardsdk.R;
import com.positiveapps.pelecardsdk.util.BackStackUtil;
import com.positiveapps.pelecardsdk.util.DialogUtil;


/**
 * @author Nati Gabay
 *
 */
public class BaseFragment extends Fragment{
	
	protected ProgressDialog progressDialog;
	public boolean fragIsOn;
	protected String screenName;
	
	public boolean onBackPressed () {
		return false;
	}
	
	protected void showProgressDialog(String message) {
		this.progressDialog = 
				DialogUtil.showProgressDialog(getActivity(), message);
	}
	
	protected void showProgressDialog() {
		try {
			this.progressDialog = 
			         DialogUtil.showProgressDialog(getActivity(), 
			        		 getString(R.string.dialog_deafult_message));
		} catch (Exception e) {}
	}
	
	protected void dismisProgressDialog() {
		DialogUtil.dismisDialog(this.progressDialog);
	}
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		fragIsOn = true;
		PelecardContainerActivity.currentFragment = this;
		BackStackUtil.addToBackStack(getClass().getSimpleName());
		
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		fragIsOn = false;
		BackStackUtil.removeFromBackStack(getClass().getSimpleName());
	}

}
