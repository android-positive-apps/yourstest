/**
 * 
 */
package com.positiveapps.pelecardsdk.fragments;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.positiveapps.pelecardsdk.PelecardContainerActivity;
import com.positiveapps.pelecardsdk.R;
import com.positiveapps.pelecardsdk.util.AppUtil;
import com.positiveapps.pelecardsdk.util.FragmentsUtil;
import com.positiveapps.pelecardsdk.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class DealDetailsFragment extends BaseFragment implements OnClickListener {
	
	
	
	private EditText businessNameEt;
	private EditText productNameEt;
	private EditText productCostEt;
	private Button continueBtn;
	
	

	public DealDetailsFragment() {

	}

	public static DealDetailsFragment newInstance() {
		DealDetailsFragment instance = new DealDetailsFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_deal_details, container,
				false);
		
		PelecardContainerActivity.setFragmentName(getString(R.string.deal_details_screen_name));
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		businessNameEt = (EditText) view.findViewById(R.id.business_name_et);
		productNameEt = (EditText) view.findViewById(R.id.product_name_et);
		productCostEt = (EditText) view.findViewById(R.id.cost_et);
		continueBtn = (Button) view.findViewById(R.id.continue_btn);
		
		continueBtn.setOnClickListener(this);
		
	}

	
	@Override
	public void onClick(View v) {
		openPamentFragment ();
	}

	/**
	 * 
	 */
	private void openPamentFragment() {
		if (AppUtil.validation(businessNameEt,productNameEt,productCostEt)){
			
			PelecardContainerActivity.setPelecardDeal
							(businessNameEt.getText().toString(),
							productNameEt.getText().toString(),
							productCostEt.getText().toString());
			
			FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
					PaymentFragment.newInstance(), R.id.main_container);
			
		}else{
			ToastUtil.toster(getActivity(), getString(R.string.validation_fields_message), false);
		}
	}


}
