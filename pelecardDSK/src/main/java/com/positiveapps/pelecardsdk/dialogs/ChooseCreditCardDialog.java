/**
 * 
 */
package com.positiveapps.pelecardsdk.dialogs;


import com.positiveapps.pelecardsdk.R;
import com.positiveapps.pelecardsdk.fragments.PaymentFragment;
import com.positiveapps.pelecardsdk.object.CreditCard;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;



/**
 * @author natiapplications
 *
 */
public class ChooseCreditCardDialog extends DialogFragment implements OnClickListener{
	
	public static final String DIALOG_NAME = "ChooseCreditCardDialog";
	
	
	private Fragment parent;
	
	private ImageView creditCardImage;
	private TextView creditCardNumber;
	private TextView creditCardValidity;
	private TextView cvvValidation;
	private EditText creditCardCvv;
	private Button yesBtn;
	private Button anutherCardBtn;
	
	
	private CreditCard data;
	
	private PelecardDialogCallback callback;
	
	/**
	 * 
	 * @param description
	 * @param credits
	 * @param callback
	 */
	public ChooseCreditCardDialog(Fragment parent,CreditCard data,PelecardDialogCallback callback){
		//this.mDescription = description;
		this.callback = callback;
		this.data = data;
		this.parent = parent;
	}
	
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_choose_credit_card, container, false);
                      
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        
        creditCardImage = (ImageView)view.findViewById(R.id.card_image_iv);
        creditCardValidity = (TextView)view.findViewById(R.id.card_validity_txt);
        creditCardNumber = (TextView)view.findViewById(R.id.crdit_card_number_txt);
        cvvValidation = (TextView)view.findViewById(R.id.missing_cvv_txt);
        creditCardCvv = (EditText)view.findViewById(R.id.card_cvv_et);
        yesBtn = (Button)view.findViewById(R.id.yes_btn);
        anutherCardBtn = (Button)view.findViewById(R.id.anuther_credit_card_btn);
        
        yesBtn.setOnClickListener(this);
        anutherCardBtn.setOnClickListener(this);
        
        creditCardImage.setImageDrawable(PaymentFragment.creditCardLogos.getDrawable(data.getCreditCardType()));
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < data.getCardNumber().length()-4; i++) {
			stringBuilder.append("X");
		}
        String lastFourNumber = "XXXX";
        try {
        	lastFourNumber = data.getCardNumber().substring(data.getCardNumber().length()-4,
        			data.getCardNumber().length());
		} catch (Exception e) {}
        
        
        stringBuilder.append(lastFourNumber);
        creditCardNumber.setText(stringBuilder.toString());
        
        creditCardValidity.setText(data.getCardValidity());
        cvvValidation.setVisibility(View.GONE);
       
        return view;
    }


  

    private void dismissDialog(){
    	
    	this.dismiss();
    }
    
    

	@Override
	public void onClick(View v) {
		
		
		if (v.getId() == R.id.anuther_credit_card_btn){
			dismissDialog();
			return;
		}
		
		if (creditCardCvv.getText().toString().equals("")||creditCardCvv.getText().toString().length() != 3){
			cvvValidation.setVisibility(View.VISIBLE);
			cvvValidation.setText(getString(R.string.set_cvv));
			return;
		}else if (creditCardCvv.getText().toString().length() == 3 
				&& !creditCardCvv.getText().toString().equalsIgnoreCase(data.getCardCVV())){
			cvvValidation.setVisibility(View.VISIBLE);
			cvvValidation.setText(getString(R.string.invalid_cvv));
			return;
		}
		cvvValidation.setVisibility(View.GONE);
    	dismissDialog();
    	
    	if(callback != null){
    		callback.onDialogButtonPressed(v.getId(), this,creditCardCvv.getText().toString());
    	}
	}


	

	
}
