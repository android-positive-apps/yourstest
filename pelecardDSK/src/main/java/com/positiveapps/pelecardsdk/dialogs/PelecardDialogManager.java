/**
 * 
 */
package com.positiveapps.pelecardsdk.dialogs;


import com.positiveapps.pelecardsdk.object.CreditCard;

import android.support.v4.app.Fragment;

/**
 * @author Maor
 *
 */
public class PelecardDialogManager {

	/**
	 * 
	 * @param parentFragment
	 * @param description
	 * @param callback
	 */
	public static void showChooseCreditCardDialog(Fragment parentFragment, CreditCard data,
			PelecardDialogCallback callback){
		
		ChooseCreditCardDialog dialog = new ChooseCreditCardDialog(parentFragment,data,callback);
		dialog.show(parentFragment.getFragmentManager(), ChooseCreditCardDialog.DIALOG_NAME);
	}
	
	
	
}
