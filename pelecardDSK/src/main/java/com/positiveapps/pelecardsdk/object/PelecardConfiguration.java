/**
 * 
 */
package com.positiveapps.pelecardsdk.object;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class PelecardConfiguration implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 545658;
	
	
	private String terminalNumber;
	private String userName;
	private String password;
	private long payTimeout;
	
	
	
	public PelecardConfiguration() {
		super();
		// TODO Auto-generated constructor stub
	}



	public PelecardConfiguration(String terminalNumber, String userName,
			String password) {
		super();
		this.terminalNumber = terminalNumber;
		this.userName = userName;
		this.password = password;
	}



	/**
	 * @return the terminalNumber
	 */
	public String getTerminalNumber() {
		return terminalNumber;
	}



	/**
	 * @param terminalNumber the terminalNumber to set
	 */
	public void setTerminalNumber(String terminalNumber) {
		this.terminalNumber = terminalNumber;
	}



	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}



	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}



	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}



	/**
	 * @return the payTimeout
	 */
	public long getPayTimeout() {
		return payTimeout;
	}



	/**
	 * @param payTimeout the payTimeout to set
	 */
	public void setPayTimeout(long payTimeout) {
		this.payTimeout = payTimeout;
	}



	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean isValidConfig (){
		if (userName == null || userName.equals("") ||
				password == null || password.equals("") ||
				terminalNumber == null || terminalNumber.equals("")){
			return false;
		}
		return true;
	}
	
	

}
