/**
 * 
 */
package com.positiveapps.pelecardsdk.object;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class PelecardDeal implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 546541;
	
	private String businessName;
	private String businessImageUrl;
	private String productName;
	private float productCost;
	private String subMenuId;
	private String sessionId;
	private String orderNumber;
	
	
	public PelecardDeal() {
		super();
	}



	public PelecardDeal(String businessName, String businessImageUrl,
			String productName, float productCost) {
		super();
		this.businessName = businessName;
		this.businessImageUrl = businessImageUrl;
		this.productName = productName;
		this.productCost = productCost;
	}



	/**
	 * @return the businessName
	 */
	public String getBusinessName() {
		return businessName;
	}



	/**
	 * @param businessName the businessName to set
	 */
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}



	/**
	 * @return the businessImageUrl
	 */
	public String getBusinessImageUrl() {
		return businessImageUrl;
	}



	/**
	 * @param businessImageUrl the businessImageUrl to set
	 */
	public void setBusinessImageUrl(String businessImageUrl) {
		this.businessImageUrl = businessImageUrl;
	}



	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}



	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}



	/**
	 * @return the productCost
	 */
	public float getProductCost() {
		return productCost;
	}



	/**
	 * @param productCost the productCost to set
	 */
	public void setProductCost(float productCost) {
		this.productCost = productCost;
	}
	
	public int getProductCostAsSents () {
		if (getProductCost() == 0){
			return 0;
		}
		String costAsString = String.valueOf(getProductCost()).replace(".", "");
		int costAsInt = Integer.parseInt(costAsString);
		return costAsInt * 10;
	}



	/**
	 * @return the subMenuId
	 */
	public String getSubMenuId() {
		return subMenuId;
	}



	/**
	 * @param subMenuId the subMenuId to set
	 */
	public void setSubMenuId(String subMenuId) {
		this.subMenuId = subMenuId;
	}



	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}



	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	

	/**
	 * @return the orderNumber
	 */
	public String getOrderNumber() {
		return orderNumber;
	}



	/**
	 * @param orderNumber the orderNumber to set
	 */
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PelecardDeal [businessName=" + businessName
				+ ", businessImageUrl=" + businessImageUrl + ", productName="
				+ productName + ", productCost=" + productCost + "]";
	}
	

}
