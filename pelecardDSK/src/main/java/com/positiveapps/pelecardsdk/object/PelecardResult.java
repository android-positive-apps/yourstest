/**
 * 
 */
package com.positiveapps.pelecardsdk.object;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class PelecardResult implements Serializable{
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PelecardDeal deal;
	private String confirmationNumber;
	private int confirmBy;
	private String first6DigitOfCreditCardNumber;
	private CreditCard creditCard;
	private String errorCode;
	private String voucherId;
	private String dealTocken;
	
	
	
	public PelecardResult(PelecardDeal deal, String confirmationNumber,
			int confirmBy,String first6DigitOfCreditCardNumber) {
		super();
		this.deal = deal;
		this.confirmationNumber = confirmationNumber;
		this.confirmBy = confirmBy;
		this.first6DigitOfCreditCardNumber = first6DigitOfCreditCardNumber;
	}
	/**
	 * @return the first6DigitOfCreditCardNumber
	 */
	public String getFirst6DigitOfCreditCardNumber() {
		if (first6DigitOfCreditCardNumber != null && first6DigitOfCreditCardNumber.length() >=6){
			return first6DigitOfCreditCardNumber.substring(0,6);
		}
		return first6DigitOfCreditCardNumber;
	}
	
	/**
	 * @param first6DigitOfCreditCardNumber the first6DigitOfCreditCardNumber to set
	 */
	public void setFirst6DigitOfCreditCardNumber(
			String first6DigitOfCreditCardNumber) {
		this.first6DigitOfCreditCardNumber = first6DigitOfCreditCardNumber;
	}
	/**
	 * @return the deal
	 */
	public PelecardDeal getDeal() {
		return deal;
	}
	/**
	 * @param deal the deal to set
	 */
	public void setDeal(PelecardDeal deal) {
		this.deal = deal;
	}
	/**
	 * @return the confirmationNumber
	 */
	public String getConfirmationNumber() {
		return confirmationNumber;
	}
	/**
	 * @param confirmationNumber the confirmationNumber to set
	 */
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	/**
	 * @return the confirmBy
	 */
	public int getConfirmBy() {
		return confirmBy;
	}
	/**
	 * @param confirmBy the confirmBy to set
	 */
	public void setConfirmBy(int confirmBy) {
		this.confirmBy = confirmBy;
	}
	
	
	/**
	 * @return the creditCard
	 */
	public CreditCard getCreditCard() {
		return creditCard;
	}
	/**
	 * @param creditCard the creditCard to set
	 */
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	
	
	
	/**
	 * @return the voucherId
	 */
	public String getVoucherId() {
		return voucherId;
	}
	/**
	 * @param voucherId the voucherId to set
	 */
	public void setVoucherId(String voucherId) {
		this.voucherId = voucherId;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PelecardResult [deal=" + deal + ", confirmationNumber="
				+ confirmationNumber + ", confirmBy=" + confirmBy
				+ ", first6DigitOfCreditCardNumber="
				+ first6DigitOfCreditCardNumber + ", creditCard=" + creditCard
				+ "]";
	}
	/**
	 * @return the dealTocken
	 */
	public String getDealTocken() {
		return dealTocken;
	}
	/**
	 * @param dealTocken the dealTocken to set
	 */
	public void setDealTocken(String dealTocken) {
		this.dealTocken = dealTocken;
	}
	
	

}
