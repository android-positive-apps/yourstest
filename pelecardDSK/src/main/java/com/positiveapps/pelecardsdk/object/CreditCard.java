/**
 * 
 */
package com.positiveapps.pelecardsdk.object;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * @author natiapplications
 *
 */
public class CreditCard implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static String[] cardsName = {"visa","mastercard","jcb","enroute","discover","diners","american_express","isracard","unknown"};
	
	public static final int CREDIT_CARD_TYPE_VISA = 0;
    public static final int CREDIT_CARD_TYPE_MASTERCARD = 1;
    public static final int CREDIT_CARD_TYPE_JCB = 2;
    public static final int CREDIT_CARD_TYPE_ENROUTE = 3;
    public static final int CREDIT_CARD_TYPE_DISCOVER = 4;
    public static final int CREDIT_CARD_TYPE_DINERS = 5;
    public static final int CREDIT_CARD_TYPE_AMERICAN_EXPRESS = 6;
    public static final int CREDIT_CARD_TYPE_ISRACARD = 7;
    
    private int creditCardType;
    private String cardNumber;
    private String cardValidity;
    private String cardCVV;
    
    
    
    
    
    public CreditCard(String cardNumber, String cardValidity, String cardCVV) {
		super();
		this.cardNumber = cardNumber;
		this.cardValidity = cardValidity;
		this.cardCVV = cardCVV;
	}

    
    
	/**
	 * @return the cardNumber
	 */
	public String getCardNumber() {
		return cardNumber;
	}



	/**
	 * @param cardNumber the cardNumber to set
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}


	public String getCreditCardSafeNumber (){
		if (this.cardNumber != null && !this.cardNumber.equals("")){
			
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < this.cardNumber.length(); i++) {
				if (i>=6&&i<this.cardNumber.length()-4){
					builder.append("*");
				}else{
					builder.append(this.cardNumber.charAt(i));
				}
			}
			return builder.toString();
		}
		return "";
	}

	/**
	 * @return the cardValidity
	 */
	public String getCardValidity() {
		return cardValidity;
	}



	/**
	 * @param cardValidity the cardValidity to set
	 */
	public void setCardValidity(String cardValidity) {
		this.cardValidity = cardValidity;
	}



	/**
	 * @return the cardCVV
	 */
	public String getCardCVV() {
		return cardCVV;
	}



	/**
	 * @param cardCVV the cardCVV to set
	 */
	public void setCardCVV(String cardCVV) {
		this.cardCVV = cardCVV;
	}



	/**
	 * @return the creditCardType
	 */
	public int getCreditCardType() {
		return creditCardType;
	}



	/**
	 * @param creditCardType the creditCardType to set
	 */
	public void setCreditCardType(int creditCardType) {
		this.creditCardType = creditCardType;
	}



	public static boolean isValidCardNumber (int type,String toValid){
    	boolean result = false;
    	int length = toValid.length();
		switch (type) {
		case CREDIT_CARD_TYPE_VISA:
			if (length == 13 || length == 16 ){
				if (toValid.startsWith("4")){
					result = true;
				}
			}
			break;
		case CREDIT_CARD_TYPE_MASTERCARD:
			if (length != 16){
				break;
			}
			long number = Long.parseLong(toValid.substring(0,2));
			if (number >50 && number <= 55){
				result = true;
			}
			break;
		case CREDIT_CARD_TYPE_JCB:
			if (length != 16){
				break;
			}
			long number2 = Long.parseLong(toValid.substring(0,4));
			if (number2 == 3088 || number2 == 3096 || number2 == 3112 || number2 == 3158 ||number2 == 3337){
				result = true;
			}else if (number2 >= 35280000 && number2 <= 35899999){
				result = true;
			}
			break;
		case CREDIT_CARD_TYPE_ENROUTE:
			if (length != 15){
				break;
			}
			long number3 = Long.parseLong(toValid.substring(0,4));
			if (number3 == 2014 || number3 == 2149){
				result = true;
			}
			break;
		case CREDIT_CARD_TYPE_DISCOVER:
			if (length != 16){
				break;
			}
			long number4 = Long.parseLong(toValid.substring(0,8));
			if (number4 >= 60110000 && number4 <= 60119999){
				result = true;
			}else if (number4 >= 65000000 && number4 <= 65999999){
				result = true;
			}else if (number4 >= 62212600 && number4 <= 62292599){
				result = true;
			}
			break;
		case CREDIT_CARD_TYPE_DINERS:
			if (length != 14){
				break;
			}
			long number5 = Long.parseLong(toValid.substring(0,2));
			if (number5 == 30 || number5 == 36 || number5 == 38){
				result = true;
			}
			break;
		case CREDIT_CARD_TYPE_AMERICAN_EXPRESS:
			if (length != 15){
				break;
			}
			long number6 = Long.parseLong(toValid.substring(0,2));
			if (number6 == 34 || number6 == 37){
				result = true;
			}
			break;
		case CREDIT_CARD_TYPE_ISRACARD:
			if (length < 8||length>9){
				break;
			}
			result = isValidIsracardNumber(toValid);
			
			break;
		}
		
		return result;
    }
    
	private static  boolean isValidIsracardNumber (String number){
		String temp = number;
		String check = "987654321";
		if (number.length() == 8){
			temp = "0" + number;
		}
		int total = 0;
		for (int i = 0; i < check.length(); i++) {
			int tempCheckInt = Integer.parseInt(check.charAt(i)+"");
			int tempNumber = Integer.parseInt(temp.charAt(i)+"");
			int cal = tempCheckInt*tempNumber;
			total+=cal;
		}
		if (total%11==0){
			return true;
		}
		return false;
	}
	
    public static boolean isValidValidity (long validityTimeAsMilli){
    	
    	Calendar calendar = Calendar.getInstance();
    
    	if (validityTimeAsMilli < calendar.getTimeInMillis()){
    		return false;
    	}
    	
    	return true;
    	
    }



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CreditCard [creditCardType=" + creditCardType + ", cardNumber="
				+ cardNumber + ", cardValidity=" + cardValidity + ", cardCVV="
				+ cardCVV + "]";
	}

    
}
