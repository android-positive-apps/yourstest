/**
 * 
 */
package com.positiveapps.pelecardsdk.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.os.Build;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author Nati Gabay
 *
 */
public class AppUtil {
	
	
	/**
	 * get the application version name
	 * 
	 * @param context - context of current application
	 * @return the application version name
	 */
	public  static String getApplicationVersion(Context context) {
		String appVersion = "";
		try {
			PackageManager manager = context.getPackageManager();
			PackageInfo info = manager.getPackageInfo(
					context.getPackageName(), 0);
			appVersion =  info.versionName;
		} catch (Exception e) {
		}
		return appVersion;
	}
	
	
	/**
	 * set up activity to display on full screen mood
	 * 
	 * @param activity - activity to set up
	 */
	public static void makeFullScreenActivity (Activity activity){
		activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	/**
	 * set up activity to display on full screen mood
	 * 
	 * @param activity - activity to set up
	 */
	public static void makeNoTitleActivity (Activity activity){
		activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		/*activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
	}
	
	
	
	/**
	 * Picasso
	 * Load image from URL using Picasso library
	 * @param context
	 * @param url
	 * @param target
	 * @param imageForPlaceHolder
	 * @param imageWhenError
	 * @param newWidth
	 * @param newHeight
	 * @param callback
	 */
	public static void loadImageInto(final Context context,String url, final ImageView target, 
			final int imageForPlaceHolder, final int imageWhenError, final int newWidth, final int newHeight, 
			final Callback callback){

    	if(url == null || url.equals("")){
    		url = "no_image";
    	}
    	
    	final String newUrl = url;
    	
		Picasso.with(context)
		.load(newUrl)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.resize(newWidth, newHeight)
		.into(target, new Callback() {
			
			@Override
			public void onSuccess() {
				if(callback != null){
					callback.onSuccess();
				}
				
				loadImage( context,newUrl, target, imageForPlaceHolder, imageWhenError, newWidth, newHeight);
			}
			
			@Override
			public void onError() {
				
			}
		});
    }
	
	/**
	 * Picasso
	 * Load image with no callback - use to refresh the image after first load
	 * @param context
	 * @param url
	 * @param target
	 * @param imageForPlaceHolder
	 * @param imageWhenError
	 * @param newWidth
	 * @param newHeight
	 */
	private static void loadImage(Context context, String url, 
			ImageView target, int imageForPlaceHolder, int imageWhenError, 
			int newWidth, int newHeight){
		
		Picasso.with(context)
		.load(url)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.resize(newWidth, newHeight)
		.into(target);
		
	}
	
	public static void loadOrginalImage(Context context, String url, 
			ImageView target, int imageForPlaceHolder, int imageWhenError){
		
		Picasso.with(context)
		.load(url)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.into(target);
		
	}
	
  
    
    public static String cleanText (String toClean){
    	String result = "";
    	try {
    		for (int i = 0; i < toClean.length(); i++) {
        		char temp = toClean.charAt(i);
        		if (temp >= '0' && temp <='9'){
        			result += temp;
        		}
    			
    		}
		} catch (Exception e) {}
    	return result;
    }
    
    
    
    public static void printKeyHash(Context context){
	    // Add code to print out the key hash
	    try {
	        PackageInfo info = context.getPackageManager().getPackageInfo(
	                "com.positiveapps.pelecardsdk", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {
	        Log.d("KeyHash:", e.toString());
	    } catch (NoSuchAlgorithmException e) {
	        Log.d("KeyHash:", e.toString());
	    }
	}
    
    
    
    public static void saveScreenDimention(Activity activity) {

		WindowManager w = activity.getWindowManager();
		Display d = w.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		d.getMetrics(metrics);
		// since SDK_INT = 1;
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
		try {
		    widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
		    heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
		} catch (Exception ignored) {
		}
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 17)
		try {
		    Point realSize = new Point();
		    Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
		    widthPixels = realSize.x;
		    heightPixels = realSize.y;
		} catch (Exception ignored) {
		}

		
	}
    
    public static boolean validation (TextView... toValid){
    	for (int i = 0; i < toValid.length; i++) {
			if (toValid[i].getText().toString().equals("")){
				return false;
			}
		}
    	return true;
    }
    

}
