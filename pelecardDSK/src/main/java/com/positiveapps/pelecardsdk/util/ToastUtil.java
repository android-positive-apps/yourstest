package com.positiveapps.pelecardsdk.util;


import android.content.Context;
import android.widget.Toast;

public class ToastUtil {
	
	public static void toster (Context context,String message, boolean length){
		try {
			if (length){
				Toast.makeText(context, message, Toast.LENGTH_LONG).show();
			}else{
				Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
		}
	}
	
	
}
