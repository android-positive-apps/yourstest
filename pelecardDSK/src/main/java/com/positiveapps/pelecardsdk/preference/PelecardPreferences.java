package com.positiveapps.pelecardsdk.preference;


import android.content.Context;
import android.content.SharedPreferences;

public class PelecardPreferences {

	private static PelecardPreferences mInstance = null;
	
	private SharedPreferences mPrefs = null;
	
	public static final String PELECARD_TOKEN = "PelecardToken";
	
	public static final String CREDIT_CARD_TYPE = "CreditCardTypeN";
	public static final String CREDIT_CARD_NUMBER = "CreditCardNumberN";
	public static final String CREDIT_CARD_VALIDITY = "CreditCardValidityN";
	public static final String CREDIT_CARD_CVV = "CreditCardCvvN";
	
	
	private PelecardPreferences (Context context){
		
		mPrefs = context.getSharedPreferences("PelecardPreferences", Context.MODE_PRIVATE);
	}
	
	public static PelecardPreferences getInstans(Context context){
		if(mInstance == null){
			mInstance = new PelecardPreferences(context);
		}
		return mInstance;
	}
	
	// getters
	
	public void setPelecardToken (String token) {
		mPrefs.edit().putString(PELECARD_TOKEN, token).commit();
	}
	
	public String getPelecardToken () {
		
		return mPrefs.getString(PELECARD_TOKEN, "");
		
	}
	
	public void setCreditCardNumber (String number) {
		mPrefs.edit().putString(CREDIT_CARD_NUMBER, number).commit();
	}
	
	public String getCreditCardNumber () {
		
		return mPrefs.getString(CREDIT_CARD_NUMBER, "");
		
	}
	
	public void setCreditCardValidity (String validity) {
		mPrefs.edit().putString(CREDIT_CARD_VALIDITY, validity).commit();
	}
	
	public String getCreditCardValidity () {
		
		return mPrefs.getString(CREDIT_CARD_VALIDITY, "");
		
	}
	public void setCreditCardCvv (String cvv) {
		mPrefs.edit().putString(CREDIT_CARD_CVV, cvv).commit();
	}
	
	public String getCreditCardCvv () {
		
		return mPrefs.getString(CREDIT_CARD_CVV, "");
		
	}
	public void setCreditCardType (int type) {
		mPrefs.edit().putInt(CREDIT_CARD_TYPE, type).commit();
	}
	
	public int getCreditCardType () {
		
		return mPrefs.getInt(CREDIT_CARD_TYPE, 0);
		
	}
	
	
	
	
	
	
	
}
