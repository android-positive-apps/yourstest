/**
 * 
 */
package com.positiveapps.pelecardsdk.api;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateUtils;

import com.positiveapps.pelecardsdk.R;
import com.positiveapps.pelecardsdk.object.PelecardConfiguration;
import com.positiveapps.pelecardsdk.object.PelecardDeal;


/**
 * @author Nati Gabay
 *
 * @description 
 */
public class PelecardApiManager { 
	
	
	/*********************
	 * ALL API REQUEST URLs
	 *********************/
	

		
	public static final String BASE_URL = "https://ws101.pelecard.biz/webservices.asmx/";
	
	
	private final String DEBIT_REGULAR_TYPE = BASE_URL + "DebitRegularType";
	private final String CONVERT_TO_TOKEN = BASE_URL + "ConvertToToken";
	
	
	
	
	
	
	/***********************
	 * ALL API REQUEST KEYs
	 ***********************/
	
	
	
	/** GENERAL KEYS **/
	
	
	private final String USER_NAME = "userName";
	private final String PASSWORD = "password";
	private final String TERMINAL_NUM = "termNo";
	private final String CREDIT_CARD_NUM = "creditCard";
	private final String CREDIT_CARD_VALIDITI = "creditCardDateMmyy";
	private final String SHOP_NUM = "shopNo";
	private final String TOKEN = "token";
	private final String TOTAL = "total";
	private final String CURRENCY = "currency";
	private final String CVV = "cvv2";
	private final String ID ="id";
	private final String AUTH_NUM = "authNum";
	private final String PARMX = "parmx";
	
	
	
	
	
	// context of current application
	private Context cotext;
	
	// single instance of this class
	private static PelecardApiManager instance;
	
	// connectivity manager to check network state before send the requests
	private  ConnectivityManager connectivityManager;
	
	
	/**
	 * private constructor
	 * 
	 * @param context - context of current application
	 */
	private PelecardApiManager (Context context){
		this.cotext = context;
		this.connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	}
	
	
	/**
	 * singleton design 
	 * get the single instance of this class 
	 * 
	 * @param context - context of current application
	 * @return the single instance of this class
	 */
	public static PelecardApiManager getInstance (Context context){
		if (instance == null){
			instance = new PelecardApiManager(context);
		}
		return instance;
	}
	
	/**
	 * remove single instance when application is destroyed
	 */
	public void removeInstance (){
		instance = null;
	}
	
	/**
	 * perform the requests by specify the request parameters
	 * 
	 * @param URL - string URL path of the server
	 * @param postData - List<NameValuePair> contains data to send over post request
	 * @param dialog - progress dialog to showing in progress. this parameter is optional. is may be null
	 * @param message - string message to display on dialog. this parameter is optional. is may be null
	 * @param callback - callback method to call when the task is done. this parameter is optional. is may be null
	 * 
	 */
	public void sendRequest (String URL,JSONObject postData,PelecardApiCallback callback){
		if (isNetworkAvailable()){
			PelecardRequestTask requestTask = new PelecardRequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.execute();
			return;
		}
		callback.networkUnavailable(cotext.getString(R.string.network_unavailable_message));
	}
	
	/**
	 * perform the requests by specify the request parameters
	 * 
	 * @param URL - string URL path of the server
	 * @param postData - List<NameValuePair> contains data to send over post request
	 * @param dialog - progress dialog to showing in progress. this parameter is optional. is may be null
	 * @param message - string message to display on dialog. this parameter is optional. is may be null
	 * @param callback - callback method to call when the task is done. this parameter is optional. is may be null
	 * 
	 */
	public void sendRequest (String URL,List<NameValuePair> postData,PelecardApiCallback callback){
		if (isNetworkAvailable()){
			PelecardRequestTask requestTask = new PelecardRequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.execute();
			return;
		}
		if (callback != null){
			callback.networkUnavailable(cotext.getString(R.string.network_unavailable_message));
		}
	}
	
	
	
	/**
	 * checks the network state
	 * 
	 * @return false if network is not available at current time
	 */
	public  boolean isNetworkAvailable() {

		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		} else {
			return false;
		}

	}
	
	
	public void convertToToken (PelecardConfiguration config,
			String creditCardNum, String creditCardValidity
			, PelecardApiCallback callback) {
		
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(USER_NAME, config.getUserName()));
		postData.add(new BasicNameValuePair(PASSWORD, config.getPassword()));
		postData.add(new BasicNameValuePair(TERMINAL_NUM, config.getTerminalNumber()));
		postData.add(new BasicNameValuePair(CREDIT_CARD_NUM, creditCardNum));
		postData.add(new BasicNameValuePair(CREDIT_CARD_VALIDITI, creditCardValidity.replace("/", "")));
		
		
		sendRequest(CONVERT_TO_TOKEN, postData,callback);
	
	}
	
	public void performPayment (PelecardDeal deal,PelecardConfiguration config,
			String creditCardNum, String creditCardValidity,String token,String cvv,String id
			, PelecardApiCallback callback) {
		
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(USER_NAME, config.getUserName()));
		postData.add(new BasicNameValuePair(PASSWORD, config.getPassword()));
		postData.add(new BasicNameValuePair(TERMINAL_NUM, config.getTerminalNumber()));
		postData.add(new BasicNameValuePair(SHOP_NUM, "001"));
		postData.add(new BasicNameValuePair(CREDIT_CARD_NUM, creditCardNum));
		postData.add(new BasicNameValuePair(CREDIT_CARD_VALIDITI, creditCardValidity.replace("/", "")));
		postData.add(new BasicNameValuePair(TOKEN, token));
		postData.add(new BasicNameValuePair(TOTAL, deal.getProductCostAsSents()+""));
		postData.add(new BasicNameValuePair(CURRENCY, "1"));
		postData.add(new BasicNameValuePair(CVV, cvv));
		postData.add(new BasicNameValuePair(ID, id));
		postData.add(new BasicNameValuePair(AUTH_NUM, ""));
		postData.add(new BasicNameValuePair(PARMX, Calendar.getInstance().getTimeInMillis()+ " " + deal.getOrderNumber()));
		
		sendRequest(DEBIT_REGULAR_TYPE, postData,callback);
	
	}
	
	
	
	

}
