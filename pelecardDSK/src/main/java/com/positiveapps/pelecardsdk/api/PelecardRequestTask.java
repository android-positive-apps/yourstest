package com.positiveapps.pelecardsdk.api;


import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


public class PelecardRequestTask extends AsyncTask<PelecardResponseObject, Void, PelecardResponseObject>{

	private final String TAG = "PelecardRequestTaskLog";
	private String requestName = "anonimus name";
	private boolean hasPostData;
	private Context context;
	
	private String url;
	private JSONObject dataEntity;
	private List<NameValuePair> listDataEntitiy;
	private PelecardApiCallback callback;
	
	
	
	
	public PelecardRequestTask(Context context, String url,
			PelecardApiCallback callback) {
		super();
		this.context = context;
		this.url = url;
		this.callback = callback;
	}
	
	
	/**
	 * set data to send in post request
	 * 
	 * @param dataEntity - data as List<NameValuePair> to send
	 */
	public void setDataEntityToPost (JSONObject dataEntity){
		this.dataEntity = dataEntity;
		this.hasPostData = true;
	}
	
	/**
	 * set data to send in post request
	 * 
	 * @param dataEntity - data as List<NameValuePair> to send
	 */
	public void setDataEntityToPost (List<NameValuePair> dataEntity){
		this.listDataEntitiy = dataEntity;
		this.hasPostData = true;
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		String[] requestSplit = url.split("/");
		if (requestSplit.length > 0){
			this.requestName = requestSplit[requestSplit.length-1];
		}
		Log.d(TAG, "Request name: " + requestName + " sent!" 
		+ "\n" + "Request url = " +  url);
	}
	
	
	
	@Override
	protected PelecardResponseObject doInBackground(PelecardResponseObject... params) {
		// TODO Auto-generated method stub
		
		PelecardResponseObject result = null;
		try {
			/*HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 10000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			HttpConnectionParams.setSoTimeout(httpParameters, 10000);
			
			DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

			HttpPost post = new HttpPost(this.url);*/
			
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 40000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			HttpConnectionParams.setSoTimeout(httpParameters, 40000);
			HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
			DefaultHttpClient client = new DefaultHttpClient(httpParameters);
			SchemeRegistry registry = new SchemeRegistry();
			SSLSocketFactory socketFactory = SSLSocketFactory.getSocketFactory();
			socketFactory.setHostnameVerifier((X509HostnameVerifier) hostnameVerifier);
			registry.register(new Scheme("https", socketFactory, 443));
			SingleClientConnManager mgr = new SingleClientConnManager(client.getParams(), registry);
			DefaultHttpClient httpClient = new DefaultHttpClient(mgr, client.getParams());

			// Set verifier     
			HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
			HttpPost post = new HttpPost(this.url);

			if (hasPostData) { 
				
				if (dataEntity != null){
					 post.setEntity(new StringEntity(dataEntity.toString(),HTTP.UTF_8)); 
				}else if (listDataEntitiy != null){
					post.setEntity(new UrlEncodedFormEntity(listDataEntitiy,HTTP.UTF_8));
				}
		       
		        String postData = EntityUtils.toString(post.getEntity()); 
		        Log.d(TAG, "data post over request: " + postData);
			}
			HttpResponse response = httpClient.execute(post);
			
			String responseString = EntityUtils.toString(response.getEntity());
			Log.e(TAG, "response string: " + responseString);
			
			if (this.callback != null){
				result = this.callback.parseResponse(responseString);
			}
			
		} catch (final Exception e) {
			Log.e(TAG, "exception: " + e.getMessage());
			e.printStackTrace();
		} catch (final Error er){
			Log.e(TAG, "error: " + er.getMessage());
		}
		
		return result;
	}
	
	
	@Override
	protected void onPostExecute(PelecardResponseObject result) {
		
		super.onPostExecute(result);
	
		if (result != null){
			Log.i(TAG, "result object" + result.toString());
			if (callback != null){
				callback.onDataRecived(result,result.isHasError(),result.getErrorDesc());
			}
		}else{
			Log.i(TAG, "result is null");
			if (callback != null){
				callback.onError();
			}
		}
	}
	

}
