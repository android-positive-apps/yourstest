/**
 * 
 */
package com.positiveapps.pelecardsdk.api;

import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;

import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.util.Log;

import com.positiveapps.pelecardsdk.PelecardContainerActivity;
import com.positiveapps.pelecardsdk.R;

/**
 * @author natiapplications
 *
 */
public class PelecardResponseObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int RESPONSE_TYPE_PEAYMENT = 1;
	public static final int RESPONSE_TYPE_CONVERT_TO_TOKEN = 2;
	
	public static final int CONFIRM_BY_SBA = 1;
	public static final int CONFIRM_BY_CRDIT_COMPANY = 2;
	public static final int CONFIRM_BY_VOICE_ANSWER = 3;
	public static final int NO_CONFIRM = 0;
	
	
	public static final int ANSWER_NO_SET = 0;
	public static final int ANSWER_NO_OK = 2;
	public static final int ANSWER_SET_AND_OK = 1;
	public static final int ANSWER_NO_CHECK = 3;
	
	
	
	private int type;
	private boolean isHasError;
	private String content;
	private String errorDesc;
	private String xmlContent;
	private int cvvCreditCompanyAnswer;
	private int idCreditCompanyAnswer;
	private int confirmationBy;
	private String confirmationNumber;
	private String creditCardNumber;
	private String errorCode;
	private String voucherID;
	
	public PelecardResponseObject() {
		super();
	}
	
	public PelecardResponseObject (String toParse,int responseType){
		
		this.xmlContent = toParse;
		this.type = responseType;
		try {
			parseXml(toParse);
			Log.e("parseLog", "content - " + content);
			Log.d("parseLog", "content - " + content.replace(" ", ""));
			
			if (type == RESPONSE_TYPE_PEAYMENT){
				if (content.length() <= 3){
					isHasError = true;
					errorCode = content;
					if (content.equals("039") || content.equals("033")){
						errorDesc = PelecardContainerActivity.appContext.getString(R.string.invalid_credit_card_number);
					}else if (content.equals("036")){
						errorDesc = PelecardContainerActivity.appContext.getString(R.string.invalid_credit_card_validity);
					}else{
						errorDesc = PelecardContainerActivity.appContext.getString(R.string.general_pelecard_error);
					}
				}else{
					if(content.startsWith("000")&&content.substring(0, 3).equalsIgnoreCase("000")){
						parseResponse();
						isHasError = false;
					}else{
						isHasError = true;
						errorCode = content.substring(0,3);
						if (content.substring(0, 3).equals("039") || content.substring(0, 3).equals("033")){
							errorDesc = PelecardContainerActivity.appContext.getString(R.string.invalid_credit_card_number);
						}else if (content.substring(0, 3).equals("036")){
							errorDesc = PelecardContainerActivity.appContext.getString(R.string.invalid_credit_card_validity);
						}else{
							errorDesc = PelecardContainerActivity.appContext.getString(R.string.general_pelecard_error);
						}
					}
					
				}
			}else if (type == RESPONSE_TYPE_CONVERT_TO_TOKEN){
				if (content.length() == 3){
					isHasError = true;
					errorDesc = PelecardContainerActivity.appContext.getString(R.string.convert_to_token_error);
				}else{
					if (content.length() == 10){
						isHasError = false;
					}else{
						errorDesc = PelecardContainerActivity.appContext.getString(R.string.convert_to_token_error);
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			isHasError = true;
			errorDesc = e.getMessage();
		}
		
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PelecardResponseObject [type=" + type + ", isHasError="
				+ isHasError + ", content=" + content + ", errorDesc="
				+ errorDesc + ", xmlContent=" + xmlContent
				+ ", cvvCreditCompanyAnswer=" + cvvCreditCompanyAnswer
				+ ", idCreditCompanyAnswer=" + idCreditCompanyAnswer
				+ ", confirmationBy=" + confirmationBy
				+ ", confirmationNumber=" + confirmationNumber + "]";
	}

	/**
	 * 
	 */
	private void parseResponse() {
		
		try {
			
			String creditCardNumber = content.substring(4,23);
			this.creditCardNumber = cleanFirstZero(creditCardNumber);
			Log.e("parseResponse", "creditCardNumber = " + this.creditCardNumber);
			
			String cvvAnswer = content.charAt(33)+"";
			Log.e("parseResponse", "cvvAnswer = " + cvvAnswer);
			cvvCreditCompanyAnswer = Integer.parseInt(cvvAnswer);
			
			String idAnswer = content.charAt(34)+"";
			Log.e("parseResponse", "idAnswer - " +idAnswer);
			idCreditCompanyAnswer = Integer.parseInt(idAnswer);
			
			String confirmation =  content.charAt(69)+"";
			Log.e("parseResponse", "confirmation - " + confirmation);
			confirmationBy = Integer.parseInt(confirmation);
			
			confirmationNumber = content.substring(70,77);
			Log.e("parseResponse", "confirmationNumber = " + confirmationNumber);
			
			try {
				String fileNumber =  content.substring(95,97);
				String kupa =  content.substring(97,100);
				String soder =  content.substring(100,103);
				voucherID = fileNumber + kupa + soder;
			} catch (Exception e) {}
			
		
		} catch (Exception e) {
		  Log.e("parseResponse", "ex = " + e.getMessage());
		}
		
	}
	
	private String cleanFirstZero (String toClean){
		int start = 0;
		for (int i = 0; i < toClean.length(); i++) {
			char temp = toClean.charAt(i);
			if (temp != '0' && temp != ' '){
				start = i;
				break;
			}
		}
		return toClean.substring(start,toClean.length());
	}

	/**
	 * @return the cvvCreditCompanyAnswer
	 */
	public int getCvvCreditCompanyAnswer() {
		return cvvCreditCompanyAnswer;
	}

	/**
	 * @param cvvCreditCompanyAnswer the cvvCreditCompanyAnswer to set
	 */
	public void setCvvCreditCompanyAnswer(int cvvCreditCompanyAnswer) {
		this.cvvCreditCompanyAnswer = cvvCreditCompanyAnswer;
	}

	/**
	 * @return the idCreditCompanyAnswer
	 */
	public int getIdCreditCompanyAnswer() {
		return idCreditCompanyAnswer;
	}

	/**
	 * @param idCreditCompanyAnswer the idCreditCompanyAnswer to set
	 */
	public void setIdCreditCompanyAnswer(int idCreditCompanyAnswer) {
		this.idCreditCompanyAnswer = idCreditCompanyAnswer;
	}

	/**
	 * @return the confirmationBy
	 */
	public int getConfirmationBy() {
		return confirmationBy;
	}

	/**
	 * @param confirmationBy the confirmationBy to set
	 */
	public void setConfirmationBy(int confirmationBy) {
		this.confirmationBy = confirmationBy;
	}

	/**
	 * @return the confirmationNumber
	 */
	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	/**
	 * @param confirmationNumber the confirmationNumber to set
	 */
	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}

	private void parseXml(String xml) throws XmlPullParserException,
			IOException {
		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();

			xpp.setInput(new StringReader(xml));
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_DOCUMENT) {
				} else if (eventType == XmlPullParser.END_DOCUMENT) {
				} else if (eventType == XmlPullParser.START_TAG) {
				} else if (eventType == XmlPullParser.END_TAG) {
				} else if (eventType == XmlPullParser.TEXT) {
					content = xpp.getText().trim();
				}
				eventType = xpp.next();
			}
		}
	}
	/**
	 * @return the isHasError
	 */
	public boolean isHasError() {
		return isHasError;
	}

	/**
	 * @param isHasError the isHasError to set
	 */
	public void setHasError(boolean isHasError) {
		this.isHasError = isHasError;
	}

	/**
	 * @return the errorDesc
	 */
	public String getErrorDesc() {
		return errorDesc;
	}

	/**
	 * @param errorDesc the errorDesc to set
	 */
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	/**
	 * @return the xmlContent
	 */
	public String getXmlContent() {
		return xmlContent;
	}

	/**
	 * @param xmlContent the xmlContent to set
	 */
	public void setXmlContent(String xmlContent) {
		this.xmlContent = xmlContent;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the creditCardNumber
	 */
	public String getCreditCardNumber() {
		return creditCardNumber;
	}

	/**
	 * @param creditCardNumber the creditCardNumber to set
	 */
	public void setCreditCardNumber(String creditCardNumber) {
		this.creditCardNumber = creditCardNumber;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the voucherID
	 */
	public String getVoucherID() {
		return voucherID;
	}

	/**
	 * @param voucherID the voucherID to set
	 */
	public void setVoucherID(String voucherID) {
		this.voucherID = voucherID;
	}

	
	
	
	

}
