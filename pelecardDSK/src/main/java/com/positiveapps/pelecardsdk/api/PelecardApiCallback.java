/**
 * 
 */
package com.positiveapps.pelecardsdk.api;

import org.json.JSONObject;


import android.util.Log;




/**
 * @author Nati GAbay
 *
 */
public class PelecardApiCallback {
	
	/**
	 * called when Internet connection is not available
	 */
	public void networkUnavailable (String message){
		Log.e("RequestTasklog", "network was not anabled");
		
	};
	
	
	/**
	 * called when the response returned from the server as String. in this method we parsing the response by convert it to some object
	 * the object returned by this method will provide by the onDataReceive method.
	 * 
	 * @param toParse - the response as String
	 * @return the object created by the parsing response
	 */
	public PelecardResponseObject parseResponse (String toParse){
		return new PelecardResponseObject(toParse,1);
	}
	
	/**
	 * called when the response has been success. in this case the method provides the object which created from the response 
	 * and boolean that indicate if has some error including in the response with the error description
	 * 
	 * @param response - the object which created by the response
	 * @param isHasError - indicate if has some error including in the response
	 * @param erroDescription - error description ( null if there is not any errors)
	 */
	public void onDataRecived (PelecardResponseObject response,boolean isHasError,String erroDescription){};
	
	/**
	 * called when the request failed 
	 */
	public void onError (){
		Log.e("RequestTasklog", "has error");
		
	};

}
