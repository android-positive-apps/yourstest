/**
 * 
 */
package com.positiveapps.yoursTest.database;



import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;

import android.content.Context;
import android.content.SharedPreferences;


/**
 * @author Nati Gabay
 *
 */
public class AppPreference {

	
	public static final int UNITS_TYPE_KM = 1;
	public static final int UNITS_TYPE_MILES = 2;
	public static final int SKI_PASS_TYPE_AREA = 1;
	public static final int SKI_PASS_TYPE_RESORT = 2;
	
	
	private static AppPreference instance;
	private static Context context = YoursApp.appContext;
	
	private static PreferenceUserProfil userProfil;
	private static PreferenceUserSettings userSettings;
	private static PreferenceGeneralSettings generalSettings;
	
	private AppPreference (Context context){
		userSettings = PreferenceUserSettings.getUserSettingsInstance();
		userProfil = PreferenceUserProfil.getUserProfileInstance();
		generalSettings = PreferenceGeneralSettings.getGeneralSettingsInstance();
	}
	
	public static AppPreference getInstans(Context context){
		if(instance == null){
			instance = new AppPreference(context);
		}
		setInstancContext(context);
		return instance;
	}
	
	public static void removeInstance(){
		userSettings.removeInstance();
		userProfil.removeInstance();
		generalSettings.removeInstance();
		instance = null;
	}
	
	private static void setInstancContext (Context context){
		AppPreference.context = context;
	}
	
	
	
	/**
	 * @return the userProfil
	 */
	public PreferenceUserProfil getUserProfil() {
		return userProfil;
	}

	/**
	 * @param userProfil the userProfil to set
	 */
	public void setUserProfil(PreferenceUserProfil userProfil) {
		this.userProfil = userProfil;
	}

	/**
	 * @return the userSettings
	 */
	public PreferenceUserSettings getUserSettings() {
		return userSettings;
	}

	/**
	 * @param userSettings the userSettings to set
	 */
	public void setUserSettings(PreferenceUserSettings userSettings) {
		this.userSettings = userSettings;
	}
	
	/**
	 * @return the generalSettings
	 */
	public PreferenceGeneralSettings getGeneralSettings() {
		return generalSettings;
	}

	/**
	 * @param generalSettings the generalSettings to set
	 */
	public void setGeneralSettings(PreferenceUserSettings generalSettings) {
		this.userSettings = generalSettings;
	}


	public static class PreferenceGeneralSettings {

		public final String GENERAL_SETTINGS = "GeneralSettings";

		public static final String LAT = "Lat";
		public static final String LON = "Lon";
		public static final String GCM_KEY = YoursApp.appVersion + "GcmKey";
		public static final String SCREEN_WIDTH = "ScreenWidth";
		public static final String SCREEN_HIGHT = "ScreenHight";
		public static final String LAST_READ_MESSAGE_COUNT = "LastReadMessageCount";
		public static final String LAST_DATE_UPDATED_CATEGORIES = "lastDateUpdatedCategories";
		

		private SharedPreferences generalSettings;
		public static PreferenceGeneralSettings generalSettingInstance;

		private PreferenceGeneralSettings() {
			generalSettings = context.getSharedPreferences(GENERAL_SETTINGS,
					Context.MODE_PRIVATE);
		}

		public static PreferenceGeneralSettings getGeneralSettingsInstance() {
			if (generalSettingInstance == null) {
				generalSettingInstance = new PreferenceGeneralSettings();
			}
			return generalSettingInstance;
		}
		
		public void removeInstance(){
			generalSettings.edit().clear().commit();
			generalSettingInstance = null;
		}

		public void setLat(String toSet) {
			generalSettings.edit().putString(LAT, toSet).commit();
		}

		public void setLon(String toSet) {
			generalSettings.edit().putString(LON, toSet).commit();
		}
		
		public void setGCMKey(String gcmKey) {
			generalSettings.edit().putString(GCM_KEY, gcmKey).commit();
		}
		
		
		public void setScreenWidth(int width) {
			generalSettings.edit().putInt(SCREEN_WIDTH, width).commit();
		}
		
		public void setScreenHight(int hight) {
			generalSettings.edit().putInt(SCREEN_HIGHT, hight).commit();
		}
		
		public void setLastDateUpdatedCategories(long lastDateInMillis) {
			generalSettings.edit().putLong(LAST_DATE_UPDATED_CATEGORIES, lastDateInMillis).commit();
		}

		public void setLastReadMessageCount(String groupID,int count) {
			generalSettings.edit().putInt(groupID + LAST_READ_MESSAGE_COUNT, count).commit();
		}
		
		public String getLat() {
			return generalSettings.getString(LAT, "0.0");
		}

		public String getLon() {
			return generalSettings.getString(LON, "0.0");
		}
		
		
		public String getLocation () {
			return getLat()+","+getLon();
		}
		
		public String getGCMKey () {
			return generalSettings.getString(GCM_KEY, "");
		}
		
		
		
		public int getScreenWidth () {
			return generalSettings.getInt(SCREEN_WIDTH, 0);
		}
		
		public int getScreenHight () {
			return generalSettings.getInt(SCREEN_HIGHT, 0);
		}
		
		public int getLastReadMessageCount (String groupId) {
			return generalSettings.getInt(groupId + LAST_READ_MESSAGE_COUNT, 0);
		}
		
		public long getLastDateUpdatedCategories() {
			return generalSettings.getLong(LAST_DATE_UPDATED_CATEGORIES, 0);
		}
		

	}



	public static class PreferenceUserProfil {
		
		public static final String USER_PROFILE = "UserProfile";
		
		
		
		private static final String FIRST_NAME = "FName";
		private static final String USER_ID = "UserId";
		private static final String LAST_NAME = "LName";
		private static final String IDENT_USER = "IdentUser";
		private static final String PASS = "Password";
		private static final String STREET = "Street";
		private static final String ZIP_CODE = "ZipNum";
		private static final String CITY = "Town";
		private static final String TELEPHONE = "Telephone";
		private static final String MOBILE = "Mobile";
		private static final String EMAIL = "Email";
		private static final String USER_NUMBER = "UserNumber";
		private static final String IS_PUBLIC = "IsPub";
		
		private SharedPreferences userProfile;
		private static PreferenceUserProfil userProfileInstance;
		
		private  PreferenceUserProfil (){
			userProfile = context.getSharedPreferences(USER_PROFILE, Context.MODE_PRIVATE);
		}
		
		public  static PreferenceUserProfil getUserProfileInstance (){
			if (userProfileInstance == null){
				userProfileInstance = new PreferenceUserProfil();
			}
			return userProfileInstance;
		}
		
		public  void removeInstance(){
			userProfile.edit().clear().commit();
			userProfileInstance = null;
		}
		
		public void setUserId(String userId) {
			userProfile.edit().putString(USER_ID, userId).commit();
		}

		public void setUserFirstNaem(String firstName) {
			userProfile.edit().putString(FIRST_NAME, firstName).commit();
		}

		public void setUserEmail(String email) {
			userProfile.edit().putString(EMAIL, email).commit();
		}

		
		public void setUserLastName(String lastName) {
			userProfile.edit().putString(LAST_NAME, lastName).commit();
		}
		
		public void setUserCity(String city) {
			userProfile.edit().putString(CITY, city).commit();
		}
		
		public void setUserPhone(String phone) {
			userProfile.edit().putString(MOBILE, phone).commit();
		}
		
		public void setUserAddress(String address) {
			userProfile.edit().putString(STREET, address).commit();
		}
		public void setUserPass(String pass) {
			userProfile.edit().putString(PASS, pass).commit();
		}
		public void setUserZipCode(String zipCode) {
			userProfile.edit().putString(ZIP_CODE, zipCode).commit();
		}
		public void setUserTelephone(String telephone) {
			userProfile.edit().putString(TELEPHONE, telephone).commit();
		}
		public void setUserNumber(String userNumber) {
			userProfile.edit().putString(USER_NUMBER, userNumber).commit();
		}
		public void setIdentUser(String identUser) {
			userProfile.edit().putString(IDENT_USER, identUser).commit();
		}
		public void setUserAccessLevel(boolean isPub) {
			userProfile.edit().putBoolean(IS_PUBLIC, isPub).commit();
		}
		
		
		
		
		public String getUserId() {
			return userProfile.getString(USER_ID, "");
		}

		public String getUserFirstName() {
			return userProfile.getString(FIRST_NAME, "");
		}

		public String getUserEmail() {
			return userProfile.getString(EMAIL, "");
		}

		public String getUserLastName() {
			return userProfile.getString(LAST_NAME,"");
		}
		
		public String getUserFullName(String dilimiter) {
			return getUserFirstName()+dilimiter+getUserLastName();
		}
		
		public String getUserPhone() {
			return userProfile.getString(MOBILE, "");
		}
		
		public String getUserTelephone() {
			return userProfile.getString(TELEPHONE, "");
		}
		public String getUserPass() {
			return userProfile.getString(PASS, "");
		}
		public String getUserAddress() {
			return userProfile.getString(STREET, "");
		}
		public String getUserCity() {
			return userProfile.getString(CITY, "");
		}
		public String getUserFullAddress(String dilimiter) {
			return getUserCity()+dilimiter+getUserAddress();
		}
		public String getUserZipCode() {
			return userProfile.getString(ZIP_CODE, "");
		}
		public String getUserNumber() {
			return userProfile.getString(USER_NUMBER, "");
		}
		public boolean isUserPublic() {
			return userProfile.getBoolean(IS_PUBLIC, true);
		}
		public String getIdentUser() {
			return userProfile.getString(IDENT_USER, "");
		}
		public String getUserStatusAsString(){
			if (isUserPublic()){
				return YoursApp.appContext.getString(R.string.public_status);
			}else{
				return YoursApp.appContext.getString(R.string.private_status);
			}
		}
		
		
		
		


	}
	
	public static class PreferenceUserSettings {
		
		public  final String USER_SETTINGS = "UserSettings";
		
		

		private SharedPreferences userSettings;
		public static  PreferenceUserSettings userSettingInstance;
		
		private PreferenceUserSettings() {
			userSettings = context.getSharedPreferences(USER_SETTINGS, Context.MODE_PRIVATE);
		}
		
		public static PreferenceUserSettings getUserSettingsInstance() {
			if (userSettingInstance == null){
				userSettingInstance = new PreferenceUserSettings();
			}
			return userSettingInstance;
		}
		
		public void removeInstance(){
			userSettings.edit().clear().commit();
			userSettingInstance = null;
		}
		
		
		
		

	}
	
	
	
	
	
	
	
	

}
