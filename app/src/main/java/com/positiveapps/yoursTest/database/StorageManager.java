/**
 * 
 */
package com.positiveapps.yoursTest.database;

import java.io.Serializable;

import com.positiveapps.yoursTest.YoursApp;

import android.util.Log;


/**
 * @author Nati Gabay
 *
 */
public class StorageManager {
	
	private final String TAG = "storatelog";
	private static  String BASE_NAME ;
	
	public static String CATEGORIES_FILE_NAME = "CategoriesFileName";
	
	
	
	
	private static StorageManager instance;
	
	private StorageManager () {
		
	}
	
	public static StorageManager getInstance () {
		if (instance == null){
			instance = new StorageManager();
		}
		
		
		return instance;
	}
	
	public static void removeInstance () {
		instance = null;
	}
	
	public void readFromStorage (String fileName, StorageListener callback) {
		Log.d(TAG, fileName);
		new ReadFromStorageThread(YoursApp.appContext, fileName, callback).start();
	}
	
	public void writeToStorage (String fileName, Serializable toWrite){
		Log.e(TAG, fileName);
		new WriteToStorageThread(YoursApp.appContext, toWrite, fileName).start();
	}

}
