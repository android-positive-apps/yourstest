/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

import org.json.JSONObject;

import com.positiveapps.yoursTest.util.DateUtil;

/**
 * @author natiapplications
 *
 */
public class HistoryProchase implements Serializable{

	private static final String M_DATE = "m_date";
	private static final String ORDER_ID = "order_id";
	private static final String SHOW_NAME = "mofa";
	private static final String SHOW_PLACE = "hofa_venue";
	private static final String SHOW_DATE = "hofa_date";
	private static final String SHOW_TIME = "hofa_time";
	private static final String X = "y";
	private static final String Y = "x";
	private static final String MONTH = "month";
	private static final String SINGLE_PRICE = "def_price";
	private static final String SAM_PRICE = "sum_isska";
	private static final String MORE_CONFIRM = "ishur_nossaf";
	private static final String CONFIRM_NUMBER = "isska_nb";
	private static final String CLIENT_EMAIL = "client_email";
	private static final String CLIENT_PHONE = "phone";
	private static final String IMPLEMENTION_DATE = "mimosh_date";
	private static final String IMPLEMENTION = "mimosh";
	private static final String CREDIT_NUMBER = "credit_number";
	private static final String COUPON = "copon";
	
	
	private int orderId;
	private String buy_date;
	private String show_date;
	private String showPlace;
	private String name;
	private String showTime;
	private int x;
	private int y;
	private int singlePrice;
	private int samPrice;
	private String confirmNumber;
	private String moreConfirm;
	private String email;
	private String phone;
	private String implementionDate;
	private String implemention;
	private String creditNumber;
	private String couponNumber;
	
	
	
	
	public HistoryProchase(JSONObject toParse) {
		super();
		this.orderId = toParse.optInt(ORDER_ID);
		this.buy_date = toParse.optString(M_DATE);
		this.show_date = toParse.optString(SHOW_DATE);
		this.showPlace = toParse.optString(SHOW_PLACE);
		this.name = toParse.optString(SHOW_NAME);
		this.x = toParse.optInt(X);
		this.y = toParse.optInt(Y);
		this.showTime = toParse.optString(SHOW_TIME);
		this.singlePrice = toParse.optInt(SINGLE_PRICE);
		this.samPrice = toParse.optInt(SAM_PRICE);
		this.confirmNumber = toParse.optString(CONFIRM_NUMBER);
		this.moreConfirm = toParse.optString(MORE_CONFIRM);
		this.email = toParse.optString(CLIENT_EMAIL);
		this.phone = toParse.optString(CLIENT_PHONE);
		this.implemention = toParse.optString(IMPLEMENTION);
		this.implementionDate = toParse.optString(IMPLEMENTION_DATE);
		this.creditNumber = toParse.optString(CREDIT_NUMBER);
		this.couponNumber = toParse.optString(COUPON);
	}


	
	public String getItemDetails (){
		StringBuilder stringBuilder = new StringBuilder();
		if (name != null && !name.isEmpty()){
			stringBuilder.append("שם: ");
			stringBuilder.append(name);
			stringBuilder.append("\n");
		}
		if(show_date != null && !show_date.isEmpty()){
			stringBuilder.append("תאריך: ");
			stringBuilder.append(DateUtil.getServerMillisAsDateString(show_date));
			stringBuilder.append("\n");
		}
		if (showPlace != null && !showPlace.isEmpty()){
			stringBuilder.append("מקום: ");
			stringBuilder.append(showPlace);
			stringBuilder.append("\n");
		}
		
		return stringBuilder.toString();
	}
	
	public String getBuyerDetails (){
		StringBuilder stringBuilder = new StringBuilder();
		if (email != null && !email.isEmpty()){
			stringBuilder.append("מייל: ");
			stringBuilder.append(email);
			stringBuilder.append("\n");
		}
		if(phone != null && !phone.isEmpty()){
			stringBuilder.append("פלאפון: ");
			stringBuilder.append(phone);
			stringBuilder.append("\n");
		}
		return stringBuilder.toString();
	}


	/**
	 * @return the orderId
	 */
	public int getOrderId() {
		return orderId;
	}




	/**
	 * @param orderId the orderId to set
	 */
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}




	/**
	 * @return the buy_date
	 */
	public String getBuy_date() {
		return buy_date;
	}




	/**
	 * @param buy_date the buy_date to set
	 */
	public void setBuy_date(String buy_date) {
		this.buy_date = buy_date;
	}




	/**
	 * @return the show_date
	 */
	public String getShow_date() {
		return show_date;
	}




	/**
	 * @param show_date the show_date to set
	 */
	public void setShow_date(String show_date) {
		this.show_date = show_date;
	}




	/**
	 * @return the showPlace
	 */
	public String getShowPlace() {
		return showPlace;
	}




	/**
	 * @param showPlace the showPlace to set
	 */
	public void setShowPlace(String showPlace) {
		this.showPlace = showPlace;
	}




	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}




	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}




	/**
	 * @return the showTime
	 */
	public String getShowTime() {
		return showTime;
	}




	/**
	 * @param showTime the showTime to set
	 */
	public void setShowTime(String showTime) {
		this.showTime = showTime;
	}




	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}




	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}




	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}




	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}




	/**
	 * @return the singlePrice
	 */
	public int getSinglePrice() {
		return singlePrice;
	}




	/**
	 * @param singlePrice the singlePrice to set
	 */
	public void setSinglePrice(int singlePrice) {
		this.singlePrice = singlePrice;
	}




	/**
	 * @return the samPrice
	 */
	public int getSamPrice() {
		return samPrice;
	}




	/**
	 * @param samPrice the samPrice to set
	 */
	public void setSamPrice(int samPrice) {
		this.samPrice = samPrice;
	}




	/**
	 * @return the confirmNumber
	 */
	public String getConfirmNumber() {
		return confirmNumber;
	}




	/**
	 * @param confirmNumber the confirmNumber to set
	 */
	public void setConfirmNumber(String confirmNumber) {
		this.confirmNumber = confirmNumber;
	}




	/**
	 * @return the moreConfirm
	 */
	public String getMoreConfirm() {
		return moreConfirm;
	}




	/**
	 * @param moreConfirm the moreConfirm to set
	 */
	public void setMoreConfirm(String moreConfirm) {
		this.moreConfirm = moreConfirm;
	}




	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}




	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}




	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}




	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}




	/**
	 * @return the implementionDate
	 */
	public String getImplementionDate() {
		return implementionDate;
	}




	/**
	 * @param implementionDate the implementionDate to set
	 */
	public void setImplementionDate(String implementionDate) {
		this.implementionDate = implementionDate;
	}




	/**
	 * @return the implemention
	 */
	public String getImplemention() {
		return implemention;
	}




	/**
	 * @param implemention the implemention to set
	 */
	public void setImplemention(String implemention) {
		this.implemention = implemention;
	}




	/**
	 * @return the creditNumber
	 */
	public String getCreditNumber() {
		if (creditNumber == null){
			return "לא ידוע";
		}
		if (creditNumber.length() >=4){
			return creditNumber.substring(creditNumber.length()-4,creditNumber.length());
		}
		return creditNumber;
	}




	/**
	 * @param creditNumber the creditNumber to set
	 */
	public void setCreditNumber(String creditNumber) {
		this.creditNumber = creditNumber;
	}



	/**
	 * @return the couponNumber
	 */
	public String getCouponNumber() {
		return couponNumber;
	}



	/**
	 * @param couponNumber the couponNumber to set
	 */
	public void setCouponNumber(String couponNumber) {
		this.couponNumber = couponNumber;
	}
	
	
	
	
}
