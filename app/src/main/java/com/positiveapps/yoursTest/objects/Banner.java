/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class Banner implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String imageUrl;
	private String link;
	
	
	public Banner(){
		
	}
	
	public Banner(String imageUrl, String link) {
		super();
		this.imageUrl = imageUrl;
		this.link = link;
	}
	
	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}
	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}
	/**
	 * @param link the link to set
	 */
	public void setLink(String link) {
		this.link = link;
	}
	
	

}
