/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.util.ColorsFactory;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class DrawerItem {
	
	public static final int ITEM_TYPE_CATEGORY = 1;
	public static final int ITEM_TYPE_SUB_CATEGORY = 2;
	
	
	private int type;
	private int category;
	private String name;
	private View view;
	private int position;
	private int subCategoryType;
	
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
	/**
	 * @return the category
	 */
	public int getCategory() {
		return category;
	}
	/**
	 * @param category the category to set
	 */
	public void setCategory(int category) {
		this.category = category;
	}
	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}
	
	
	/**
	 * @return the subCategoryType
	 */
	public int getSubCategoryType() {
		return subCategoryType;
	}
	/**
	 * @param subCategoryType the subCategoryType to set
	 */
	public void setSubCategoryType(int subCategoryType) {
		this.subCategoryType = subCategoryType;
	}
	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}
	public View getView (){
		switch (type) {
		case ITEM_TYPE_CATEGORY:
			view = buildCategoryView();
			break;
		case ITEM_TYPE_SUB_CATEGORY:
			view =  buildSubCategoryView();
			break;
		}
		return this.view;
	}
	/**
	 * @return
	 */
	private View buildSubCategoryView() {
		LayoutInflater inflater = MainActivity.mainInstance.getLayoutInflater();
		View result = inflater.inflate(R.layout.drawer_item_sub_category, null);
		TextView subCategoryName = (TextView)result.findViewById(R.id.txt_sub_category_name);
		subCategoryName.setText(name);
		switch (category) {
		case Category.CATEGORY_FAMILLY:
			subCategoryName.setBackgroundResource(ColorsFactory.famillyColors[position]);
			break;
		case Category.CATEGORY_FUN:
			subCategoryName.setBackgroundResource(ColorsFactory.funColors[position]);
			break;
		case Category.CATEGORY_HOBY:
			subCategoryName.setBackgroundResource(ColorsFactory.hobyColors[position]);
			break;
		case Category.CATEGORY_PROCHASES:
			subCategoryName.setBackgroundResource(ColorsFactory.prochaseColor[position]);
			break;
		}
		return result;
	}
	/**
	 * @return
	 */
	private View buildCategoryView() {
		LayoutInflater inflater = MainActivity.mainInstance.getLayoutInflater();
		View result = inflater.inflate(R.layout.drawer_item_category, null);
		ImageView categoryBar = (ImageView)result.findViewById(R.id.category_bar);
		categoryBar.setImageResource(Category.categoryBars[category]);
		return result;
	}
	

}
