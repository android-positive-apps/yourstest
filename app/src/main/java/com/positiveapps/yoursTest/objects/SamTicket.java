/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author natiapplications
 *
 */
public class SamTicket extends SamObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String place;
	private ArrayList<RowTicket> tickets;
	private double samPrice;
	
	public SamTicket(String place, ArrayList<RowTicket> tickets) {
		super();
		this.place = place;  
		this.tickets = tickets;
	}
	
	public SamTicket() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		this.place = place;
	}

	/**
	 * @return the tickets
	 */
	public ArrayList<RowTicket> getTickets() {
		return tickets;
	}

	/**
	 * @param tickets the tickets to set
	 */
	public void setTickets(ArrayList<RowTicket> tickets) {
		this.tickets = tickets;
	}

	/**
	 * @return the samPrice
	 */
	public double getSamPrice() {
		
		if (samPrice > 0){
			return samPrice;
		}
		
		double sam = 0;
		
		if (tickets == null){
			return sam;
		}
		for (int i = 0; i < tickets.size(); i++) {
			sam += tickets.get(i).getPrice();
		}
		return sam;
	}

	/**
	 * @param samPrice the samPrice to set
	 */
	public void setSamPrice(double samPrice) {
		this.samPrice = samPrice;
	}

	

	
	
	

}
