/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author natiapplications
 *
 */
public class SamGeneralItem extends SamObject implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<RowGeneralItem> items;
	private double samPrice;
	
	public SamGeneralItem(ArrayList<RowGeneralItem> tickets) {
		super();
		this.items = tickets;
	}
	
	public SamGeneralItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	/**
	 * @return the tickets
	 */
	public ArrayList<RowGeneralItem> getItems() {
		return items;
	}

	/**
	 * @param tickets the tickets to set
	 */
	public void setItems(ArrayList<RowGeneralItem> items) {
		this.items = items;
	}

	/**
	 * @return the samPrice
	 */
	public double getSamPrice() {
		
		if (samPrice > 0){
			return samPrice;
		}
		double sam = 0;
		if (items == null){
			return sam;
		}
		for (int i = 0; i < items.size(); i++) {
			sam += items.get(i).getPrice();
		}
		return sam;
	}

	/**
	 * @param samPrice the samPrice to set
	 */
	public void setSamPrice(double samPrice) {
		this.samPrice = samPrice;
	}

	

	
	
	

}
