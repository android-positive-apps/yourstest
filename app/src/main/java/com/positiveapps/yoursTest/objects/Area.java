/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

import com.positiveapps.yoursTest.R;

/**
 * @author natiapplications
 *
 */
public class Area implements Serializable{
	
	
	public static final int[] AREA_COLORS = {R.drawable.area_bg_blue,R.drawable.area_bg_green,R.drawable.area_bg_paper,R.drawable.area_bg_yellow
		,R.drawable.area_bg_blue,R.drawable.area_bg_green,R.drawable.area_bg_paper,R.drawable.area_bg_yellow
		,R.drawable.area_bg_blue,R.drawable.area_bg_green,R.drawable.area_bg_paper,R.drawable.area_bg_yellow,
		R.drawable.area_bg_blue,R.drawable.area_bg_green,R.drawable.area_bg_paper,R.drawable.area_bg_yellow}; 
	
	private String itemId;
	private String areaId;
	private String areaName;
	private int areaColor;
	private int index;
	private String place;
	private String areaDate;
	private PlaceMap placeMap;
	private String placeID;
	
	/**
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}

	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		this.place = place;
	}

	public Area(int index){
		this.index = index;
	}
	
	/**
	 * @return the areaId
	 */
	public String getAreaId() {
		return areaId;
	}
	/**
	 * @param areaId the areaId to set
	 */
	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}
	/**
	 * @return the areaName
	 */
	public String getAreaName() {
		return areaName;
	}
	/**
	 * @param areaName the areaName to set
	 */
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	/**
	 * @return the areaColor
	 */
	public int getAreaColor() {
		int pos = index;
		if (pos >= AREA_COLORS.length){
			pos = AREA_COLORS.length-1;
		}
		return AREA_COLORS[pos];
	}

	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}

	/**
	 * @param areaColor the areaColor to set
	 */
	public void setAreaColor(int areaColor) {
		this.areaColor = areaColor;
	}

	/**
	 * @return the placeMap
	 */
	public PlaceMap getPlaceMap() {
		return placeMap;
	}

	/**
	 * @param placeMap the placeMap to set
	 */
	public void setPlaceMap(PlaceMap placeMap) {
		this.placeMap = placeMap;
	}

	/**
	 * @return the itemId
	 */
	public String getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	/**
	 * @return the areaDate
	 */
	public String getAreaDate() {
		return areaDate;
	}

	/**
	 * @param areaDate the areaDate to set
	 */
	public void setAreaDate(String areaDate) {
		this.areaDate = areaDate;
	}

	/**
	 * @return the placeID
	 */
	public String getPlaceID() {
		return placeID;
	}

	/**
	 * @param placeID the placeID to set
	 */
	public void setPlaceID(String placeID) {
		this.placeID = placeID;
	}
	
	
	
	
	

}
