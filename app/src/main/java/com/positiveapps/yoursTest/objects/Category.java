/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.res.TypedArray;
import android.util.Log;

import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;

/**
 * @author natiapplications
 *
 */
public class Category implements Serializable{
	
	
	public static final int[] categoryBars = {R.drawable.family_bar,R.drawable.prochases_bar,R.drawable.fun_bar,R.drawable.hoby_bar};
	public static final int[] circle_frame = {R.drawable.circle_blue,R.drawable.circle_payper,R.drawable.circle_cry,R.drawable.circle_green,R.drawable.circle_orange};
	public static final String[] CATEGORIES_NAMES = YoursApp.appContext.getResources().getStringArray(R.array.categories_names);
	public static final TypedArray CATEGORIES_ORDER_BUTTONS = YoursApp.appContext.getResources().obtainTypedArray(R.array.order_buttons);
	public static final TypedArray CATEGORIES_INFO_BARS = YoursApp.appContext.getResources().obtainTypedArray(R.array.infromation_bars);
	public static final TypedArray CATEGORIES_DETAILS_BOX = YoursApp.appContext.getResources().obtainTypedArray(R.array.details_boxs);
	public static final TypedArray CATEGORIES_CONTINUE_BUTTONS = YoursApp.appContext.getResources().obtainTypedArray(R.array.continue_bottons);
	public static final TypedArray CATEGORIES_ADD_NUTTONS = YoursApp.appContext.getResources().obtainTypedArray(R.array.add_bottons);
	public static final TypedArray CATEGORIES_MINUS_BUTTONS = YoursApp.appContext.getResources().obtainTypedArray(R.array.minus_bottons);
	public static final TypedArray CATEGORIES_TIME_LIST_BG = YoursApp.appContext.getResources().obtainTypedArray(R.array.time_list_bg);
	
	public static final int CATEGORY_FAMILLY = 0;
	public static final int CATEGORY_PROCHASES = 1;
	public static final int CATEGORY_FUN = 2;
	public static final int CATEGORY_HOBY = 3;
	public static final int CATEGORY_HOT_MONTH = 4;
	
	
	public static final int KEY_MENU_FAMILLY = 1;
	public static final int KEY_MENU_PROCHASES = 2;
	public static final int KEY_MENU_FUN = 3;
	public static final int KEY_MENU_HOBY = 4;
	public static final int KEY_HOT_MONTH = 5;
	
	private final String GET_SUBJECT_RESULT = "GetSubjectOfListResult";
	

	private String categoryName;
	private int categoryKeyMenu;
	private int categoryType;
	private boolean isHasData;
	private boolean isDone;
	
	private ArrayList<SubCategory> subCategories = new ArrayList<SubCategory>();
	
	public Category (String categoryName,int categoryKeyMenu,int CategoryType){
		this.categoryName = categoryName;
		this.categoryKeyMenu = categoryKeyMenu;
		this.categoryType = CategoryType;
	
	}
	
	public void initCategory (int subMenuId) {
		
		if (categoryType == CATEGORY_HOT_MONTH){
			subMenuId = 12;
		}
		YoursApp.networkManager.getSubCategoryDetails(String.valueOf(categoryKeyMenu), String.valueOf(subMenuId), new NetworkCallback() {
		
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				isDone = true;
				try {
					System.out.println("niv check:= !!!!!!!!!!!!!!"+response.getXmlContent());

					JSONObject jsonObject = new JSONObject(response.getXmlContent());
					JSONArray subsJsonArray = new JSONArray(jsonObject.optString(GET_SUBJECT_RESULT));
					for (int i = 0; i < subsJsonArray.length(); i++) {
						JSONObject temp = new JSONObject(subsJsonArray.optString(i));
						SubCategory subTemp = new SubCategory(categoryType, categoryKeyMenu, temp);
						subCategories.add(subTemp);
						System.out.println("niv check:= !!!!!!!subTemp!!!!!!!" +subTemp.toString());

					}
					isHasData = true;
				} catch (Exception e) {
					isHasData = false;
					Log.d("exloglog", "ex = " + e.getMessage());
				}
			}
			
			
			@Override
			public void onError() {
				super.onError();
				isHasData = false;
				isDone = true;
				Log.i("exloglog", "error getting data in category = " + categoryType);
			}
			
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				isHasData = false;
				isDone = true;
				Log.d("exloglog", "error getting data in category = " + categoryType);
			}
			
		});
		
	}

	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}

	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	/**
	 * @return the categoryKeyMenu
	 */
	public int getCategoryKeyMenu() {
		return categoryKeyMenu;
	}

	/**
	 * @param categoryKeyMenu the categoryKeyMenu to set
	 */
	public void setCategoryKeyMenu(int categoryKeyMenu) {
		this.categoryKeyMenu = categoryKeyMenu;
	}

	/**
	 * @return the categoryType
	 */
	public int getCategoryType() {
		return categoryType;
	}

	/**
	 * @param categoryType the categoryType to set
	 */
	public void setCategoryType(int categoryType) {
		this.categoryType = categoryType;
	}

	/**
	 * @return the isHasData
	 */
	public boolean isHasData() {
		return isHasData;
	}

	/**
	 * @param isHasData the isHasData to set
	 */
	public void setHasData(boolean isHasData) {
		this.isHasData = isHasData;
	}

	/**
	 * @return the isDone
	 */
	public boolean isDone() {
		return isDone;
	}

	/**
	 * @param isDone the isDone to set
	 */
	public void setDone(boolean isDone) {
		this.isDone = isDone;
	}

	/**
	 * @return the subCategories
	 */
	public ArrayList<SubCategory> getSubCategories() {
		return subCategories;
	}

	/**
	 * @param subCategories the subCategories to set
	 */
	public void setSubCategories(ArrayList<SubCategory> subCategories) {
		this.subCategories = subCategories;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Category [categoryName=" + categoryName + ", categoryKeyMenu="
				+ categoryKeyMenu + ", categoryType=" + categoryType
				+ ", isHasData=" + isHasData + ", isDone=" + isDone
				+ ", subCategories=" + subCategories + "]";
	}
	
	
	
	

}
