/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONObject;


import com.positiveapps.yoursTest.parser.AreaMapParser;

/**
 * @author natiapplications
 *
 */
public class ExtendedYoursItem implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String AREA_ID = "ShetahId";
	private final String AREA_NAME = "ShetahName";
	private final String COORD_XY = "Coord_xy";
	private final String CITY = "City";
	private final String DATE_LENGTH = "Date_length";
	private final String dATE_LENGTH = "date_length";
	private final String TIME_LENGTH = "Time_length";
	private final String PLACE_NAME = "VenueName";
	private final String STOKE_STATE = "Azal";
	private final String CLUB_PRICE = "Club_price";
	private final String SALE_KIND = "SaleKind";
	private String ADRRESS = "Address";
	
	
	private int index;
	private String cord_xy;
	private Area area;
	private ItemDate date;
	//private ArrayList<SeatRow> areaMap;
	private PlaceMap placeMap;
	
	
	public ExtendedYoursItem (JSONObject toParse,int index,int status){
		this.index = index;
		try {
			area = new Area(index);
			area.setAreaId(toParse.optString(AREA_ID));
			area.setAreaName(toParse.optString(AREA_NAME));
			area.setPlace(toParse.optString(PLACE_NAME));
			area.setPlaceID(String.valueOf(toParse.optInt("Id")));
			date = new ItemDate();
			date.setAddress(toParse.optString(ADRRESS));
			date.setCity(toParse.optString(CITY));
			date.setName(toParse.optString("Name"));
			String dateString = toParse.optString(DATE_LENGTH);
			if (dateString.isEmpty()){
				dateString = toParse.optString(dATE_LENGTH);
			}
			if (!dateString.isEmpty()){
				dateString = dateString.split("T")[0];
			}
			
			
			date.setDate(dateString);
			date.setId(toParse.optInt("Id"));
			date.setTime(toParse.optString(TIME_LENGTH));
			
			area.setAreaDate(date.getDate()+date.getTime());
			
			date.setStatus(status);			
			String stokeStatus = toParse.optString(STOKE_STATE);
			if (stokeStatus.equalsIgnoreCase("out of stock")){
				date.setItemIsOver(true);
			}else{
				date.setItemIsOver(false);
			}
			
			date.setPlace(toParse.optString(PLACE_NAME));
			date.setPrice(toParse.optInt("Price"));
			date.setSaleKind(toParse.optString(SALE_KIND));
			date.setClubPrice(toParse.optInt(CLUB_PRICE));
			
			cord_xy = toParse.optString(COORD_XY);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append("<root>");
			stringBuilder.append(cord_xy);
			stringBuilder.append("</root>");
			cord_xy = stringBuilder.toString();
			placeMap = new PlaceMap();
			placeMap.setPlace(toParse.optString(PLACE_NAME));
			ArrayList<SeatRow> areaMap = new AreaMapParser(cord_xy).parse();
			placeMap.setRows(areaMap);
			
			area.setPlaceMap(placeMap);
			String itemID = toParse.optString("id");
			if (itemID.isEmpty()){
				itemID = toParse.optString("Id");
			}
			area.setItemId(itemID);
			
			
		} catch (Exception e) {}
	}



	/**
	 * @return the cord_xy
	 */
	public String getCord_xy() {
		return cord_xy;
	}



	/**
	 * @param cord_xy the cord_xy to set
	 */
	public void setCord_xy(String cord_xy) {
		this.cord_xy = cord_xy;
	}



	/**
	 * @return the area
	 */
	public Area getArea() {
		return area;
	}



	/**
	 * @param area the area to set
	 */
	public void setArea(Area area) {
		this.area = area;
	}



	/**
	 * @return the date
	 */
	public ItemDate getDate() {
		return date;
	}



	/**
	 * @param date the date to set
	 */
	public void setDate(ItemDate date) {
		this.date = date;
	}



	/**
	 * @return the placeMap
	 */
	public PlaceMap getPlaceMap() {
		return placeMap;
	}



	/**
	 * @param placeMap the placeMap to set
	 */
	public void setPlaceMap(PlaceMap placeMap) {
		this.placeMap = placeMap;
	}


	

	
	
	
	
	
	
}
