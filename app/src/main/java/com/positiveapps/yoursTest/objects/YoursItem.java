/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.positiveapps.yoursTest.fragments.purchase.PurchaseChooseSeatPashbarFragment;
import com.positiveapps.yoursTest.util.TextUtil;

import android.util.Log;

/**
 * @author natiapplications
 *
 */
public class YoursItem implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int STATUS_SINGEL = 1;
	public static final int STATUS_MOLTIPLE = 2;
	public static final int STATUS_BUNNER = 3;
	public static final int STATUS_EXTERNAL = 4;
	public static final int STATUS_PASHBAR = 5;
	
	
	public static final int KEY_ORDER_BUNNER = 1;
	public static final int KEY_ORDER_SEATS = 2;
	public static final int KEY_ORDER_GENERAL = 3;
	
	
	
	private String NAME = "Name";
	private String LOCATION = "Location";
	private String DESCRIPTION = "Description";
	private String KIND = "Kind";
	private String PRICE = "Price";
	private String CLUB_PRICE = "ClubPrice";
	private String CLUBPRICE = "Club_price";
	private String SALE_KIND = "SaleKind";
	private String TIME_LENGTH = "Time_length";
	private String DATE_LENGTH = "Date_length";
	private String IMAGE = "Image";
	private String ID = "Id";
	private String STATUS = "status";
	private String CHOOSABLE = "seatReserv";
	private String SUBJECT_ID = "subjectid";
	private String KEY_MENU = "keyMenu";
	private String SUB_MENU_ID = "subMenuId";
	private String MAX_BUY = "maxBuy";
	private String EXTERNAL_LINK = "externalLink";
	private String ADRRESS = "Address";
	
	
	private String GET_SUBJECT_DETAILS_RESULT = "GetSubjectDetailResult";
	
	
	private String ERROR = "Error";
	private String ERROR_DESC = "ErrDesc";
	
	private String subCategoryName;
	private int subCateroryMenuId;
	private int parentKeyMenu;
	private int parentType;
	
	private String itemName;
	private String itemLocation;
	private String itemDesc;
	private String longDesc = "";
	private String itemKind;
	private int itemOriginalPrice;
	private int itemClubPrice;
	private String itemSaleKind;
	private int timeLength;
	private String dateLength;
	private String imageUrl;
	private int id;
	private int subjectId;
	private int keyMenu;
	private String subMenuId;
	private int actionNumber;
	private int maxOrder;
	private String bannerImage;
	private String address;
	
	private ArrayList<ExtendedYoursItem> extendedYoursItems = new ArrayList<ExtendedYoursItem>();
	
	private boolean isHasError;
	private String errorDesc;
	private String externalLink;
	private String sessionID;
	
	private boolean isAnimated;
	private int status;
	private boolean choosable;
	
	
	public YoursItem (String subCategoryName,int parentKeyMenu, int parentType, JSONObject toParse){
		this.subCategoryName = subCategoryName;
		this.parentKeyMenu = parentKeyMenu;
		this.parentType = parentType;
		try {
			itemName = toParse.optString(NAME);
			itemLocation = toParse.optString(LOCATION);
			itemDesc = toParse.optString(DESCRIPTION);
			itemKind = toParse.optString(KIND);
			
			itemOriginalPrice = toParse.optInt(PRICE,0);
			itemClubPrice = toParse.optInt(CLUB_PRICE,0);
			
			if (itemName.trim().equalsIgnoreCase("רולדין")){
				Log.e("pricelog", "parse price  = " + toParse.optInt(CLUB_PRICE,0) + "  s  " + itemClubPrice);
			}
			
			
			itemSaleKind = toParse.optString(SALE_KIND);
			timeLength = toParse.optInt(TIME_LENGTH);
			dateLength = toParse.optString(DATE_LENGTH);
			imageUrl = toParse.optString(IMAGE);
			imageUrl = TextUtil.stringByAddingPercentEscapesUsingEncoding(imageUrl, HTTP.UTF_8);
			id = toParse.optInt(ID);
			subjectId = toParse.optInt(SUBJECT_ID);
			keyMenu = toParse.optInt(KEY_MENU);
			subMenuId = toParse.optString(SUB_MENU_ID);
			maxOrder = toParse.optInt(MAX_BUY);
			
			bannerImage = toParse.optString(DESCRIPTION);
			externalLink = toParse.optString(EXTERNAL_LINK);
			
			String status = toParse.optString(STATUS);
			
			if (status != null) {
				if (status.equalsIgnoreCase("single")) {
					this.status = STATUS_SINGEL;
				} else if (status.equalsIgnoreCase("multi")) {
					this.status = STATUS_MOLTIPLE;
				} else if (status.equalsIgnoreCase("banner")) {
					this.status = STATUS_BUNNER;
				} else if (status.equalsIgnoreCase("external")) {
					this.status = STATUS_EXTERNAL;
				} else if (status.equalsIgnoreCase("pashbar")) {
					this.status = STATUS_PASHBAR;
				}
			}
			
			String resiveSeats = toParse.optString(CHOOSABLE);
			if (resiveSeats.equalsIgnoreCase("Y")){
				choosable = true;
			}
			
			isHasError = toParse.optBoolean(ERROR);
			errorDesc = toParse.optString(ERROR_DESC);
			address = toParse.optString(ADRRESS);
			} catch (Exception e) {
			Log.d("categoreisLog", "error getting data in sub item = " + itemName);
		}
		
		
	}


	/**
	 * @return the subCategoryName
	 */
	public String getSubCategoryName() {
		return subCategoryName;
	}


	/**
	 * @param subCategoryName the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}


	/**
	 * @return the subCateroryMenuId
	 */
	public int getSubCateroryMenuId() {
		return subCateroryMenuId;
	}


	/**
	 * @param subCateroryMenuId the subCateroryMenuId to set
	 */
	public void setSubCateroryMenuId(int subCateroryMenuId) {
		this.subCateroryMenuId = subCateroryMenuId;
	}


	/**
	 * @return the parentKeyMenu
	 */
	public int getParentKeyMenu() {
		return parentKeyMenu;
	}


	/**
	 * @param parentKeyMenu the parentKeyMenu to set
	 */
	public void setParentKeyMenu(int parentKeyMenu) {
		this.parentKeyMenu = parentKeyMenu;
	}


	/**
	 * @return the parentType
	 */
	public int getParentType() {
		return parentType;
	}


	/**
	 * @param parentType the parentType to set
	 */
	public void setParentType(int parentType) {
		if (parentType > 4){
			parentType = 4;
		}
		this.parentType = parentType;
	}


	/**
	 * @return the itemName
	 */
	public String getItemName() {
		return itemName;
	}


	/**
	 * @param itemName the itemName to set
	 */
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


	/**
	 * @return the itemLocation
	 */
	public String getItemLocation() {
		return itemLocation;
	}


	/**
	 * @param itemLocation the itemLocation to set
	 */
	public void setItemLocation(String itemLocation) {
		this.itemLocation = itemLocation;
	}


	/**
	 * @return the itemDesc
	 */
	public String getItemDesc() {
		return itemDesc;
	}


	/**
	 * @param itemDesc the itemDesc to set
	 */
	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}


	/**
	 * @return the itemKind
	 */
	public String getItemKind() {
		return itemKind;
	}


	/**
	 * @param itemKind the itemKind to set
	 */
	public void setItemKind(String itemKind) {
		this.itemKind = itemKind;
	}


	/**
	 * @return the itemOriginalPrice
	 */
	public int getItemOriginalPrice() {
		return (int) itemOriginalPrice;
	}


	/**
	 * @param itemOriginalPrice the itemOriginalPrice to set
	 */
	public void setItemOriginalPrice(int itemOriginalPrice) {
		this.itemOriginalPrice = itemOriginalPrice;
	}


	/**
	 * @return the itemClubPrice
	 */
	public int getItemClubPrice() {
		return (int) itemClubPrice;
	}


	/**
	 * @param itemClubPrice the itemClubPrice to set
	 */
	public void setItemClubPrice(int itemClubPrice) {
		this.itemClubPrice = itemClubPrice;
	}


	/**
	 * @return the itemSaleKind
	 */
	public String getItemSaleKind() {
		return itemSaleKind;
	}


	/**
	 * @param itemSaleKind the itemSaleKind to set
	 */
	public void setItemSaleKind(String itemSaleKind) {
		this.itemSaleKind = itemSaleKind;
	}


	/**
	 * @return the timeLength
	 */
	public int getTimeLength() {
		return timeLength;
	}


	/**
	 * @param timeLength the timeLength to set
	 */
	public void setTimeLength(int timeLength) {
		this.timeLength = timeLength;
	}


	/**
	 * @return the imageUrl
	 */
	public String getImageUrl() {
		return imageUrl;
	}


	/**
	 * @param imageUrl the imageUrl to set
	 */
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the keyMenu
	 */
	public int getKeyMenu() {
		return keyMenu;
	}


	/**
	 * @param keyMenu the keyMenu to set
	 */
	public void setKeyMenu(int keyMenu) {
		this.keyMenu = keyMenu;
	}


	/**
	 * @return the subMenuId
	 */
	public String getSubMenuId() {
		return subMenuId;
	}


	/**
	 * @param subMenuId the subMenuId to set
	 */
	public void setSubMenuId(String subMenuId) {
		this.subMenuId = subMenuId;
	}

	

	/**
	 * @return the maxOrder
	 */
	public int getMaxOrder() {
		return maxOrder;
	}


	/**
	 * @param maxOrder the maxOrder to set
	 */
	public void setMaxOrder(int maxOrder) {
		this.maxOrder = maxOrder;
	}


	/**
	 * @return the isHasError
	 */
	public boolean isHasError() {
		return isHasError;
	}


	/**
	 * @param isHasError the isHasError to set
	 */
	public void setHasError(boolean isHasError) {
		this.isHasError = isHasError;
	}

	

	/**
	 * @return the subjectId
	 */
	public int getSubjectId() {
		return subjectId;
	}


	/**
	 * @param subjectId the subjectId to set
	 */
	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}


	/**
	 * @return the isAnimated
	 */
	public boolean isAnimated() {
		return isAnimated;
	}


	/**
	 * @param isAnimated the isAnimated to set
	 */
	public void setAnimated(boolean isAnimated) {
		this.isAnimated = isAnimated;
	}

	

	/**
	 * @return the dateLength
	 */
	public String getDateLength() {
		return dateLength;
	}


	/**
	 * @param dateLength the dateLength to set
	 */
	public void setDateLength(String dateLength) {
		this.dateLength = dateLength;
	}


	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}


	/**
	 * @return the choosable
	 */
	public boolean isChoosable() {
		return choosable;
	}


	/**
	 * @param choosable the choosable to set
	 */
	public void setChoosable(boolean choosable) {
		this.choosable = choosable;
	}
	
	
	/**
	 * @return the actionNumber
	 */
	public int getActionNumber() {
		return actionNumber;
	}


	/**
	 * @param actionNumber the actionNumber to set
	 */
	public void setActionNumber(int actionNumber) {
		this.actionNumber = actionNumber;
	}

	

	/**
	 * @return the longDesc
	 */
	public String getLongDesc() {
		return longDesc;
	}


	/**
	 * @param longDesc the longDesc to set
	 */
	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}
	
	


	/**
	 * @return the bannerImage
	 */
	public String getBannerImage() {
		return bannerImage;
	}


	/**
	 * @param bannerImage the bannerImage to set
	 */
	public void setBannerImage(String bannerImage) {
		this.bannerImage = bannerImage;
	}


	public ArrayList<Area> getAreasByPlaceAndDate (String place,String date){
		ArrayList<Area> result = new ArrayList<Area>();
		for (int i = 0; i < extendedYoursItems.size(); i++) {
			Area temp = extendedYoursItems.get(i).getArea();
			if (temp.getPlace().equalsIgnoreCase(place)&&temp.getAreaDate().equalsIgnoreCase(date)) {
				result.add(temp);
			}
		}
		HashMap<String, Area> fillt = new HashMap<String,Area>();
		for (int i = 0; i < result.size(); i++) {
			Area temp = result.get(i);
			fillt.put(temp.getAreaName(), temp);
		}
		result.clear();
		Set<String> keys = fillt.keySet();
		for(String key:keys){
			result.add(fillt.get(key));
		}
		return result;
	}
	
	public ArrayList<ItemDate> getDates (){
		ArrayList<ItemDate> result = new ArrayList<ItemDate>(); 
		for (int i = 0; i < extendedYoursItems.size(); i++) {
			result.add(extendedYoursItems.get(i).getDate());
		}
		
		HashMap<String, ItemDate> fillt = new HashMap<String,ItemDate>();
		for (int i = 0; i < result.size(); i++) {
			ItemDate temp = result.get(i);
			fillt.put(temp.toString(), temp);
		}
		result.clear();
		Set<String> keys = fillt.keySet();
		for(String key:keys){
			result.add(fillt.get(key));
		}
		Collections.sort(result, new Comparator<ItemDate>() {

			@Override
			public int compare(ItemDate lhs, ItemDate rhs) {
				// TODO Auto-generated method stub
				return lhs.getDateInMillis().compareTo(rhs.getDateInMillis());
			}
		});
		return result;
	}
	
	
	public PlaceMap getPlaceMapByPlaceName (String placeName){
		for (int i = 0; i < extendedYoursItems.size(); i++) {
			PlaceMap temp = extendedYoursItems.get(i).getPlaceMap();
			if (temp.getPlace().equalsIgnoreCase(placeName)&&temp.isHasData()){
				return temp;
			}
		}
		PlaceMap deafult = new PlaceMap();
		deafult.setRows(new ArrayList<SeatRow>());
		deafult.setPlace("");
		return deafult;
	}
	
	
	public void addFullDeatails (JSONObject moreDetails) {
		
		try {
			JSONArray jsonArray = new JSONArray(
					moreDetails.optString(GET_SUBJECT_DETAILS_RESULT));
			Log.e("extendedSizelog", "after parse");
			extendedYoursItems.clear();
			for (int i = 0; i < jsonArray.length(); i++) {
				extendedYoursItems.add(new ExtendedYoursItem(new JSONObject(jsonArray.optString(i)),i,getStatus()));
			}
			Log.e("extendedSizelog", "size = " + extendedYoursItems.size());
			JSONObject firstItem = new JSONObject(jsonArray.optString(0));
			if (firstItem != null){
				this.longDesc = firstItem.optString(DESCRIPTION).trim();
				this.id = firstItem.optInt(ID);
				
				/*String priceS = firstItem.optInt(PRICE,0)+"";
				String clubPriceS = firstItem.optInt(CLUB_PRICE,0)+"";
				Log.e("pricelog", "price = " + priceS);
				this.itemOriginalPrice = Integer.parseInt(priceS.split(".")[0]);
				this.itemClubPrice = Integer.parseInt(clubPriceS.split(".")[0]);*/
				
				this.itemOriginalPrice = firstItem.optInt(PRICE,0);
				this.itemOriginalPrice = firstItem.optInt(PRICE,0);
				this.itemClubPrice = firstItem.optInt(CLUBPRICE,0);
				if (itemName.trim().equalsIgnoreCase("רולדין")){
					Log.e("pricelog", "parse price  = " + firstItem.optInt(CLUBPRICE,0) + "  s  " + itemClubPrice);
				}
				this.itemName = firstItem.optString(NAME);
				/*try {
					this.itemClubPrice = Double.parseDouble(TextUtil.getNumberFromString(firstItem.optString(CLUBPRICE)));
				} catch (Exception e) {}*/
				
				this.maxOrder = firstItem.optInt(MAX_BUY);
				if (status == YoursItem.STATUS_BUNNER){
					String image = firstItem.optString("Img_link");
					if (!image.contains("http")){
						image = "http://www.yours.co.il/" + image;
					}
					try {
						bannerImage = TextUtil.stringByAddingPercentEscapesUsingEncoding(image, HTTP.UTF_8);
						
					} catch (UnsupportedEncodingException e) {
						bannerImage = image;
					}
				}
			}
			
			
			
			
		} catch (JSONException e) {
			Log.e("mapAreaLog", "ex = " + e.getMessage() );
		}
	}
	
	
	
	
	/**
	 * @return the externalLink
	 */
	public String getExternalLink() {
		return externalLink;
	}


	/**
	 * @param externalLink the externalLink to set
	 */
	public void setExternalLink(String externalLink) {
		this.externalLink = externalLink;
	}

    
	/**
	 * @return the sessionID
	 */
	public String getSessionID() {
		return sessionID;
	}


	/**
	 * @param sessionID the sessionID to set
	 */
	public void setSessionID(String url,int type) {
		
		if (type == PurchaseChooseSeatPashbarFragment.TYPE_PASHBAR){
			Log.i("sessionparslog", "url = " + url);
			String[] parameters = url.split(Pattern.quote("&"));
			Log.i("sessionparslog", "param = null ? " + (parameters==null));
			if (parameters != null){
				Log.i("sessionparslog", "size = " + parameters.length );
				for (int i = 0; i < parameters.length; i++) {
					Log.i("sessionparslog", "param = " + parameters[i]);
					if (parameters[i].contains("session_id")){
						this.sessionID = parameters[i].replace("session_id=", "");
						
						Log.i("sessionparslog", "id = " + this.sessionID );
					}
				}
			}
		}else if (type == PurchaseChooseSeatPashbarFragment.TYPE_CINEMA){
			Log.i("sessionparslog", "url = " + url);
			String[] parameters = url.split(Pattern.quote("?"));
			Log.i("sessionparslog", "param = null ? " + (parameters==null));
			if (parameters != null){
				Log.i("sessionparslog", "size = " + parameters.length );
				for (int i = 0; i < parameters.length; i++) {
					Log.i("sessionparslog", "param = " + parameters[i]);
					if (parameters[i].contains("sessionId")){
						this.sessionID = parameters[i].replace("sessionId=", "");
						
						Log.i("sessionparslog", "id = " + this.sessionID );
					}
				}
			}
		}
		
		
	}


	@Override
	public String toString() {
		return "YoursItem [subCategoryName=" + subCategoryName
				+ ", subCateroryMenuId=" + subCateroryMenuId
				+ ", parentKeyMenu=" + parentKeyMenu + ", parentType="
				+ parentType + ", itemName=" + itemName + ", itemLocation="
				+ itemLocation + ", itemDesc=" + itemDesc + ", itemKind="
				+ itemKind + ", itemOriginalPrice=" + itemOriginalPrice
				+ ", itemClubPrice=" + itemClubPrice + ", itemSaleKind="
				+ itemSaleKind + ", timeLength=" + timeLength + ", dateLength="
				+ dateLength + ", imageUrl=" + imageUrl + ", id=" + id
				+ ", subjectId=" + subjectId + ", keyMenu=" + keyMenu
				+ ", subMenuId=" + subMenuId + ", isHasError=" + isHasError
				+ ", errorDesc=" + errorDesc + "]";
	}
	
	
	

}
