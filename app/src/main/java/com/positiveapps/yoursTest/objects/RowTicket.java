/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class RowTicket implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int row;
	private int seat;
	private String ticketType;
	private int month;
	private double price;
	public RowTicket(int row, int seat, String ticketType, int month,
			double price) {
		super();
		this.row = row;
		this.seat = seat;
		this.ticketType = ticketType;
		this.month = month;
		this.price = price;
	}
	public RowTicket() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}
	/**
	 * @param row the row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}
	/**
	 * @return the seat
	 */
	public int getSeat() {
		return seat;
	}
	/**
	 * @param seat the seat to set
	 */
	public void setSeat(int seat) {
		this.seat = seat;
	}
	/**
	 * @return the ticketType
	 */
	public String getTicketType() {
		return ticketType;
	}
	/**
	 * @param ticketType the ticketType to set
	 */
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	

}
