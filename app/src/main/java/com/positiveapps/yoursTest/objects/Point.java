/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class Point implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public int x;
	public int y;
	
	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	};
	
	
}
