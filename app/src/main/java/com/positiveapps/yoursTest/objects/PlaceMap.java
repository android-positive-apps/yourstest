/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author natiapplications
 *
 */
public class PlaceMap implements Serializable{
	
	
	private ArrayList<SeatRow> rows;
	private String place;
	
	/**
	 * @return the rows
	 */
	public ArrayList<SeatRow> getRows() {
		return rows;
	}
	/**
	 * @param rows the rows to set
	 */
	public void setRows(ArrayList<SeatRow> rows) {
		this.rows = rows;
	}
	/**
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}
	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		this.place = place;
	}
	
	public boolean isHasData()
	{
		if (this.rows != null && this.rows.size() >0 ){
			return true;
		}
		return false;
	}	
	

}
