/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;

import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;

/**
 * @author natiapplications
 *
 */
public class Seat implements Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static int[] SEAT_ICONS = {R.drawable.empty,R.drawable.seat_full,R.drawable.seat_selector};
	public static final int STATUS_FREE = 2;
	public static final int STATUS_FULL = 1;
	public static final int STATUS_EMPTY = 0;
	
	
	private Point index;
	private int status;
	private boolean isSelected;
	private double price;
	private boolean hasPrice;
	private int seatNumber;
	private int size;
	private int rowIndex;
	
	public Seat() {
		super();
		size = 60;
		// TODO Auto-generated constructor stub
	}

	public Seat(Point index, int status, boolean isSelected) {
		super();
		this.index = index;
		this.status = status;
		this.isSelected = isSelected;
	}

	/**
	 * @return the index
	 */
	public Point getIndex() {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex(Point index) {
		this.index = index;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the isSelected
	 */
	public boolean isSelected() {
		return isSelected;
	}

	/**
	 * @param isSelected the isSelected to set
	 */
	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}
	
	
	

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * @return the hasPrice
	 */
	public boolean isHasPrice() {
		return hasPrice;
	}

	/**
	 * @param hasPrice the hasPrice to set
	 */
	public void setHasPrice(boolean hasPrice) {
		this.hasPrice = hasPrice;
	}

	
	
	/**
	 * @return the seatNumber
	 */
	public int getSeatNumber() {
		return seatNumber;
	}
	
	

	/**
	 * @return the size
	 */
	public int getSize() {
		return size;
	}

	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}

	/**
	 * @param seatNumber the seatNumber to set
	 */
	public void setSeatNumber(int seatNumber) {
		this.seatNumber = seatNumber;
	}

	public View getView (final OnSeatClickListener listener){
		final ImageView imageView = new ImageView(MainActivity.mainInstance);
		imageView.setBackgroundResource(SEAT_ICONS[status]);
		imageView.setScaleType(ScaleType.FIT_XY);
		imageView.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				listener.onSeatClick(Seat.this, imageView);
			}
		});
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size,size);
		params.gravity = Gravity.CENTER;
		imageView.setLayoutParams(params);
		if (isSelected){
			imageView.setSelected(true);
		}
		return imageView;
	}
	
	public interface OnSeatClickListener {
		public void onSeatClick(Seat seat, View view);
	}

	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(seatNumber);
		stringBuilder.append(",");
		stringBuilder.append(rowIndex);
		stringBuilder.append(",");
		stringBuilder.append((int)price+"");
		stringBuilder.append(",0,");
		stringBuilder.append((int)price+"");
		return stringBuilder.toString();
	}

	/**
	 * @return the rowIndex
	 */
	public int getRowIndex() {
		return rowIndex;
	}

	/**
	 * @param rowIndex the rowIndex to set
	 */
	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	
}
