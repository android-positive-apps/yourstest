/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

import org.json.JSONObject;

/**
 * @author natiapplications
 *
 */
public class FullSeat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String ID = "id";
	private final String X = "x";
	private final String Y = "y";
	private final String M_STATUS = "m_status";
	private final String SESSION_ID = "session_id";
	private final String SOOG = "soog";
	private final String PRICE = "price";
	
	
	private int id;
	private int x;
	private int y;
	private int status;
	private String sessionId;
	private int soog;
	private double price;
	
	public FullSeat (JSONObject toParse){
		id = toParse.optInt(ID);
		x = toParse.optInt(X);
		y = toParse.optInt(Y);
		status = toParse.optInt(M_STATUS);
		sessionId = toParse.optString(SESSION_ID);
		soog = toParse.optInt(SOOG);
		price = toParse.optInt(PRICE);
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the sessionId
	 */
	public String getSessionId() {
		return sessionId;
	}

	/**
	 * @param sessionId the sessionId to set
	 */
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return the soog
	 */
	public int getSoog() {
		return soog;
	}

	/**
	 * @param soog the soog to set
	 */
	public void setSoog(int soog) {
		this.soog = soog;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	

}
