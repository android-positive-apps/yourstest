/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

/**
 * @author natiapplications
 *
 */
public class SubCategory implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final String CATEGORY = "Category";
	private final String LIST = "List";
	
	private String subCategoryName;
	private int subCategoryMenuId;
	private int parentType;
	private int parentKeyMenu;
	
	private ArrayList<YoursItem> items = new ArrayList<YoursItem>();
	
	public SubCategory (int parentType, int parentKeyMenu,JSONObject toParse){
		this.parentType = parentType;
		this.parentKeyMenu = parentKeyMenu;
		
		try {
			subCategoryName = toParse.optString(CATEGORY);
			JSONArray itemsList = toParse.optJSONArray(LIST);
			for (int i = 0; i < itemsList.length(); i++) {
				JSONObject temp = new JSONObject(itemsList.optString(i));
				YoursItem yoursItem = new YoursItem(subCategoryName, parentKeyMenu, parentType, temp);
				items.add(yoursItem);
				System.out.println("niv check:= !!!!!!!!yoursItem!!!!!!" + yoursItem.toString());

			}
		} catch (Exception e) {
			Log.d("categoreisLog", "error getting data in sub category = " + subCategoryName);
		}
		
		
	}

	/**
	 * @return the subCategoryName
	 */
	public String getSubCategoryName() {
		return subCategoryName;
	}

	/**
	 * @param subCategoryName the subCategoryName to set
	 */
	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	/**
	 * @return the subCategoryMenuId
	 */
	public int getSubCategoryMenuId() {
		return subCategoryMenuId;
	}

	/**
	 * @param subCategoryMenuId the subCategoryMenuId to set
	 */
	public void setSubCategoryMenuId(int subCategoryMenuId) {
		this.subCategoryMenuId = subCategoryMenuId;
	}

	/**
	 * @return the parentType
	 */
	public int getParentType() {
		return parentType;
	}

	/**
	 * @param parentType the parentType to set
	 */
	public void setParentType(int parentType) {
		this.parentType = parentType;
	}

	/**
	 * @return the parentKeyMenu
	 */
	public int getParentKeyMenu() {
		return parentKeyMenu;
	}

	/**
	 * @param parentKeyMenu the parentKeyMenu to set
	 */
	public void setParentKeyMenu(int parentKeyMenu) {
		this.parentKeyMenu = parentKeyMenu;
	}

	/**
	 * @return the items
	 */
	public ArrayList<YoursItem> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(ArrayList<YoursItem> items) {
		this.items = items;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SubCategory [subCategoryName=" + subCategoryName
				+ ", subCategoryMenuId=" + subCategoryMenuId + ", parentType="
				+ parentType + ", parentKeyMenu=" + parentKeyMenu + ", items="
				+ items + "]";
	}
	
	

}
