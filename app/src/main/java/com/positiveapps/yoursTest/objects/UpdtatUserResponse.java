/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.network.ResponseObject;

/**
 * @author natiapplications
 * 
 */
public class UpdtatUserResponse extends ResponseObject implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private final String UPDATE_RESULT = "UpdateUserRegistrationResult";
	
	private final String FIRST_NAME = "FirstName";
	private final String USER_ID = "IdUser";
	private final String LAST_NAME = "LastName";
	private final String IDENT_USER = "IdentUser";
	private final String PASS = "Password";
	private final String STREET = "Street";
	private final String ZIP_CODE = "ZipNum";
	private final String CITY = "Town";
	private final String TELEPHONE = "Telephone";
	private final String MOBILE = "Mobile";
	private final String EMAIL = "Email";
	private final String USER_NUMBER = "Usernumber";
	private final String IS_PUBLIC = "Ispub";
	private final String ERROR = "Error";
	private final String ERROR_DESC = "ErrorDescription";

	public UpdtatUserResponse(JSONObject toParse) {
		super(toParse);
		JSONObject loginResult;
		Log.e("loginlog", "result = " + toParse.toString());
		try {
			loginResult = new JSONObject(toParse.optString(UPDATE_RESULT));
			/*int error = loginResult.optInt(ERROR,1);
			if (error  == 0){
				isHasError = false;
			}else{
				isHasError = true;
			}*/
			isHasError = loginResult.optBoolean(ERROR, true);
			errorDesc = loginResult.optString(ERROR_DESC);
			if (!isHasError) {
				YoursApp.userProfil.setUserFirstNaem(loginResult.optString(FIRST_NAME));
				YoursApp.userProfil.setUserId(loginResult.optString(USER_ID));
				YoursApp.userProfil.setUserLastName(loginResult.optString(LAST_NAME));
				//YoursApp.userProfil.setUserNumber(loginResult.optString(USER_NUMBER));
				YoursApp.userProfil.setIdentUser(loginResult.optString(IDENT_USER));
				YoursApp.userProfil.setUserPass(loginResult.optString(PASS));
				YoursApp.userProfil.setUserTelephone(loginResult.optString(TELEPHONE));
				YoursApp.userProfil.setUserPhone(loginResult.optString(MOBILE));
				YoursApp.userProfil.setUserAddress(loginResult.optString(STREET));
				YoursApp.userProfil.setUserCity(loginResult.optString(CITY));
				YoursApp.userProfil.setUserZipCode(loginResult.optString(ZIP_CODE));
				YoursApp.userProfil.setUserEmail(loginResult.optString(EMAIL));
				YoursApp.userProfil.setUserAccessLevel(loginResult.optBoolean(IS_PUBLIC));
			}

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	

}
