/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.util.ArrayList;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.Seat.OnSeatClickListener;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class SeatRow implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int index;
	private ArrayList<Seat> seats;
	
	public SeatRow(int index, ArrayList<Seat> seats) {
		super();
		this.index = index;
		this.seats = seats;
	}
	
	public SeatRow() {
		super();
		// TODO Auto-generated constructor stub
	}
	/**
	 * @return the index
	 */
	public int getIndex() {
		return index;
	}
	/**
	 * @param index the index to set
	 */
	public void setIndex(int index) {
		this.index = index;
	}
	/**
	 * @return the seats
	 */
	public ArrayList<Seat> getSeats() {
		return seats;
	}
	/**
	 * @param seats the seats to set
	 */
	public void setSeats(ArrayList<Seat> seats) {
		this.seats = seats;
	}
	
	public View getView(final OnSeatClickListener listener){
		View v = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.seat_row_view, null);
		TextView indexTxt = (TextView)v.findViewById(R.id.index);
		LinearLayout seatContainer = (LinearLayout)v.findViewById(R.id.row_seat_container);
		
		indexTxt.setText(String.valueOf(index));
		if (index == 0){
			indexTxt.setText(String.valueOf(" "));
		}
		for (int i = 0; i < seats.size(); i++) {
			seatContainer.addView(seats.get(i).getView(listener));
		}
		return v;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SeatRow [index=" + index + ", seats=" + seats + "]";
	}
	
	
	
	
}
