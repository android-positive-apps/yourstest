/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;

/**
 * @author natiapplications
 *
 */
public class RowGeneralItem implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String  name;
	private String ticketType;
	private int month;
	private double price;
	
	public RowGeneralItem(String name, String ticketType, int month,
			double price) {
		super();
		this.name = name;
		
		this.ticketType = ticketType;
		this.month = month;
		this.price = price;
	}
	public RowGeneralItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the ticketType
	 */
	public String getTicketType() {
		return ticketType;
	}
	/**
	 * @param ticketType the ticketType to set
	 */
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}
	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		this.month = month;
	}
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}
	
	

}
