/**
 * 
 */
package com.positiveapps.yoursTest.objects;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author natiapplications
 *
 */
public class ItemDate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String date;
	private String time;
	private double price;
	private String place;
	private String city;
	private String address;
	private boolean itemIsOver;
	private int id;
	private String saleKind;
	private int clubPrice;
	private int status;
	private String name;
	
	
	public ItemDate(int status,int id,String date, String time, double price, String place,
			boolean itemIsOver,String name) {
		super();
		this.name = name;
		this.date = date.trim();
		this.time = time.trim();
		this.price = price;
		this.place = place.trim();
		this.itemIsOver = itemIsOver;
		this.id = id;
		this.status = status;
		
	}


	public ItemDate() {
		super();
		// TODO Auto-generated constructor stub
	}


	/**
	 * @return the date
	 */
	public String getDate() {
		android.util.Log.e("DateLog","date = " + this.date);
		if (date != null&&!date.isEmpty()&&!date.contains("0001")){
			String result = "";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			try {
				Date d = simpleDateFormat.parse(date);
				simpleDateFormat.applyPattern("dd/MM/yyyy");
				result = simpleDateFormat.format(d);
			} catch (Exception e) {}
			android.util.Log.d("DateLog","date = " + result);
			return result;
		}else{
			return "";
		}
		
	}


	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}


	/**
	 * @return the time
	 */
	public String getTime() {
		if (time == null){
			return "";
		}
		return time;
	}


	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}


	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}


	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.price = price;
	}


	/**
	 * @return the place
	 */
	public String getPlace() {
		return place;
	}


	/**
	 * @param place the place to set
	 */
	public void setPlace(String place) {
		this.place = place;
	}


	/**
	 * @return the itemIsOver
	 */
	public boolean isItemIsOver() {
		return itemIsOver;
	}


	/**
	 * @param itemIsOver the itemIsOver to set
	 */
	public void setItemIsOver(boolean itemIsOver) {
		this.itemIsOver = itemIsOver;
	}


	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}


	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	
	
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	@Override
	public boolean equals(Object o) {
		
		if (o instanceof ItemDate){
			ItemDate itemDate = (ItemDate)o;
			if (this.getTime().equalsIgnoreCase(itemDate.getTime())&&
					this.getCity().equalsIgnoreCase(itemDate.getCity())&&
					this.getPlace().equalsIgnoreCase(itemDate.getPlace())){
				return true;
			}
		}
		return false;
	}


	public String getFullAddress () {
		return city +","+place;
	}
	
	@Override
	public String toString() {
		return date + "-" + time + "-" + place + "-" + city + "-" + saleKind + "-" + name;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the saleKind
	 */
	public String getSaleKind() {
		return saleKind;
	}


	/**
	 * @param saleKind the saleKind to set
	 */
	public void setSaleKind(String saleKind) {
		this.saleKind = saleKind;
	}


	/**
	 * @return the clubPrice
	 */
	public int getClubPrice() {
		return clubPrice;
	}


	/**
	 * @param clubPrice the clubPrice to set
	 */
	public void setClubPrice(int clubPrice) {
		this.clubPrice = clubPrice;
	}


	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}


	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	public Long getDateInMillis () {
		String dateAsString = getDate()+getTime();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyyHH:mm");
		try {
			Date d = simpleDateFormat.parse(dateAsString);
			if (d!= null){
				return d.getTime();
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new Long(0);
	}


	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}


	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
}
