/**
 * 
 */
package com.positiveapps.yoursTest.adapters;

import java.util.ArrayList;
import java.util.List;

import com.positiveapps.yoursTest.objects.DrawerItem;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * @author natiapplications
 *
 */
public class DrawerListAdapter extends ArrayAdapter<DrawerItem> {

	
	/**
	 * @param context
	 * @param resource
	 * @param objects
	 */
	public DrawerListAdapter(Context context, int resource,
			List<DrawerItem> objects) {
		super(context, resource, objects);
		// TODO Auto-generated constructor stub
		this.items = (ArrayList<DrawerItem>) objects;
	}


	private ArrayList<DrawerItem> items;
	
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return items.size();
	}
	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = items.get(position).getView();
		return convertView;
	}

}
