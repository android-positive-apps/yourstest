/**
 * 
 */
package com.positiveapps.yoursTest.adapters;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.objects.ItemDate;
import com.positiveapps.yoursTest.objects.YoursItem;

import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class ItemTimesListAdapter extends BaseAdapter {

	private ArrayList<ItemDate>  itemns;
	
	
	public ItemTimesListAdapter (ArrayList<ItemDate> itmens){
		this.itemns = itmens;
	}
	@Override
	public int getCount() {
		return itemns.size();
	}

	@Override
	public Object getItem(int index) {
		return itemns.get(index);
	}

	
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	@Override
	public View getView(int index, View v, ViewGroup arg2) {
		
		v = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.list_item_times, null);
		
		TextView date = (TextView)v.findViewById(R.id.date);
		TextView time = (TextView)v.findViewById(R.id.time);
		TextView price = (TextView)v.findViewById(R.id.price);
		TextView place = (TextView)v.findViewById(R.id.place);
		Button orderBtn = (Button)v.findViewById(R.id.order_btn);
		
		ItemDate temp = itemns.get(index);
		
		date.setText(temp.getDate());
		time.setText(temp.getTime());
		
		String placeContent = temp.getName();
		if (temp.getPlace() != null&&!temp.getPlace().isEmpty()){
			placeContent = temp.getPlace();
		}
		place.setText(placeContent);
		
		
		
		if (temp.isItemIsOver()){
			price.setTextColor(Color.RED);
			price.setText(YoursApp.appContext.getString(R.string.item_over));
			orderBtn.setVisibility(View.INVISIBLE);
		}else{
			String priceString  = new DecimalFormat("##.##").format(temp.getPrice());
			String clubPriceString  = new DecimalFormat("##.##").format(temp.getClubPrice());
			if (clubPriceString != null && !clubPriceString.isEmpty()
					&&!clubPriceString.equalsIgnoreCase(priceString)){
				price.setText("מוזל: " + priceString + " מועדון: " + clubPriceString);
			}else{
				price.setText(priceString);
			}
			
		}
		
		if (temp.getSaleKind() != null&&
				!temp.getSaleKind().isEmpty()&&
				!temp.getSaleKind().equalsIgnoreCase("null")&&
				temp.getStatus() != YoursItem.STATUS_PASHBAR){
			
			date.setText(temp.getSaleKind());
			date.setMaxLines(2);
			time.setText("");
			price.setText(MainActivity.mainInstance.getString(R.string.club_price) + " " + temp.getClubPrice());
			place.setText(MainActivity.mainInstance.getString(R.string.regular_price_type) + " " + (int)temp.getPrice());
		}
		return v;
	}

}
