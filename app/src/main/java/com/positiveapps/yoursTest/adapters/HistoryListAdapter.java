/**
 * 
 */
package com.positiveapps.yoursTest.adapters;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.HistoryProchase;
import com.positiveapps.yoursTest.util.DateUtil;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class HistoryListAdapter extends BaseAdapter {

	private ArrayList<HistoryProchase>  itemns;
	
	
	public HistoryListAdapter (ArrayList<HistoryProchase> itmens){
		this.itemns = itmens;
	}
	@Override
	public int getCount() {
		return itemns.size();
	}

	@Override
	public Object getItem(int index) {
		return itemns.get(index);
	}

	
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	@Override
	public View getView(int index, View v, ViewGroup arg2) {
		
		v = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.list_item_history, null);
		
		TextView date = (TextView)v.findViewById(R.id.date);
		TextView confirmNumber = (TextView)v.findViewById(R.id.confirm_number);
		TextView companyAndProduct = (TextView)v.findViewById(R.id.company_and_product);
		TextView priceAndCreditCard = (TextView)v.findViewById(R.id.price_and_credit_card);
		
		HistoryProchase temp = itemns.get(index);
		
		date.setText(DateUtil.getServerMillisAsDateString(temp.getBuy_date()));
		confirmNumber.setText(temp.getConfirmNumber());
		companyAndProduct.setText(temp.getName());
		
		String priceString  = new DecimalFormat("##.##").format(temp.getSamPrice());
		
		if (temp.getSamPrice() > 0 && temp.getSinglePrice() > 0){
			priceString  = new DecimalFormat("##.##").format(temp.getSamPrice());
		}else if (temp.getSamPrice() > 0 && temp.getSinglePrice() <=0){
			priceString  = new DecimalFormat("##.##").format(temp.getSamPrice());
		}else if (temp.getSamPrice() <= 0 && temp.getSinglePrice() > 0){
			priceString  = new DecimalFormat("##.##").format(temp.getSinglePrice());
		}else{
			priceString  = new DecimalFormat("##.##").format(temp.getSamPrice());
		}
		
		
		priceAndCreditCard.setText(priceString + " כ.א: "+ temp.getCreditNumber() + "...");
		
		return v;
	}

}
