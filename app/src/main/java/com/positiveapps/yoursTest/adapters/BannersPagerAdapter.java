/**
 * 
 */
package com.positiveapps.yoursTest.adapters;

import java.util.ArrayList;


import com.positiveapps.yoursTest.fragments.BunnerFragment;
import com.positiveapps.yoursTest.fragments.BunnerFragment.OnBunnerClickListener;
import com.positiveapps.yoursTest.objects.YoursItem;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * @author Nati Gabay
 *
 */
public class BannersPagerAdapter extends FragmentStatePagerAdapter {
	
	
	private ArrayList<YoursItem> bunnersArray;
	private OnBunnerClickListener listener;
	
	public BannersPagerAdapter(FragmentManager fm) {
		super(fm);
	}
	
	public void setBunners(ArrayList<YoursItem> bunners,OnBunnerClickListener listener){
		this.bunnersArray = bunners;
		this.listener = listener;
	}

	@Override
	public Fragment getItem(int i) {
		BunnerFragment bunnerFragment = BunnerFragment.newInstance
				(bunnersArray.get(i),i);
		bunnerFragment.setOnBunnerClickListener(listener);
		return bunnerFragment;
	}

	// count of pages in pager
	@Override
	public int getCount() {
		return bunnersArray.size();
	}

	
}
