/**
 * 
 */
package com.positiveapps.yoursTest;



import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.positiveapps.yoursTest.database.AppPreference;
import com.positiveapps.yoursTest.database.AppPreference.PreferenceGeneralSettings;
import com.positiveapps.yoursTest.database.AppPreference.PreferenceUserProfil;
import com.positiveapps.yoursTest.database.AppPreference.PreferenceUserSettings;
import com.positiveapps.yoursTest.database.StorageManager;
import com.positiveapps.yoursTest.network.NetworkManager;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.util.AppUtil;

/**
 * @author natiapplications
 *
 */
public class YoursApp extends Application {
	
	public static Context appContext;
	public static String appVersion;
	public static AppPreference appPreference;
	public static PreferenceUserSettings userSettings;
	public static PreferenceGeneralSettings generalSettings;
	public static PreferenceUserProfil userProfil;
	public static NetworkManager networkManager;
	public static StorageManager storageManager;
	public static ArrayList<Category> categoriesArray;
	
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Fabric.with(this, new Crashlytics());
		appContext = getApplicationContext();
		appVersion = AppUtil.getApplicationVersion(appContext);
		networkManager = NetworkManager.getInstance(appContext);
		storageManager = StorageManager.getInstance();
		loadPreferences();
	}
	
	private void loadPreferences() {
		appPreference = AppPreference.getInstans(appContext);
		userSettings = appPreference.getUserSettings();
		userProfil = appPreference.getUserProfil();
		generalSettings = appPreference.getGeneralSettings();
	}
	
	public static void createCategories () {
		/*if (categoriesArray != null&&categoriesArray.size() > 0 &&!needUpdate()){
			return;
		}else{
			
			if (!needUpdate()){
				storageManager.readFromStorage(StorageManager.CATEGORIES_FILE_NAME, new StorageListener() {
					
					@SuppressWarnings("unchecked")
					@Override
					public void onDataStorageRcived(String type, Object object) {
						
						Log.e("categoriesfromStorageLog", "size = " + ((ArrayList<Category>)object).size());
						if ((ArrayList<Category>)object != null && ((ArrayList<Category>)object).size() <= 0){
							performCreation();
						}else{
							categoriesArray = (ArrayList<Category>)object;
						}
						
					}
					
					@Override
					public void DataNotFound(String type) {
						performCreation();
					}
				});
				
			}else{
				performCreation();
			}
			
		}*/
		performCreation();
	}
	
	private static void performCreation(){
		categoriesArray = new ArrayList<Category>();
		categoriesArray.add(new Category
				(Category.CATEGORIES_NAMES[Category.CATEGORY_FAMILLY], Category.KEY_MENU_FAMILLY, Category.CATEGORY_FAMILLY));
		categoriesArray.add(new Category
				(Category.CATEGORIES_NAMES[Category.CATEGORY_PROCHASES], Category.KEY_MENU_PROCHASES, Category.CATEGORY_PROCHASES));
		categoriesArray.add(new Category
				(Category.CATEGORIES_NAMES[Category.CATEGORY_FUN], Category.KEY_MENU_FUN, Category.CATEGORY_FUN));
		categoriesArray.add(new Category
				(Category.CATEGORIES_NAMES[Category.CATEGORY_HOBY], Category.KEY_MENU_HOBY, Category.CATEGORY_HOBY));
		categoriesArray.add(new Category
				(Category.CATEGORIES_NAMES[Category.CATEGORY_HOT_MONTH], Category.KEY_HOT_MONTH, Category.CATEGORY_HOT_MONTH));
		
		for (int i = 0; i < categoriesArray.size(); i++) {
			categoriesArray.get(i).initCategory(-1);
		}
	}
	
	private static boolean needUpdate (){
		long currentTime = Calendar.getInstance().getTimeInMillis();
		long lastUpdatedTime = generalSettings.getLastDateUpdatedCategories();
		long howers = 60*1000*60*3;
		if (currentTime >= lastUpdatedTime+howers){
			return true;
		}
		return false;
	}
	
	public static boolean initializerFinished () {
		if (categoriesArray == null){
			return false;
		}
		
		for (int i = 0; i < categoriesArray.size(); i++) {
			if (!categoriesArray.get(i).isDone()){
				return false;
			}
		} 
		return true;
	}
	
	public static void printAllCategoriesData () {
		
		Log.d("categoriesfromStorageLog", "size = " + categoriesArray.size());
		storageManager.writeToStorage(StorageManager.CATEGORIES_FILE_NAME, categoriesArray);
		generalSettings.setLastDateUpdatedCategories(Calendar.getInstance().getTimeInMillis());
	}

}
