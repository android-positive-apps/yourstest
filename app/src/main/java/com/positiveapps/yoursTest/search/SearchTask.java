/**
 * 
 */
package com.positiveapps.yoursTest.search;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.SubCategory;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.util.DialogUtil;

/**
 * @author natiapplications
 *
 */
public class SearchTask extends AsyncTask<ArrayList<YoursItem>, Void, ArrayList<YoursItem>>{

	
	private String keyWord;
	private SearchCallback callback;
	private ProgressDialog dialog;
	
	
	
	
	public SearchTask(String keyWord, SearchCallback callback) {
		super();
		this.keyWord = keyWord;
		this.callback = callback;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		dialog = DialogUtil.showProgressDialog
				(MainActivity.mainInstance, YoursApp.appContext.getString(R.string.on_search_message));
	}
	
	@Override
	protected ArrayList<YoursItem> doInBackground(
			ArrayList<YoursItem>... params) {
		final ArrayList<YoursItem> result = new ArrayList<>();
		ArrayList<Category> categories = YoursApp.categoriesArray;
		for (int i = 0; i < categories.size(); i++) {
			ArrayList<SubCategory> tempSub = categories.get(i).getSubCategories();
			for (int j = 0; j < tempSub.size(); j++) {
				ArrayList<YoursItem> tempItems = tempSub.get(j).getItems();
				for (int k = 0; k < tempItems.size(); k++) {
					YoursItem temp = tempItems.get(k);
					if (itemContainskeyword (keyWord,temp)){
						result.add(temp);
					}
				}
			}
			
		}
		return result;
	}
	
	@Override
	protected void onPostExecute(ArrayList<YoursItem> result) {
		super.onPostExecute(result);
		
		if (callback != null){
			callback.onSearchResult(result);
		}
		DialogUtil.dismisDialog(dialog);
	}
	
	private boolean itemContainskeyword(String keyWord,YoursItem item) {
		if (StringUtils.containsIgnoreCase(item.getItemLocation(), keyWord)){
			return true;
		}else if(StringUtils.containsIgnoreCase(item.getItemName(), keyWord)){
			return true;
		}else if(StringUtils.containsIgnoreCase(item.getItemDesc(), keyWord)){
			return true;
		}else if(StringUtils.containsIgnoreCase(item.getItemKind(), keyWord)){
			return true;
		}else if(StringUtils.containsIgnoreCase(Category.CATEGORIES_NAMES[item.getParentType()], keyWord)){
			return true;
		}else if(StringUtils.containsIgnoreCase(item.getSubCategoryName(), keyWord)){
			return true;
		}

		return false;
	}

}
