/**
 * 
 */
package com.positiveapps.yoursTest.search;

import java.util.ArrayList;

import com.positiveapps.yoursTest.objects.YoursItem;

/**
 * @author natiapplications
 *
 */
public interface SearchCallback {
	
	public void onSearchResult (ArrayList<YoursItem> result);

}
