package com.positiveapps.yoursTest;

import java.util.ArrayList;

import com.google.analytics.tracking.android.EasyTracker;
import com.positiveapps.yoursTest.adapters.DrawerListAdapter;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.fragments.MainMenuFragment;
import com.positiveapps.yoursTest.fragments.YoursItemsListFragment;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.DrawerItem;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.search.SearchCallback;
import com.positiveapps.yoursTest.search.SearchTask;
import com.positiveapps.yoursTest.ui.ProfilView;
import com.positiveapps.yoursTest.ui.SettingsView;
import com.positiveapps.yoursTest.util.AppUtil;
import com.positiveapps.yoursTest.util.BackStackUtil;
import com.positiveapps.yoursTest.util.DialogUtil;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements OnItemClickListener,OnClickListener,DrawerListener{

	
	public static final String EXTRA_FROM_NOTIFICATION = "FromNotification";
	
	public static BaseFragment currentFragment;
	public static MainActivity mainInstance;
	public static TextView screenTitle;
	public static  ImageView appLogo;
	
	private DrawerLayout drawerLayout;
    private ListView drawerList;
    private ArrayList<DrawerItem> drawerItems = new ArrayList<>();
    private DrawerListAdapter drawerListAdapter;
    /**
     * list header properties
     */
    private TextView nameTxt;
    private View listHeader;
    private View listFouter;
    private LinearLayout profileItem;
    private LinearLayout profileContainer;
    private ImageView settingsBtn;
    private ImageView refreshBtn;
    
    private EditText searchEt;
    private ImageView searchBtn;
    private ImageView cancelSearchBtn;
    private View settingsView;
    private View profileView;
    
    
    private View actionBarLayout;
	private Button drawerTriger;
	private Animation drawerButtonOpenAnim;
	private Animation drawerButtonCloseAnim;
	private boolean isOpenAnimate;
	private boolean isCloseAnimate;
	
	private ProgressDialog refreshDialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.saveScreenDimention(this);
		setContentView(R.layout.activity_main);
		mainInstance = this;
		
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.left_drawer);
        
        
        setUpActionBar();
        if (savedInstanceState == null){
            FragmentsUtil.addFragment(getSupportFragmentManager(), MainMenuFragment.newInstance(), R.id.content_frame);
        }
        
	}
	public static void setScreenTitle (String title){
		title= "TEST";
		screenTitle.setTextColor(Color.RED);
		try {
			if (title == null){
				screenTitle.setVisibility(View.GONE);
				appLogo.setVisibility(View.VISIBLE);
			}else{
				appLogo.setVisibility(View.GONE);
				screenTitle.setVisibility(View.VISIBLE);
				screenTitle.setText(title);
			}
		} catch (Exception e) {} catch (Error e){}
		
	}

	 
	
	@Override
	protected void onResume() {
		super.onResume();
		setUpDrawerList();
	}
	
	
	@Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	@Override
	public void onStop() {
	    super.onStop();
	   EasyTracker.getInstance(this).activityStop(this);  // Add this method.
    }
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (drawerLayout.isDrawerOpen(Gravity.RIGHT)){
			closeDrawer();
			return;
		}
		if (!currentFragment.onBackPressed()){
			if (currentFragment.getClass().getSimpleName().equalsIgnoreCase(MainMenuFragment.class.getSimpleName())){
				showExitDialog();
				return;
			}else{
				super.onBackPressed();
			}
		}
	}
	
	private void showExitDialog () {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.exit_message))
				.setPositiveButton("אישור", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				}).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				}).show();
	}
	
	/**
	 * 
	 */
	private void setUpDrawerList() {
		
		if (listHeader != null){
			drawerList.removeHeaderView(listHeader);
		}
		if (listFouter != null){
			drawerList.removeFooterView(listFouter);
		}
		//header
		listHeader = getLayoutInflater().inflate(R.layout.drawer_header_layout, null);
		drawerList.addHeaderView(listHeader);
		setUpDrawerHeader(listHeader);
		listFouter = getLayoutInflater().inflate(R.layout.drawer_fouter, null);
		drawerList.addFooterView(listFouter);
		
		buildDrawerItems();
		drawerListAdapter = new DrawerListAdapter(this, 0, drawerItems);
		drawerList.setAdapter(drawerListAdapter);
		drawerList.setOnItemClickListener(this);
		
	}
	
    private void setUpDrawerHeader(View listHeager) {
    	settingsView = new SettingsView().getView();
    	profileView = new ProfilView().getView();
    	
    	profileContainer = (LinearLayout)listHeager.findViewById(R.id.profile_container);
    	profileContainer.addView(settingsView);
    	profileContainer.addView(profileView);
    	settingsView.setVisibility(View.GONE);
    	profileView.setVisibility(View.GONE);
    	
    	nameTxt = (TextView)listHeager.findViewById(R.id.txt_name);
    	searchEt = (EditText)listHeager.findViewById(R.id.et_search);
    	searchBtn = (ImageView)listHeager.findViewById(R.id.btn_search);
    	refreshBtn = (ImageView)listHeager.findViewById(R.id.refresh_btn);
    	cancelSearchBtn = (ImageView)listHeager.findViewById(R.id.btn_cancel_search);
    	settingsBtn = (ImageView)listHeager.findViewById(R.id.btn_settings);
    	profileItem = (LinearLayout)listHeager.findViewById(R.id.profil_item);
    	nameTxt.setText(YoursApp.userProfil.getUserFullName(" "));
    	
    	profileItem.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (profileView.getVisibility() == View.VISIBLE){
					profileView.setVisibility(View.GONE);
				}else{
					if (settingsView.getVisibility() == View.VISIBLE){
						settingsView.setVisibility(View.GONE);
					}
					profileView.setVisibility(View.VISIBLE);
				}
			}
		});
    	settingsBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (settingsView.getVisibility() == View.VISIBLE){
					settingsView.setVisibility(View.GONE);
				}else{
					if (profileView.getVisibility() == View.VISIBLE){
						profileView.setVisibility(View.GONE);
					}
					settingsView.setVisibility(View.VISIBLE);
				}
			}
		});
    	
    	searchBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				InputMethodManager imm = (InputMethodManager)YoursApp.appContext.getSystemService(
		    		      Context.INPUT_METHOD_SERVICE);
		    		imm.hideSoftInputFromWindow(searchEt.getWindowToken(), 0);
		    		
				String keyWord = searchEt.getText().toString();
				searchEt.setText("");
				if (keyWord.length() == 0){
					ToastUtil.toster(getString(R.string.no_key_word), false);
				}else if (keyWord.length() < 3){
					ToastUtil.toster(getString(R.string.no_key_word_minimum), false);
				}else{
					search(keyWord);
				}
			}
		});
    	cancelSearchBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				searchEt.setText("");
			}
		});
    	refreshBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				refreshDialog = DialogUtil.showProgressDialog(mainInstance, "מרענן נתונים...");
				refreshDialog.setCancelable(false);
				YoursApp.createCategories();
				new WaitToDataInitializerThread().start();
			}
		});
	}
    
    
    class WaitToDataInitializerThread extends Thread {
		
		@Override
		public void run() {
			super.run();
			while (!YoursApp.initializerFinished()){
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					DialogUtil.dismisDialog(refreshDialog);
				}
			});
			
		}
	}
    @SuppressWarnings("unchecked")
	private void search (String keyWord){
		new SearchTask(keyWord, new SearchCallback() {
			@Override
			public void onSearchResult(ArrayList<YoursItem> result) {
				closeDrawer();
				openYoursItemListFragment(0, 0, "", false, result);
			}
		}).execute();
	}

	/**
	 * 
	 */
	private void buildDrawerItems() {
		drawerItems.clear();
		for (int i = 0; i < YoursApp.categoriesArray.size(); i++) {
			if (YoursApp.categoriesArray.get(i).getCategoryType() == Category.CATEGORY_HOT_MONTH){
				continue;
			}
			DrawerItem tempCategory = new DrawerItem();
			tempCategory.setType(DrawerItem.ITEM_TYPE_CATEGORY);
			int categoryType = YoursApp.categoriesArray.get(i).getCategoryType();
			tempCategory.setCategory(categoryType);
			drawerItems.add(tempCategory);
			for (int j = 0; j < YoursApp.categoriesArray.get(i).getSubCategories().size(); j++) {
				DrawerItem tempSub = new DrawerItem();
				tempSub.setType(DrawerItem.ITEM_TYPE_SUB_CATEGORY);
				tempSub.setCategory(categoryType);
				tempSub.setPosition(j);
				tempSub.setSubCategoryType(YoursApp.categoriesArray.get(i).getSubCategories().get(j).getSubCategoryMenuId());
				tempSub.setName(YoursApp.categoriesArray.get(i).getSubCategories().get(j).getSubCategoryName());
				drawerItems.add(tempSub);
			}
		}
		
	}


	private void setUpActionBar () {
		
		actionBarLayout = getLayoutInflater().inflate(R.layout.action_bar_layout, null);
        final ActionBar actionBar = getSupportActionBar();  
	    
        actionBar.setDisplayShowTitleEnabled(false);
	    actionBar.setDisplayUseLogoEnabled(false);
	    actionBar.setDisplayHomeAsUpEnabled(false);
	    actionBar.setDisplayShowCustomEnabled(true);
	    actionBar.setDisplayShowHomeEnabled(false);
        
	    actionBarLayout.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));
	    
	    actionBar.setCustomView(actionBarLayout, new ActionBar.LayoutParams(LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT));

		Toolbar toolbar = (Toolbar)actionBarLayout.getParent();
		toolbar.setContentInsetsAbsolute(0,0);

	    drawerTriger = (Button)actionBarLayout.findViewById(R.id.drawer_triger);
	    screenTitle = (TextView)actionBarLayout.findViewById(R.id.screen_title);
	    appLogo = (ImageView)actionBarLayout.findViewById(R.id.app_logo);
	    drawerTriger.setOnClickListener(this);
	    drawerButtonOpenAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_left_anim);
	    drawerButtonCloseAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_to_right_anim);
	    drawerLayout.setDrawerListener(this);
		
	}

	
	public void openDrawer(){
		drawerTriger.startAnimation(drawerButtonOpenAnim);
		drawerLayout.openDrawer(Gravity.RIGHT);
		isOpenAnimate = true;
		isCloseAnimate = false;
	}
	
	
	public void closeDrawer(){
		drawerTriger.startAnimation(drawerButtonCloseAnim);
		drawerLayout.closeDrawer(Gravity.RIGHT);
		isCloseAnimate = true;
		isOpenAnimate = false;
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, final int arg2, long arg3) {
		
		if (arg2 - 1 >= drawerItems.size()){
			return;
		}
		closeDrawer();
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				
				DrawerItem selected = drawerItems.get(arg2-1);
				if (selected.getType() == DrawerItem.ITEM_TYPE_CATEGORY){
					openYoursItemListFragment (selected.getCategory(),0,"",false,null);
				}else if (selected.getType() == DrawerItem.ITEM_TYPE_SUB_CATEGORY){
					openYoursItemListFragment (selected.getCategory(),selected.
							getSubCategoryType(),selected.getName(),true,null);
				}
			}
		}, 500);
		
		
	}


	
	/**
	 * @param category
	 * @param name
	 */
	private void openYoursItemListFragment(final int category,int subCategorytype, 
			final String name,final boolean fillter,final ArrayList<YoursItem> items) {
		if (MainActivity.currentFragment instanceof YoursItemsListFragment){
			YoursItemsListFragment frag = (YoursItemsListFragment)MainActivity.currentFragment;
			if (items != null){
				frag.setYoursItemsArrayToDisplay(items);
				frag.refreshListByArray();
				return;
			}
			frag.setCategoryAndSubCategoryTypes(category, subCategorytype, name, fillter);
			if (fillter){
				frag.refreshListByFillter();
			}else{
				frag.refreshListShowAll();
			}
			return;
		}
		if (!BackStackUtil.popToBackStack(YoursItemsListFragment.class.getSimpleName(), getSupportFragmentManager())){
			YoursItemsListFragment frag = YoursItemsListFragment.newInstance();
			if (items != null){
				frag.setYoursItemsArrayToDisplay(items);
			}else{
				frag.setCategoryAndSubCategoryTypes(category, 0, name, fillter);
			}
			FragmentsUtil.openFragmentRghitToLeft(getSupportFragmentManager(), frag, R.id.content_frame);
		}else{
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					if (MainActivity.currentFragment instanceof YoursItemsListFragment){
						YoursItemsListFragment frag = (YoursItemsListFragment)MainActivity.currentFragment;
						if (items != null){
							frag.setYoursItemsArrayToDisplay(items);
							frag.refreshListByArray();
							return;
						}
						frag.setCategoryAndSubCategoryTypes(category, 0, name, fillter);
						if (fillter){
							frag.refreshListByFillter();
						}else{
							frag.refreshListShowAll();
						}
					}
					
				}
			}, 500);
			
		}
		
		
	}


	@Override
	public void onClick(View v) {
		
		if (v.getId() == R.id.drawer_triger){
			if (drawerLayout.isDrawerOpen(Gravity.RIGHT)) {
               closeDrawer();
            } else {
               openDrawer();
            }
		}
		
	}


	
	@Override
	public void onDrawerClosed(View arg0) {
		if(!isCloseAnimate){
			drawerTriger.startAnimation(drawerButtonCloseAnim);
			isCloseAnimate = true;
			isOpenAnimate = false;
		}
	}


	@Override
	public void onDrawerOpened(View arg0) {
		if (!isOpenAnimate){
			drawerTriger.startAnimation(drawerButtonOpenAnim);
			isOpenAnimate = true;
			isCloseAnimate = false;
		}
	}

	@Override
	public void onDrawerSlide(View arg0, float arg1) {
		// TODO Auto-generated method stub
		
		
	}

	@Override
	public void onDrawerStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}
	
	

}
