package com.positiveapps.yoursTest;

import com.positiveapps.yoursTest.util.AppUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class ForgotPassActivity extends Activity {

	private RelativeLayout relativeLayout;
	private WebView forgotWebView;
	private ProgressBar progressBar;
	private final String forgotURL  = "http://www.yours.co.il/password1.aspx?pass=1";
	
	@SuppressLint("ClickableViewAccessibility")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		AppUtil.makeFullScreenActivity(this);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forgot_pass);
		relativeLayout = (RelativeLayout)findViewById(R.id.screen);
		
		forgotWebView = (WebView)findViewById(R.id.forgot_pass_webView);
		
		progressBar = (ProgressBar)findViewById(R.id.progressBar);
		
		forgotWebView.getSettings().setBuiltInZoomControls(true);
		forgotWebView.requestFocus();
		forgotWebView.getSettings().setJavaScriptEnabled(true);
		forgotWebView.getSettings().setUseWideViewPort(true);
		forgotWebView.getSettings().setLoadsImagesAutomatically(true);
		forgotWebView.setWebViewClient(new MyBrowser());
		forgotWebView.loadUrl(forgotURL);
		/*forgotWebView.requestFocusFromTouch();
		forgotWebView.setOnTouchListener(new View.OnTouchListener() {

			@SuppressLint("ClickableViewAccessibility")
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				 switch (event.getAction()) {
	                case MotionEvent.ACTION_DOWN:
	                case MotionEvent.ACTION_UP:
	                    if (!v.hasFocus()) {
	                        v.requestFocus();
	                        forgotWebView.onCheckIsTextEditor();
	                    }
	                    break;
	            }
	            return false;
			}
	    });
		
		
		forgotWebView.requestFocus(View.FOCUS_DOWN);*/
		
	}
	
	private class MyBrowser extends WebViewClient {
		  int counter = 0;
	      @Override
	      public boolean shouldOverrideUrlLoading(WebView view, String url) {
	         view.loadUrl(url);
	         return true;
	      }
	      
	      public void onPageFinished(WebView view, String url) {
			   progressBar.setVisibility(View.GONE);
			   Log.e("on page finished", "on page finishd");
			   if (!url.equalsIgnoreCase(forgotURL)){
				   finish();
			   }
		    }
	   }

	
}
