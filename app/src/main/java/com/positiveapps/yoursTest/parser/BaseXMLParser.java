package com.positiveapps.yoursTest.parser;

import java.io.IOException;
import java.io.StringReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class BaseXMLParser {
	
	public static final String END_ROW = "EndRow";
	
	private GetTagContent getTagContent;
	private String xml;
	
	public BaseXMLParser (String xml,GetTagContent getTagContent){
		this.getTagContent = getTagContent;
		this.xml = xml;
	}

	public void parse (){
		if (this.xml == null){
			return;
		}
		try {
			parseXml(this.xml);
		} catch (Exception e) {}
	}
	
	private void parseXml(String xml) throws XmlPullParserException,
			IOException {

		{
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			factory.setNamespaceAware(true);
			XmlPullParser xpp = factory.newPullParser();

			
			xpp.setInput(new StringReader(xml));
			String startTagName = "";
			int eventType = xpp.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_DOCUMENT) {
					getTagContent.onStartDucoment();
				} else if (eventType == XmlPullParser.END_DOCUMENT) {
					getTagContent.onEndDucoment();
				} else if (eventType == XmlPullParser.START_TAG) {
					startTagName = xpp.getName();
					getTagContent.getStartTag(startTagName);
					int count = xpp.getAttributeCount();
					if(count > 0){
						for (int i = 0; i < count; i++) {
							getTagContent.getAttribute(startTagName,xpp.getAttributeName(i), xpp.getAttributeValue(i));
						}
						getTagContent.onEndAttribute(startTagName);
					}
				} else if (eventType == XmlPullParser.END_TAG) {
					getTagContent.getAndTag(xpp.getName());
				} else if (eventType == XmlPullParser.TEXT) {
					getTagContent.getStartTagContent(startTagName, xpp.getText());
					startTagName = "";
				}
				eventType = xpp.next();
			}
		}
	}

	 
	
}
