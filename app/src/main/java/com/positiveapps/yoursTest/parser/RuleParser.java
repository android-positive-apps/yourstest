/**
 * 
 */
package com.positiveapps.yoursTest.parser;

import android.util.Log;





/**
 * @author natiapplications
 *
 */
public class RuleParser implements GetTagContent{

	private final String BODY = "body";
	public static final int TYPE_EULA = 1;
	public static final int TYPE_ABOUT = 2;
	
	
	
	private String xml;
	private BaseXMLParser parser;
	private int type;
	
	private String result;
	
	
	public RuleParser (String xml,int type){
		this.xml = xml;
		this.type = type;
	}
	
	public String parse(){
		parser = new BaseXMLParser(xml, this);
		parser.parse();
		return this.result;
	}	
	
	@Override
	public void getStartTagContent(String tag, String content) {
		
		Log.d("ruleparserlog" , "tag = " + tag + " content = " + content);
	/*	if (type == TYPE_EULA){
			if (tag.equalsIgnoreCase("_x003C_Rule_x003E_k__BackingField")){
				result = content;
			}
		}else if (type == TYPE_ABOUT){
			if (tag.equalsIgnoreCase("_x003C_About_x003E_k__BackingField")){
				result = content;
			}
		}*/
		
		if (tag.equalsIgnoreCase("_x003C_Rule_x003E_k__BackingField")){
			result = content;
		}
		
	}

	
	@Override
	public void getAttribute(String tagName, String attrName, String attrValue) {
		
	}

	
	@Override
	public void getAndTag(String tag) {
		
		
	}

	
	@Override
	public void getStartTag(String tag) {
		Log.e("ruleparserlog" , "tag = " + tag);
		
	}

	
	@Override
	public void onEndAttribute(String attrName) {
		
	}

	
	@Override
	public void onStartDucoment() {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void onEndDucoment() {
		// TODO Auto-generated method stub
		
	}

}