/**
 * 
 */
package com.positiveapps.yoursTest.parser;

import java.util.ArrayList;

import com.positiveapps.yoursTest.objects.Point;
import com.positiveapps.yoursTest.objects.Seat;
import com.positiveapps.yoursTest.objects.SeatRow;

/**
 * @author natiapplications
 *
 */
public class AreaMapParser implements GetTagContent{

	private final String ROW = "r";
	private final String ROW_INDEX = "rn";
	private final String SEAT = "c";
	private final String INDEX_NUMBER = "m";
	private final String SEAT_PRICE = "p";
	private final String SEAT_NUMBER = "s";
	
	
	
	private String xml;
	private BaseXMLParser parser;
	
	private ArrayList<SeatRow> rows = new ArrayList<SeatRow>();
	private ArrayList<Seat> seats = new ArrayList<Seat>();
	private SeatRow tempRow;
	private Seat tempSeat;
	
	
	public AreaMapParser (String xml){
		this.xml = xml;
	}
	
	public ArrayList<SeatRow> parse(){
		//tempRow = new SeatRow();
		//
		//tempSeat = new Seat();
		parser = new BaseXMLParser(xml, this);
		parser.parse();
		return this.rows;
	}	
	
	@Override
	public void getStartTagContent(String tag, String content) {
		if (tag.equalsIgnoreCase(ROW_INDEX)){
			tempRow.setIndex(Integer.parseInt(content));
		}else if (tag.equalsIgnoreCase(INDEX_NUMBER)){
			tempSeat.setIndex(new Point(Integer.parseInt(content), tempRow.getIndex()));
		}else if (tag.equalsIgnoreCase(SEAT_PRICE)){
			if (content.isEmpty()){
				tempSeat.setHasPrice(false);
			}else{
				try {
					double price = Double.parseDouble(content);
					if (price == 0){
						tempSeat.setHasPrice(false);
					}else{
						tempSeat.setHasPrice(false);
						tempSeat.setPrice(price);
					}
				} catch (Exception e) {
					tempSeat.setHasPrice(false);
				}
			}
			
		}else if (tag.equalsIgnoreCase(SEAT_NUMBER)){
			
			
			if (content.isEmpty()){
				tempSeat.setStatus(Seat.STATUS_EMPTY);
			}else{
				try {
					int  seatNumber = Integer.parseInt(content);
					if (seatNumber == 0){
						tempSeat.setStatus(Seat.STATUS_EMPTY);
					}else{
						tempSeat.setStatus(Seat.STATUS_FREE);
						tempSeat.setSeatNumber(seatNumber);
					}
				} catch (Exception e) {
					tempSeat.setStatus(Seat.STATUS_EMPTY);	
					
				}
			}
			
		}
	}

	
	@Override
	public void getAttribute(String tagName, String attrName, String attrValue) {
		
	}

	
	@Override
	public void getAndTag(String tag) {
		
		if (tag.equalsIgnoreCase(ROW)) {
			ArrayList<Seat> copySeats = new ArrayList<Seat>();
			copySeats.addAll(seats);
			this.tempRow.setSeats(copySeats);
			seats.clear();
			rows.add(tempRow);
		} else if (tag.equalsIgnoreCase(SEAT)) {
			tempSeat.setRowIndex(tempRow.getIndex());
			seats.add(tempSeat);
		}
	}

	
	@Override
	public void getStartTag(String tag) {
		if (tag.equalsIgnoreCase(ROW)) {
			tempRow = new SeatRow();
			
		} else if (tag.equalsIgnoreCase(SEAT)) {
			
			tempSeat = new Seat();
			
		}
	}

	
	@Override
	public void onEndAttribute(String attrName) {
		
	}

	
	@Override
	public void onStartDucoment() {
		// TODO Auto-generated method stub
		
	}

	
	@Override
	public void onEndDucoment() {
		// TODO Auto-generated method stub
		
	}

}
