package com.positiveapps.yoursTest.parser;

public interface GetTagContent {
	
	public void getStartTagContent (String tag,String content);
	public void getAttribute(String tagName,String attrName, String attrValue);
	public void getAndTag (String tag);
	public void getStartTag (String tag);
	public void onEndAttribute(String attrName);
	public void onStartDucoment();
	public void onEndDucoment();
	

}
