/**
 * 
 */
package com.positiveapps.yoursTest.network;

import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.content.Context;
import android.os.Handler;
import android.util.Log;



/**
 * @author natiapplications
 *
 */
public class SyncRequestTask extends Thread{
	
	
	private final String TAG = "SyncRequestTaskLog";
	private String requestName = "anonimus name";
	private boolean hasPostData;
	private Context context;
	
	private String url;
	private JSONObject dataEntity;
	private List<NameValuePair> listDataEntitiy;
	private NetworkCallback callback;
	
	
	
	
	public SyncRequestTask(Context context, String url,
			NetworkCallback callback) {
		super();
		this.context = context;
		this.url = url;
		this.callback = callback;
	}
	
	
	
	
	
	public void onPreTask (){
		Handler mainHandler = new Handler(context.getMainLooper());
		mainHandler.post(new Runnable() {

			@Override
			public void run() {
				String[] requestSplit = url.split("/");
				if (requestSplit.length > 0){
					requestName = requestSplit[requestSplit.length-1];
				}
				Log.d(TAG, "Request name: " + requestName + " sent!" 
				+ "\n" + "Request url = " +  url);
			}

			
		});
	}
	
	
	/**
	 * set data to send in post request
	 * 
	 * @param dataEntity - data as List<NameValuePair> to send
	 */
	public void setDataEntityToPost (JSONObject dataEntity){
		this.dataEntity = dataEntity;
		this.hasPostData = true;
	}
	
	/**
	 * set data to send in post request
	 * 
	 * @param dataEntity - data as List<NameValuePair> to send
	 */
	public void setDataEntityToPost (List<NameValuePair> dataEntity){
		this.listDataEntitiy = dataEntity;
		this.hasPostData = true;
	}
	
	
	
	@Override
	public void run() {
		super.run();
		onPreTask(); 
		
		
		ResponseObject result = null;
		try {
			HttpParams httpParameters = new BasicHttpParams();
			int timeoutConnection = 40000;
			HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
			HttpConnectionParams.setSoTimeout(httpParameters, 40000);
			
			DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

			HttpPost post = new HttpPost(this.url);
			
			if (hasPostData) { 
				
				if (dataEntity != null){
					 post.setEntity(new StringEntity(dataEntity.toString(),HTTP.UTF_8)); 
				}else if (listDataEntitiy != null){
					post.setEntity(new UrlEncodedFormEntity(listDataEntitiy,HTTP.UTF_8));
				}
		       
		        String postData = EntityUtils.toString(post.getEntity()); 
		        Log.d(TAG, "data post over request: " + postData);
			}
			HttpResponse response = httpClient.execute(post);
			
			String responseString = EntityUtils.toString(response.getEntity());
			Log.e(TAG, "response string: " + responseString);
			JSONObject jsonObject = new JSONObject(responseString);
			if (this.callback != null){
				result = this.callback.parseResponse(jsonObject);
			}
			
			
		} catch (final Exception e) {
			Log.e(TAG, "exception: " + e.getMessage());
			e.printStackTrace();
		} catch (final Error er){
			Log.e(TAG, "error: " + er.getMessage());
		}finally{
			onPostTask(result);
		}
		
	}
	
	
	public void onPostTask(final ResponseObject result) {

		Handler mainHandler = new Handler(context.getMainLooper());
		mainHandler.post(new Runnable() {

			@Override
			public void run() {

				if (result != null){
					Log.i(TAG, "result object" + result.toString());
					if (callback != null){
						callback.onDataRecived(result,result.isHasError(),result.getErrorDesc());
					}
				}else{
					Log.i(TAG, "result is null");
					if (callback != null){
						callback.onError();
					}
				}
			}
		});

	}
	
	

}
