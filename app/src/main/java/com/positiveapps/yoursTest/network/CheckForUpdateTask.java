/**
 * 
 */
package com.positiveapps.yoursTest.network;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;

/**
 * @author natiapplications
 *
 */
public class CheckForUpdateTask {
	
	public static final int RESULT_NOT_UPDATES_FOUND = 0;
	public static final int RESULT_HAS_UPDATE = 1;
	public static final int RESULT_MUST_UPDATE = 2;
	
	
	public static final int BUTTON_TYPE_UPDATE = 0;
	public static final int BUTTON_TYPE_NOT_NOW = 1;
	public static final int BUTTON_TYPE_CLOSE_APP = 2;
	
	private String oldVersion;
	private String newVersion;
	private boolean success;
	private int result;
	
	private CheckForUpdateCallback callback;
	
	public CheckForUpdateTask (CheckForUpdateCallback callback){
		this.callback = callback;
	}
	
	public void check (){
		YoursApp.networkManager.checkServerVerstion(new NetworkCallback(){
		
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				// TODO Auto-generated method stub
				return new ResponseObject(toParse);
			}
			
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				try {
					
					JSONObject jsonObject = new JSONObject(response.getXmlContent());
					JSONArray androidVersionsArray = 
							new JSONArray(jsonObject.optString("GetLastDeviceVersionResult"));
					JSONObject versionsObjsec = androidVersionsArray.optJSONObject(0);
					String version = versionsObjsec.optString("AndroidDeviceVersion");
					newVersion = version;
					
					Log.e("checkVerstionLog","version = " + version);

					String currentDeviceVersion = YoursApp.appVersion;
					oldVersion = currentDeviceVersion;
					
					if (version != null && currentDeviceVersion != null){
						
						success = true;
						
						int serverVersionAsInt = Integer.parseInt(version.replace(".", ""));
						int deviceVersionAsInt = Integer.parseInt(currentDeviceVersion.replace(".", ""));
						if (serverVersionAsInt > deviceVersionAsInt){
							if (serverVersionAsInt - deviceVersionAsInt >= 10){
								result = RESULT_MUST_UPDATE;
							}else{
								result = RESULT_HAS_UPDATE;
							}
						}else{
							result = RESULT_NOT_UPDATES_FOUND;
						}
					}else{
						result = RESULT_NOT_UPDATES_FOUND;
						success = false;
					}
					
				} catch (Exception e) {
					Log.e("checkVerstionLog","response = " + response.getXmlContent());
					result = RESULT_NOT_UPDATES_FOUND;
					success = false;
				}
				
				if (callback != null){
					callback.onCheck(success, result, oldVersion, newVersion);
				}
			}
			
			
			@Override
			public void onError() {
				super.onError();
				if (callback != null){
					callback.onCheck(success, result, oldVersion, newVersion);
				}
			}
			
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				if (callback != null){
					callback.onCheck(success, result, oldVersion, newVersion);
				}
			}
			
		});
	}
	
	
	public static void showNeedUpdateDialog (final Activity activity,final boolean must
			,final CheckForUpdateCallback callback) {
		
		String cancel = activity.getString(R.string.not_now);
		String message = activity.getString(R.string.need_update_txt);;
		if (must){
			cancel = activity.getString(R.string.close_app);
			message = activity.getString(R.string.must_updte_txt);;
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setCancelable(false);
		builder.setMessage(message);
		builder.setTitle(activity.getString(R.string.update_app))
				.setPositiveButton(activity.getString(R.string.update), new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						
						final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object
						try {
						    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
						} catch (android.content.ActivityNotFoundException anfe) {
						    activity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
						}
						if (callback != null){
							callback.onUpdateDialogButtonClicked(BUTTON_TYPE_UPDATE);
						}
					}
				}).setNegativeButton(cancel, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (must){
							if (callback != null){
								callback.onUpdateDialogButtonClicked(BUTTON_TYPE_CLOSE_APP);
							}
						}else{
							if (callback != null){
								callback.onUpdateDialogButtonClicked(BUTTON_TYPE_NOT_NOW);
							}
						}
					}
				}).show();
	
	}
	
	public interface CheckForUpdateCallback {
		public void onCheck (boolean success,int result,String oldVersion,String newVersion);
		public void onUpdateDialogButtonClicked(int buttonType);
	}

}
