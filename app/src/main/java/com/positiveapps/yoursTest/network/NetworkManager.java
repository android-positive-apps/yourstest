/**
 * 
 */
package com.positiveapps.yoursTest.network;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.positiveapps.pelecardsdk.api.PelecardApiCallback;
import com.positiveapps.pelecardsdk.api.PelecardRequestTask;
import com.positiveapps.pelecardsdk.object.CreditCard;
import com.positiveapps.pelecardsdk.object.PelecardResult;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.util.DeviceUtil;


/**
 * @author Nati Gabay
 *
 * @description 
 */
public class NetworkManager { 
	
	
	/*********************
	 * ALL API REQUEST URLs
	 *********************/
	

		
	public static final String BASE_URL = "http://212.29.241.9/shablul_test/Yours_test_mobile_TEST/YoursMobileWebService/";
	
	
	private final String LOGIN = BASE_URL + "loginaccount.svc/loginuser";
	private final String GET_SUB_CATEGORY_DETAILS = BASE_URL + "GetListOfSubject.svc/GetSubjectOfList";
	private final String GET_BANNERS = BASE_URL + "GetAdvertise.svc/GetAdvert";
	private final String GET_ITEM_DETAILS = BASE_URL + "GetSubjectDetails.svc/GetSubjectDetail";
	private final String SEND_CONTACT_US_MESSAGE = BASE_URL + "UpdateCommitments.svc/CreateContactMessage";
	private final String UPDATE_PROFILE = BASE_URL +"UpdateCommitments.svc/UpdateUserRegistration";
	private final String GET_RULES = BASE_URL +"GetSiteRules.svc/GetSiteRule";
	private final String GET_HISTORY = BASE_URL +"GetOrdersHistory.svc/GetOrderHistory";	
	
	private final String GET_FULL_SEATS = BASE_URL +"OrdersProceed.svc/GetOcupedSeats";
	private final String SET_ORDER = BASE_URL + "OrdersProceed.svc/RegisterPayment";
	private final String PRE_ORDER = BASE_URL + "OrdersProceed.svc/PreOrder";
	private final String PRE_ORDER_PASHBAR = BASE_URL + "OrdersProceed.svc/PreOrder2";
	private final String GET_PASHBAR_LINK = BASE_URL + "PashBarShows.svc/GetRequestPashBar";
	private final String GET_SINEMA_LINK = BASE_URL + "PashBarShows.svc/GetRequestCinemaCity";
	private final String REGISTER_ORDER_IN_PUSH_SERVER = "http://www.mypushserver.com/dev/amos/api/addIncome";
	
	private final String CHECK_SERVER_VERSTION = BASE_URL + "GetSiteRules.svc/GetLastDeviceVersion";
	/***********************
	 * ALL API REQUEST KEYs
	 ***********************/
	
	
	
	/** GENERAL KEYS **/
	
	
	private final String USER_NUMBER = "userNumber";
	private final String PASSWORD = "password";
	private final String KEY_MENU = "keyMenu";
	private final String SUB_NENU_ID = "SubMenuId";
	private final String WHERE = "where";
	private final String SUBJECT_ID = "subjectId";
	private final String MESSAGE_CONTENT = "MessageContent";
	private final String FULL_NAME = "FullName";
	private final String FROM_ADDRESS = "FromAddress";
	private final String TELEPHONE = "Telephone";
	private final String TO_ADDRESS ="ToAddresses";
	private final String FIRST_NAME = "firstname";
	private final String LAST_NAME = "lastname";
	private final String ID_ENTU_USER = "identuser";
	private final String OLD_PASS = "oldpassword";
	private final String NEW_PASS = "newpassword";
	private final String NEW_PASS_CONFIRMATION = "newpasswordconfirm";
	private final String STREET = "street";
	private final String ZIP_CODE = "zipnum";
	private final String CITY = "city";
	private final String MOBLIE = "mobile";
	private final String USER_ID = "UserId";
	private final String PASS = "Password";
	private final String STATE = "Ispub";
	private final String RULE_ID = "ruleId";
	private final String KEY = "key";
	private final String ID = "id";
	private final String SUBMENUID = "submenuid";
	private final String ORDER_QUANTITY = "orderquantity";
	private final String CLIENT_ID = "clientid";
	private final String SEATS_BUSY ="seatsbuys";
	private final String KEY_ORDER = "keyOrder";
	private final String ORDER_NUMBER = "currordernum";
	private final String ORDER_ID = "orderid";
	private final String SAM_PAYMENT = "sumPayment";
	private final String RECEIPT_CONFIRMATION = "receiptcreditcard";
	private final String CLIENT_EMAIL = "clientEmail";
	private final String CLIENT_PHONE = "clientPhone";
	private final String FIRST_6_DIGIT_OF_CREDIT_CARD = "first6digitCreditCard";
	private final String BUYER_NAME = "pay_name";
	private final String BUYER_ID = "pay_tz";
	
	
	private final String APP_NAME = "AppName";
	private final String OS = "OS";
	private final String AMOUNT = "Amount";
	private final String CURRENCY = "Currency";
	private final String UDID = "UDID";
	private final String PURCHASE_DESC = "PurchaseDesc";
			
	
	
	 
	
	
	// context of current application
	private Context cotext;
	
	// single instance of this class
	private static NetworkManager instance;
	
	// connectivity manager to check network state before send the requests
	private  ConnectivityManager connectivityManager;
	
	
	/**
	 * private constructor
	 * 
	 * @param context - context of current application
	 */
	private NetworkManager (Context context){
		this.cotext = context;
		this.connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
	}
	
	
	/**
	 * singleton design 
	 * get the single instance of this class 
	 * 
	 * @param context - context of current application
	 * @return the single instance of this class
	 */
	public static NetworkManager getInstance (Context context){
		if (instance == null){
			instance = new NetworkManager(context);
		}
		return instance;
	}
	
	/**
	 * remove single instance when application is destroyed
	 */
	public void removeInstance (){
		instance = null;
	}
	
	/**
	 * perform the requests by specify the request parameters
	 * 
	 * @param URL - string URL path of the server
	 * @param postData - List<NameValuePair> contains data to send over post request
	 * @param callback - callback method to call when the task is done. this parameter is optional. is may be null
	 * 
	 */
	public void sendRequest (String URL,JSONObject postData,PelecardApiCallback callback){
		if (isNetworkAvailable()){
			PelecardRequestTask requestTask = new PelecardRequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.execute();
			return;
		}
		callback.networkUnavailable(cotext.getString(R.string.network_unavailable_message));
	}
	
	/**
	 * perform the requests by specify the request parameters
	 * 
	 * @param URL - string URL path of the server
	 * @param postData - List<NameValuePair> contains data to send over post request
	 * @param callback - callback method to call when the task is done. this parameter is optional. is may be null
	 * 
	 */
	public void sendRequest (String URL,List<NameValuePair> postData,NetworkCallback callback){
		if (isNetworkAvailable()){
			RequestTask requestTask = new RequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.execute();
			return;
		}
		if (callback != null){
			callback.networkUnavailable(cotext.getString(R.string.network_unavailable_message));
		}
	}
	
	public void sendSyncRequest (String URL,List<NameValuePair> postData,NetworkCallback callback){
		if (isNetworkAvailable()){
			SyncRequestTask requestTask = new SyncRequestTask(cotext, URL, callback);
			if (postData != null){
				requestTask.setDataEntityToPost(postData);
			}
			
			requestTask.start();
			return;
		}
		if (callback != null){
			callback.networkUnavailable(cotext.getString(R.string.network_unavailable_message));
		}
	}
	
	
	
	/**
	 * checks the network state
	 * 
	 * @return false if network is not available at current time
	 */
	public  boolean isNetworkAvailable() {

		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		} else {
			return false;
		}

	}
	
	
	public void userLogin (String userNumber,String password,NetworkCallback callback) {
		
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(USER_NUMBER,userNumber));
		postData.add(new BasicNameValuePair(PASSWORD, password));
		
		sendRequest(LOGIN, postData,callback);
	}
	
	public void getSubCategoryDetails(String keyMenu, String subMenuID,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(KEY_MENU, keyMenu));
		postData.add(new BasicNameValuePair(SUB_NENU_ID, subMenuID));
		System.out.println("niv check:= "+postData.toString());
		//http://212.29.241.9/shablul_test/Yours_test_mobile/YoursMobileWebService/GetListOfSubjectTest.svc/GetSubjectOfList

		sendSyncRequest(BASE_URL + "GetListOfSubjectNewTest.svc/GetSubjectOfList", postData, callback);
	}
	
	public void getBanners(NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(WHERE, "menu"));

		sendRequest(GET_BANNERS, postData, callback);
	}
	
	public void getRules(String ruleId,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(RULE_ID, ruleId));

		sendRequest(GET_RULES, postData, callback);
	}
	
	public void getHestory(String key,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		//postData.add(new BasicNameValuePair(KEY, key));
		postData.add(new BasicNameValuePair("clientid", YoursApp.userProfil.getIdentUser()));

		sendRequest(GET_HISTORY, postData, callback);
	}
	
	public void getItemDetails(String keyMenu,String subMenuId,String subjectId,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(KEY_MENU, keyMenu));
		postData.add(new BasicNameValuePair(SUB_NENU_ID, subMenuId));
		postData.add(new BasicNameValuePair(SUBJECT_ID, subjectId));

		sendRequest(GET_ITEM_DETAILS, postData, callback);
	}
	
	
	public void getFullSeats(String subjectId,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ID, subjectId));

		sendRequest(GET_FULL_SEATS, postData, callback);
	}
	
	
	public void sendContactAsMessage(String fullName,String mail,String phone,String messageContent,String toAddress,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(FULL_NAME, fullName));
		postData.add(new BasicNameValuePair(TELEPHONE, phone));
		postData.add(new BasicNameValuePair(MESSAGE_CONTENT, messageContent));
		postData.add(new BasicNameValuePair(FROM_ADDRESS, mail));
		postData.add(new BasicNameValuePair(TO_ADDRESS, toAddress));

		sendRequest(SEND_CONTACT_US_MESSAGE, postData, callback);
	}
	
	public void updateUserProfile(String firstName,String lastName,
			String oldPass,String newPass,String confirmPass,String street,String zipCode,
			String city,String mobile,String isPub,
			NetworkCallback callback) {

	
		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(FIRST_NAME, firstName));
		postData.add(new BasicNameValuePair(LAST_NAME, lastName));
		//postData.add(new BasicNameValuePair("Password", YoursApp.userProfil.getUserPass()));
		postData.add(new BasicNameValuePair(OLD_PASS, oldPass));
		postData.add(new BasicNameValuePair(NEW_PASS, newPass));
		postData.add(new BasicNameValuePair(NEW_PASS_CONFIRMATION, confirmPass));
		postData.add(new BasicNameValuePair(STREET, street));
		postData.add(new BasicNameValuePair(ZIP_CODE, zipCode));
		postData.add(new BasicNameValuePair(CITY, city));
		postData.add(new BasicNameValuePair(MOBLIE, mobile));
		postData.add(new BasicNameValuePair(USER_ID, YoursApp.userProfil.getUserNumber()));
		postData.add(new BasicNameValuePair(PASS, YoursApp.userProfil.getUserPass()));
		postData.add(new BasicNameValuePair(STATE, isPub));
		postData.add(new BasicNameValuePair("usernumber", YoursApp.userProfil.getUserNumber()));
		
		sendRequest(UPDATE_PROFILE, postData, callback);
	}

	public void setTestOrder(String OrderNumber
			,String clientid, String cost,
			 String ConfirmationNumber,
			String userID,String UserFullName, String UserEmail, String UserPhone,String First6DigitOfCreditCardNumber,
			String submenuid ,String clubnumber,String CardValidity, String CreditCardNum,
							 String cvv, String SessionId, String VoucherId, String toke,String CreditCardType
			, NetworkCallback callback){

		List<NameValuePair> postData2 = new ArrayList<NameValuePair>();
        postData2.add(new BasicNameValuePair(ORDER_ID,OrderNumber));
        postData2.add(new BasicNameValuePair("clientid",YoursApp.userProfil.getIdentUser()));
        postData2.add(new BasicNameValuePair(SAM_PAYMENT,cost));
        postData2.add(new BasicNameValuePair(RECEIPT_CONFIRMATION, "45804580"));
        postData2.add(new BasicNameValuePair(BUYER_NAME, YoursApp.userProfil.getUserFullName(" ")));
        postData2.add(new BasicNameValuePair(BUYER_ID, YoursApp.userProfil.getUserId()));
        postData2.add(new BasicNameValuePair(CLIENT_EMAIL, YoursApp.userProfil.getUserEmail()));
        postData2.add(new BasicNameValuePair(CLIENT_PHONE, YoursApp.userProfil.getUserPhone()));
        postData2.add(new BasicNameValuePair(FIRST_6_DIGIT_OF_CREDIT_CARD, "458045"));
        postData2.add(new BasicNameValuePair("submenuid",submenuid));
        postData2.add(new BasicNameValuePair("clubnumber",YoursApp.userProfil.getUserNumber()));
        String m = "";
        String y = "";
        try {
            m = CardValidity.split("/")[0];
            y = CardValidity.split("/")[1];
        } catch (Exception e) {
            // TODO: handle exception
        }
        postData2.add(new BasicNameValuePair("expmonth",m));
        postData2.add(new BasicNameValuePair("expyear",y));
        postData2.add(new BasicNameValuePair("ccno",CreditCardNum));
        postData2.add(new BasicNameValuePair("cvv",cvv));
        postData2.add(new BasicNameValuePair("soogcredit", "visa"));
        postData2.add(new BasicNameValuePair("sessionid",OrderNumber));
        postData2.add(new BasicNameValuePair("voucherid","123456789"));
        postData2.add(new BasicNameValuePair("token","123456789"));

        System.out.println("test= "+postData2.toString());

        sendRequest(BASE_URL + "ResumeOrder.svc/RegisterPayment", postData2, callback);

	}
	
	public void setOrder(String orderNumber,PelecardResult pelecardResult,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ORDER_ID,orderNumber));
		postData.add(new BasicNameValuePair("clientid",YoursApp.userProfil.getIdentUser()));
		postData.add(new BasicNameValuePair(SAM_PAYMENT, (int)pelecardResult.getDeal().getProductCost()+""));
		postData.add(new BasicNameValuePair(RECEIPT_CONFIRMATION, pelecardResult.getConfirmationNumber()));
		postData.add(new BasicNameValuePair(BUYER_NAME, YoursApp.userProfil.getUserFullName(" ")));
		postData.add(new BasicNameValuePair(BUYER_ID, YoursApp.userProfil.getUserId()));
		postData.add(new BasicNameValuePair(CLIENT_EMAIL, YoursApp.userProfil.getUserEmail()));
		postData.add(new BasicNameValuePair(CLIENT_PHONE, YoursApp.userProfil.getUserPhone()));
		postData.add(new BasicNameValuePair(FIRST_6_DIGIT_OF_CREDIT_CARD, pelecardResult.getFirst6DigitOfCreditCardNumber()));

		Log.e("pelecardresultloglog" , "result = " + pelecardResult.toString());
		postData.add(new BasicNameValuePair("submenuid",pelecardResult.getDeal().getSubMenuId()));
		postData.add(new BasicNameValuePair("clubnumber",YoursApp.userProfil.getUserNumber()));
		String m = "";
		String y = "";
		try {
			m = pelecardResult.getCreditCard().getCardValidity().split("/")[0];
			y = pelecardResult.getCreditCard().getCardValidity().split("/")[1];
		} catch (Exception e) {
			// TODO: handle exception
		}
		postData.add(new BasicNameValuePair("expmonth",m));
		postData.add(new BasicNameValuePair("expyear",y));
		postData.add(new BasicNameValuePair("ccno",pelecardResult.getCreditCard().getCreditCardSafeNumber()));
		postData.add(new BasicNameValuePair("cvv",pelecardResult.getCreditCard().getCardCVV()));
		postData.add(new BasicNameValuePair("soogcredit", CreditCard.cardsName[pelecardResult.getCreditCard().getCreditCardType()]));
		postData.add(new BasicNameValuePair("sessionid",pelecardResult.getDeal().getSessionId()));
		postData.add(new BasicNameValuePair("voucherid",pelecardResult.getVoucherId()));
		postData.add(new BasicNameValuePair("token",pelecardResult.getDealTocken()));
		Log.e("newDetailseLog", "tocken = " + pelecardResult.getDealTocken() + " vouchenrId = " + pelecardResult.getVoucherId());
		
		sendRequest(BASE_URL+"ResumeOrder.svc/RegisterPayment", postData, callback);
	}
	
	
	public void preOrder(String orderNumber,String keyMenu,String quntenty,String id,String subMenuId,String seatsBusy,String sessionID,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		if (orderNumber != null &&! orderNumber.isEmpty()){
			postData.add(new BasicNameValuePair(ORDER_NUMBER,orderNumber));
		}
		if (sessionID != null && !sessionID.isEmpty()){
			postData.add(new BasicNameValuePair("sessionid", sessionID));
		}
		postData.add(new BasicNameValuePair(KEY_MENU,keyMenu));
		postData.add(new BasicNameValuePair(CLIENT_ID,YoursApp.userProfil.getIdentUser()));
		postData.add(new BasicNameValuePair(ORDER_QUANTITY, quntenty));
		postData.add(new BasicNameValuePair(ID, id));
		postData.add(new BasicNameValuePair(SUBMENUID, subMenuId));
		postData.add(new BasicNameValuePair(SEATS_BUSY, seatsBusy));
		postData.add(new BasicNameValuePair("devicesource", "android"));
		sendRequest(PRE_ORDER, postData, callback);
	}
	
	
	
	public void preOrderPashbar(String orderID,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair("orderid", orderID));
		sendRequest(PRE_ORDER_PASHBAR, postData, callback);
	}
	
	public void getExternalItemDataContent(String url,
			NetworkCallback callback) {
		
		sendRequest(url, null, callback);
	}
	
	
	public void registerOrderInPushServer(String prouductName,PelecardResult pelecardResult,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(APP_NAME,"Yours"));
		postData.add(new BasicNameValuePair(OS, "Android"));
		postData.add(new BasicNameValuePair(AMOUNT, (int)pelecardResult.getDeal().getProductCost()+""));
		postData.add(new BasicNameValuePair(CURRENCY, "ILS"));
		postData.add(new BasicNameValuePair(UDID, DeviceUtil.getDeviceUDID()));
		postData.add(new BasicNameValuePair(PURCHASE_DESC, "Title:" + pelecardResult.getDeal().getProductName()
				+ " | Confirmation: " + pelecardResult.getConfirmationNumber() + " | VoucherId: " + pelecardResult.getVoucherId()));
		
		sendRequest(REGISTER_ORDER_IN_PUSH_SERVER, postData, callback);
	}
	
	
	public void getPashbarLink(String id,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ID, id));
		postData.add(new BasicNameValuePair(CLIENT_ID, YoursApp.userProfil.getIdentUser()));

		sendRequest(GET_PASHBAR_LINK, postData, callback);
	}
	
	public void getSinemaSityLink(String id,NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair(ID, id));
		postData.add(new BasicNameValuePair(CLIENT_ID, YoursApp.userProfil.getIdentUser()));

		sendRequest(GET_SINEMA_LINK, postData, callback);
	}
	
	
	public void checkServerVerstion(NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair("clientid",YoursApp.userProfil.getIdentUser()));

		sendRequest(CHECK_SERVER_VERSTION, postData, callback);
	}
	
	public void payFaile(String errCode,String orderID,
			NetworkCallback callback) {

		List<NameValuePair> postData = new ArrayList<NameValuePair>();
		postData.add(new BasicNameValuePair("errorcode",errCode));
		postData.add(new BasicNameValuePair("orderid", orderID));
		postData.add(new BasicNameValuePair("clientid", YoursApp.userProfil.getIdentUser()));
		postData.add(new BasicNameValuePair("errormessage", ""));

		sendRequest(BASE_URL+"ResumeOrder.svc/RegisterLogPayment",
				postData, callback);
	}


}
