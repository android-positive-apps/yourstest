/**
 * 
 */
package com.positiveapps.yoursTest.network;

import java.io.Serializable;

import org.json.JSONObject;

/**
 * @author natiapplications
 *
 */
public class ResponseObject implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	protected boolean isHasError;
	protected String content;
	protected String errorDesc;
	protected String jsonContent;
	
	public ResponseObject() {
		super();
	}
	
	public ResponseObject (JSONObject toParse){
		
		this.jsonContent = toParse.toString();
		
	}
	
     public ResponseObject (String toParse){
		
		this.jsonContent = toParse;
		
	}

	/**
	 * @return the isHasError
	 */
	public boolean isHasError() {
		return isHasError;
	}

	/**
	 * @param isHasError the isHasError to set
	 */
	public void setHasError(boolean isHasError) {
		this.isHasError = isHasError;
	}

	/**
	 * @return the errorDesc
	 */
	public String getErrorDesc() {
		return errorDesc;
	}

	/**
	 * @param errorDesc the errorDesc to set
	 */
	public void setErrorDesc(String errorDesc) {
		this.errorDesc = errorDesc;
	}

	/**
	 * @return the xmlContent
	 */
	public String getXmlContent() {
		return jsonContent;
	}

	/**
	 * @param xmlContent the xmlContent to set
	 */
	public void setXmlContent(String xmlContent) {
		this.jsonContent = xmlContent;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	

	
	
	
	

}
