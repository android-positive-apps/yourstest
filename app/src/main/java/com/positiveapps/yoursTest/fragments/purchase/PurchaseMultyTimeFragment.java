/**
 * 
 */
package com.positiveapps.yoursTest.fragments.purchase;



import java.util.ArrayList;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.adapters.ItemTimesListAdapter;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.ItemDate;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.ui.YoursItemView;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class PurchaseMultyTimeFragment extends BaseFragment implements OnItemClickListener{
	
	public static final String 	EXTRA_YOURS_ITEM = "YoursItem";
	
	private LinearLayout itemFrame;
	private ListView timesList;
	private ItemTimesListAdapter adapter;
	private ArrayList<ItemDate> itemsArray;
	private YoursItem selectedItem;
	

	public PurchaseMultyTimeFragment() {

	}

	public static PurchaseMultyTimeFragment newInstance(YoursItem yoursItem) {
		PurchaseMultyTimeFragment instance = new PurchaseMultyTimeFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_YOURS_ITEM, yoursItem);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_prochase_moltiple_times, container,
				false);
		selectedItem = (YoursItem)getArguments().getSerializable(EXTRA_YOURS_ITEM);
		return rootView;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.setScreenTitle(getString(R.string.choose_time_title));
		itemFrame = (LinearLayout)view.findViewById(R.id.item_frame);
		timesList = (ListView)view.findViewById(R.id.times_list);
		timesList.setBackgroundDrawable(Category.CATEGORIES_TIME_LIST_BG.getDrawable(selectedItem.getParentType()));
		
		addItemFrameView ();
		itemsArray = selectedItem.getDates();
		
		updateTimesListView (itemsArray);
		timesList.setOnItemClickListener(this);
		
	}

	
	/**
	 * 
	 */
	private void updateTimesListView(ArrayList<ItemDate> items) {
		adapter = new ItemTimesListAdapter(items);
		timesList.setAdapter(adapter);
	}
	


	private void addItemFrameView() {
		YoursItemView yoursItemView = new YoursItemView(selectedItem, null);
		itemFrame.addView(yoursItemView.getItemView());
	}

	
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Log.e("listlog", "onitemclicklistener");
		
		if (selectedItem.getDates().get(arg2).isItemIsOver()){
			return;
		}
		if (selectedItem.getStatus() == YoursItem.STATUS_PASHBAR){
			
			getPashBarLink(selectedItem.getDates().get(arg2));
			/**/
			return;
		}
		if (selectedItem.isChoosable()){
			if (selectedItem.getStatus() == YoursItem.STATUS_EXTERNAL){
				FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
						PurchaseChooseSeatPashbarFragment.newInstance(selectedItem,
								selectedItem.getDates().get(arg2),
								PurchaseChooseSeatPashbarFragment.TYPE_PASHBAR), R.id.content_frame);
				return;
			}
			int areaCount = selectedItem.getAreasByPlaceAndDate(selectedItem.getDates().get(arg2).getPlace(),
					selectedItem.getDates().get(arg2).getDate()+selectedItem.getDates().get(arg2).getTime()).size();
			if (areaCount > 1){
				FragmentsUtil.openFragment(getFragmentManager(),
						PurchaseChooseAreaFragment.newInstance(selectedItem,selectedItem.getDates().get(arg2)), R.id.content_frame);
			}else{
				FragmentsUtil.openFragment(getFragmentManager(),
						PurchaseChooseSeatFragment.newInstance(selectedItem,
								selectedItem.getAreasByPlaceAndDate(selectedItem.getDates().get(arg2).getPlace()
										,selectedItem.getDates().get(arg2).getDate()+selectedItem.getDates().get(arg2).getTime()).get(0)
								,selectedItem.getDates().get(arg2)), R.id.content_frame);
			}
		}else{
			FragmentsUtil.openFragment(getFragmentManager(),
					PurchaseNoChooseSeatsFragment.newInstance(selectedItem,selectedItem.getDates().get(arg2)), R.id.content_frame);
		}
		
	}

	
	private void getPashBarLink(final ItemDate itemDate) {
		showProgressDialog();
		String id = itemDate.getId()+"";
		YoursApp.networkManager.getPashbarLink(id, new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				Log.e("pahsbarlog", "res = " + response.toString());
				try {
					JSONObject jsonObject = new JSONObject(response.getXmlContent());
					String phasbarLink = jsonObject.optString("GetRequestPashBarResult").replace("\"", "");
					selectedItem.setExternalLink(phasbarLink);
					selectedItem.setSessionID(phasbarLink,PurchaseChooseSeatPashbarFragment.TYPE_PASHBAR);
					FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
							PurchaseChooseSeatPashbarFragment.newInstance(selectedItem,
									itemDate,PurchaseChooseSeatPashbarFragment.TYPE_PASHBAR), R.id.content_frame);
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
			}
			
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
		
	}
	
	

	


}
