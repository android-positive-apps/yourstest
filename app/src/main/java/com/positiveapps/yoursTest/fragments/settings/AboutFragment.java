/**
 * 
 */
package com.positiveapps.yoursTest.fragments.settings;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.parser.RuleParser;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class AboutFragment extends BaseFragment{
	
	
	
	private WebView aboutWebView;
	private ProgressBar webProgressBar;
	
	private String aboutUrl = "http://www.yours.co.il/PageTemp1.aspx?ID=3";
	
	

	public AboutFragment() {

	}

	public static AboutFragment newInstance() {
		AboutFragment instance = new AboutFragment();
		
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings_about, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		aboutWebView = (WebView)view.findViewById(R.id.web_view);
		webProgressBar = (ProgressBar)view.findViewById(R.id.web_progress);
		
		getRule();
				
	}
	
	private void getRule (){
		YoursApp.networkManager.getRules(String.valueOf(3), new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(String toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				
				
				
				String html = new RuleParser(response.getXmlContent(),RuleParser.TYPE_ABOUT).parse();
				Log.e("xmllog" , "xml after parsing - " + html);
				
				String mime = "text/html";
				String encoding = "utf-8";

				//aboutWebView.getSettings().setJavaScriptEnabled(true);
				aboutWebView.loadDataWithBaseURL("", html, mime, encoding, "");
				aboutWebView.setWebViewClient(new WebViewClient() {

					   public void onPageFinished(WebView view, String url) {
						   webProgressBar.setVisibility(View.GONE);
					    }
					});
			}
			
			
			@Override
			public void onError() {
				// TODO Auto-generated method stub
				super.onError();
				webProgressBar.setVisibility(View.GONE);
				ToastUtil.toster(getString(R.string.general_network_error), false);
			}
			
			@Override
			public void networkUnavailable(String message) {
				// TODO Auto-generated method stub
				super.networkUnavailable(message);
				webProgressBar.setVisibility(View.GONE);
			}
		});
	}


}
