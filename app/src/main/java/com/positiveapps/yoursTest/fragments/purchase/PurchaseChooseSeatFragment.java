/**
 * 
 */
package com.positiveapps.yoursTest.fragments.purchase;



import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.dialogs.DialogManager;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.Area;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.FullSeat;
import com.positiveapps.yoursTest.objects.ItemDate;
import com.positiveapps.yoursTest.objects.Point;
import com.positiveapps.yoursTest.objects.RowTicket;
import com.positiveapps.yoursTest.objects.SamObject;
import com.positiveapps.yoursTest.objects.SamTicket;
import com.positiveapps.yoursTest.objects.Seat;
import com.positiveapps.yoursTest.objects.Seat.OnSeatClickListener;
import com.positiveapps.yoursTest.objects.SeatRow;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.ui.AreaView;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class PurchaseChooseSeatFragment extends BaseFragment implements OnClickListener,OnSeatClickListener{
	
	public static final String 	EXTRA_YOURS_ITEM = "YoursItem";
	public static final String 	EXTRA_AREA = "Area";
	public static final String 	EXTRA_DATE = "Date";
	
	private final int[] ZOOM = {15,30,60,120,240};
	
	private LinearLayout itemFrame;
	private LinearLayout seatContainer;
	private Button continueBtn;
	private ImageView zoomInBtn;
	private ImageView zoomOutBtn;
	
	private YoursItem selectedItem;
	private ItemDate itemDate;
	private Area selectedArea;
	private ArrayList<SeatRow> seatRows;
	private ArrayList<Seat> selectedSeats = new ArrayList<Seat>();
	private ArrayList<FullSeat> fullSeatsArray = new ArrayList<FullSeat>();
	private int currentZoomFactor = 2;

	public PurchaseChooseSeatFragment() {

	}

	public static PurchaseChooseSeatFragment newInstance(YoursItem yoursItem,Area area,ItemDate date) {
		PurchaseChooseSeatFragment instance = new PurchaseChooseSeatFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_YOURS_ITEM, yoursItem);
		bundle.putSerializable(EXTRA_AREA, area);
		bundle.putSerializable(EXTRA_DATE, date);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_prochase_choose_seat, container,
				false);
		selectedItem = (YoursItem)getArguments().getSerializable(EXTRA_YOURS_ITEM);
		selectedArea = (Area) getArguments().getSerializable(EXTRA_AREA);
		itemDate = (ItemDate)getArguments().getSerializable(EXTRA_DATE);
		
		//seatRows = selectedItem.getPlaceMapByPlaceName(itemDate.getPlace()).getRows();
		seatRows = selectedArea.getPlaceMap().getRows();
		
		for (int i = 0; i < seatRows.size(); i++) {
			for (int j = 0; j < seatRows.get(i).getSeats().size(); j++) {
				if (seatRows.get(i).getSeats().get(j).getPrice() <=0 &&
						seatRows.get(i).getSeats().get(j).getStatus() != Seat.STATUS_EMPTY){
					seatRows.get(i).getSeats().get(j).setStatus(Seat.STATUS_FULL);
				}
			}
		}
		return rootView;
	}

	
	
	
	@SuppressWarnings("deprecation")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.setScreenTitle(getString(R.string.chosse_seat_title));
		itemFrame = (LinearLayout)view.findViewById(R.id.item_frame);
		seatContainer = (LinearLayout)view.findViewById(R.id.seat_container);
		continueBtn = (Button)view.findViewById(R.id.continue_btn);
		zoomInBtn = (ImageView)view.findViewById(R.id.zoom_in_btn);
		zoomOutBtn = (ImageView)view.findViewById(R.id.zoom_out_btn);
		zoomInBtn.setOnClickListener(this);
		zoomOutBtn.setOnClickListener(this);
		
		continueBtn.setOnClickListener(this);
		
		continueBtn.setBackgroundDrawable(Category.CATEGORIES_CONTINUE_BUTTONS.
				getDrawable(selectedItem.getParentType()));
		
		addItemFrameView ();
		selectedSeats.clear();
		fullSeatsArray.clear();
		getFullSeats();
	}

	
	
	private void getFullSeats () {
		showProgressDialog();
		
		YoursApp.networkManager.getFullSeats(selectedArea.getItemId() + "" , new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				try {
					Log.e("fullSeatlog", "result - " + response.getXmlContent());
					JSONObject jsonObject = new JSONObject(response.getXmlContent());
					JSONArray fullSeatsJA = new JSONArray(jsonObject.optString("GetOcupedSeatsResult"));
					for (int i = 0; i < fullSeatsJA.length(); i++) {
						FullSeat temp = new FullSeat(new JSONObject(fullSeatsJA.getString(i)));
						fullSeatsArray.add(temp);
					}
				} catch (Exception e) {
				}
				for (int i = 0; i < fullSeatsArray.size(); i++) {
					FullSeat temp = fullSeatsArray.get(i);
					Seat tempSeat = getSeatBySeatNumber(temp);
					if(tempSeat != null){
						tempSeat.setStatus(Seat.STATUS_FULL);
					}
				}
				drowAreaMap();
			}
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster( getString(R.string.general_network_error), false);
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
	}
	

	private Seat getSeatBySeatNumber (FullSeat fullSeat){
		for (int i = 0; i < seatRows.size(); i++) {
			for (int j = 0; j < seatRows.get(i).getSeats().size(); j++) {
				Seat temp = seatRows.get(i).getSeats().get(j);
				if(temp.getSeatNumber() == fullSeat.getX() && fullSeat.getY() == temp.getRowIndex()){
					return temp;
				}
			}
		}
		return null;
	}
	
	private void addItemFrameView() {
		/*YoursItemView yoursItemView = new YoursItemView(selectedItem, null);
		itemFrame.addView(yoursItemView.getItemView());*/
		AreaView areaView = new AreaView(selectedArea, null);
		itemFrame.addView(areaView.getView());
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.continue_btn:
				int samOrder = selectedSeats.size();
				if (selectedItem.getMaxOrder() > 0){
					if (selectedItem.getMaxOrder() < samOrder) {
						DialogManager.showErrorDialog(this, getString(R.string.out_of_max_buy_order_message) + selectedItem.getMaxOrder(), null);
						return;
					}
				}
				notifyServerAboutSelectedSeats();
				break;
			case R.id.zoom_in_btn:
				currentZoomFactor++;
				if (currentZoomFactor > 4){
					currentZoomFactor =4;
				}
				drowAreaMap();
				break;
			case R.id.zoom_out_btn:
				currentZoomFactor--;
				if (currentZoomFactor <= 0){
					currentZoomFactor = 0;
				}
				drowAreaMap();
				break;
		}
	}

	
	
	private void drowAreaMap() {
		if (seatRows == null){
			return;
		}
		for (int i = 0; i < seatRows.size(); i++) {
			for (int j = 0; j < seatRows.get(i).getSeats().size(); j++) {
				seatRows.get(i).getSeats().get(j).setIndex(new Point(i, j));
				seatRows.get(i).getSeats().get(j).setSize(ZOOM[currentZoomFactor]);
			}
			Log.e("seatindexlog", "**************");
		}
		seatContainer.removeAllViews();
		for (int i = 0; i < seatRows.size(); i++) {
			seatContainer.addView(seatRows.get(i).getView(this));
		}
		
	}

	@Override
	public void onSeatClick(Seat seat, View view) {
		ToastUtil.toster("seat number = " + seat.getSeatNumber() + " row = " + seat.getRowIndex(), false);
		if (seat.getStatus() == Seat.STATUS_FREE){
			
			if (view.isSelected()){
				view.setSelected(false);
				selectedSeats.remove(seat);
				//seatRows.get(seat.getIndex().x).getSeats().get(seat.getIndex().y).setSelected(false);
				seat.setSelected(false);
			}else{
				view.setSelected(true);
				selectedSeats.add(seat);
				seat.setSelected(true);
				//seatRows.get(seat.getIndex().x).getSeats().get(seat.getIndex().y).setSelected(true);
				
			}
		}
	}
	
	private void openSamProchaseFragment (int clubPriceAmount,int samPrice,int terminal,String userName,String pass){
		SamTicket samTicket = new SamTicket();
		samTicket.setPlace(selectedItem.getItemName());
		if (selectedSeats.size() == 0){
			com.positiveapps.yoursTest.util.ToastUtil.toster("you must choose seat befor continue !", true);
			return;
		}
		ArrayList<RowTicket> tickets = new ArrayList<RowTicket>();
		
		for (int i = 0; i < clubPriceAmount; i++) {
			RowTicket rowTicket = new RowTicket();
			rowTicket.setRow(selectedSeats.get(i).getRowIndex());
			rowTicket.setSeat(selectedSeats.get(i).getSeatNumber());
			rowTicket.setTicketType(getString(R.string.club_price_type));
			rowTicket.setMonth(Calendar.getInstance().get(Calendar.MONTH));
			rowTicket.setPrice(selectedItem.getItemClubPrice());
			tickets.add(rowTicket);
		}
		
		
		for (int i = clubPriceAmount; i < selectedSeats.size(); i++) {
			RowTicket rowTicket = new RowTicket();
			rowTicket.setRow(selectedSeats.get(i).getRowIndex());
			rowTicket.setSeat(selectedSeats.get(i).getSeatNumber());
			rowTicket.setTicketType(getString(R.string.regular_price_type));
			rowTicket.setMonth(Calendar.getInstance().get(Calendar.MONTH));
			rowTicket.setPrice(selectedItem.getItemOriginalPrice());
			tickets.add(rowTicket);
		}
		samTicket.setTickets(tickets);
		samTicket.setSamPrice(samPrice);
		PurchaseSumFragment samFrag = PurchaseSumFragment.newInstance(SamObject.TYPE_TICKET, samTicket,terminal,userName,pass);
		samFrag.setYoursItem(selectedItem);
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
				samFrag, R.id.content_frame);
		
	}
	
	private String getSelectedSeatsAsString (){
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < selectedSeats.size(); i++) {
			stringBuilder.append(selectedSeats.get(i).toString());
			if(i < selectedSeats.size()-1){
				stringBuilder.append("|");
			}
		}
		Log.d("selectedSeatsLog", "string: " + stringBuilder.toString());
		return stringBuilder.toString();
	}
	
	private void notifyServerAboutSelectedSeats () {
		showProgressDialog();
		Log.e("seatBuys", "string - " + getSelectedSeatsAsString());
		
		String id = selectedItem.getId()+"";
		if (itemDate != null){
			id = itemDate.getId()+"";
			
		}
		if (selectedArea != null){
			id = selectedArea.getPlaceID();
		}
		
		YoursApp.networkManager.preOrder(selectedItem.getActionNumber()+"",selectedItem.getKeyMenu()+"",selectedSeats.size()+"",
				id, selectedItem.getSubMenuId()+"", getSelectedSeatsAsString(),
				selectedItem.getSessionID(),new NetworkCallback(){
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				Log.e("setorderseatslog", "result = " + response.getXmlContent());
				String result = response.getXmlContent();
				String content = "";
				try {
					JSONObject jsonObject = new JSONObject(result);
					content = jsonObject.optString("PreOrderResult");
					JSONArray jsonArray = new JSONArray(content);
					JSONObject jsonResult = new JSONObject(jsonArray.optString(0));
					selectedItem.setActionNumber(jsonResult.optInt("ordernumber"));
					int clabPriceAmount = jsonResult.optInt("subsided");
					int samPrice = jsonResult.optInt("total");
					int terminal = jsonResult.optInt("PeleCarddTerminal");
					String userName = jsonResult.optString("PeleCarddUser");
					String pelecardPass = jsonResult.optString("PeleCarddPass");
					

					
					openSamProchaseFragment(clabPriceAmount,samPrice,terminal,userName,pelecardPass);
				} catch (Exception e) {
					showErrorDialog(content);
				} 
				
			}
			
			@Override
			public void onError() {
				super.onError();
				dismisProgressDialog();
				ToastUtil.toster( getString(R.string.general_network_error), false);
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
	}

	protected void showErrorDialog(String content) {
		if (content != null && !content.isEmpty()){
			content = content.replace("error:", "");
		}else{
			content = getString(R.string.general_network_error);
		}
		DialogManager.showErrorDialog(this, content, null);
	}


}
