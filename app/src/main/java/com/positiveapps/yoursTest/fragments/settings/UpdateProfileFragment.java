/**
 * 
 */
package com.positiveapps.yoursTest.fragments.settings;


import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.UpdtatUserResponse;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class UpdateProfileFragment extends BaseFragment implements OnClickListener{
	
	
	
	private Button sendUpdateBtn;
	private EditText firstNameEt;
	private EditText lastNameEt;
	private EditText uniqIdEt;
	private EditText oldPassEt;
	private EditText newPassEt;
	private EditText confirmPassEt;
	private EditText streetEt;
	private EditText zipCodeEt;
	private EditText cityEt;
	private EditText telphoneEt;
	private EditText phoneEt;
	private EditText mailEt;
	private CheckBox isPubCB;
	
	boolean isFillDetails;
	
	
	

	public UpdateProfileFragment() {

	}

	public static UpdateProfileFragment newInstance() {
		UpdateProfileFragment instance = new UpdateProfileFragment();
		
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings_update_profile, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		sendUpdateBtn = (Button)view.findViewById(R.id.send_update_btn);
		firstNameEt = (EditText)view.findViewById(R.id.first_name_et);
		lastNameEt = (EditText)view.findViewById(R.id.familly_name_et);
		uniqIdEt = (EditText)view.findViewById(R.id.uniq_id_et);
		oldPassEt = (EditText)view.findViewById(R.id.old_pass_et);
		newPassEt = (EditText)view.findViewById(R.id.new_pass_et);
		confirmPassEt = (EditText)view.findViewById(R.id.set_pass_again_et);
		streetEt = (EditText)view.findViewById(R.id.address_et);
		zipCodeEt = (EditText)view.findViewById(R.id.zip_code_et);
		cityEt = (EditText)view.findViewById(R.id.mobile_et);
		telphoneEt = (EditText)view.findViewById(R.id.telephone_et);
		phoneEt = (EditText)view.findViewById(R.id.phone_et);
		mailEt = (EditText)view.findViewById(R.id.email_et);
		isPubCB = (CheckBox)view.findViewById(R.id.confirm_get_data_cb);
		if (!isFillDetails){
			fillUserDetails ();
		}
		
		
		sendUpdateBtn.setOnClickListener(this);
				
	}

	
	
	private void fillUserDetails() {
		firstNameEt.setText(YoursApp.userProfil.getUserFirstName());
		lastNameEt.setText(YoursApp.userProfil.getUserLastName());
		uniqIdEt.setText(YoursApp.userProfil.getUserId());
		cityEt.setText(YoursApp.userProfil.getUserCity());
		zipCodeEt.setText(YoursApp.userProfil.getUserZipCode());
		streetEt.setText(YoursApp.userProfil.getUserAddress());
		telphoneEt.setText(YoursApp.userProfil.getUserTelephone());
		phoneEt.setText(YoursApp.userProfil.getUserPhone());
		mailEt.setText(YoursApp.userProfil.getUserEmail());
		isPubCB.setChecked(YoursApp.userProfil.isUserPublic());
		isFillDetails = true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.send_update_btn:
				performSendUpdateProfile();
				break;
		}
	}
	
	private void performSendUpdateProfile () {
		showProgressDialog();
		
		String isPub = "0";
		if (isPubCB.isChecked()){
			isPub = "1";
		}
		
		YoursApp.networkManager.updateUserProfile
		(firstNameEt.getText().toString(), lastNameEt.getText().toString(),
				oldPassEt.getText().toString(), newPassEt.getText().toString(),
				confirmPassEt.getText().toString(), streetEt.getText().toString(),
				zipCodeEt.getText().toString(), cityEt.getText().toString(), phoneEt.getText().toString(),isPub, new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				UpdtatUserResponse updtatUserResponse = new UpdtatUserResponse(toParse);
				return updtatUserResponse;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				Log.e("loginlog", "result - " + response.getXmlContent());
				dismisProgressDialog();
				ToastUtil.toster(response.getErrorDesc(), false);
				if (!response.isHasError()){
					getActivity().finish();
				}
				
			}
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
				
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}		
							
		});
	}
	


}
