/**
 * 
 */
package com.positiveapps.yoursTest.fragments.purchase;



import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.ItemDate;
import com.positiveapps.yoursTest.objects.RowTicket;
import com.positiveapps.yoursTest.objects.SamObject;
import com.positiveapps.yoursTest.objects.SamTicket;
import com.positiveapps.yoursTest.objects.Seat;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.ui.YoursItemView;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class PurchaseChooseSeatPashbarFragment extends BaseFragment implements OnClickListener{
	
	public static final String 	EXTRA_YOURS_ITEM = "YoursItem";
	public static final String 	EXTRA_DATE = "Date";
	public static final String 	EXTRA_TYPE = "Type";
	
	public static final int TYPE_PASHBAR = 11;
	public static final int TYPE_CINEMA = 12;
	

	
	private WebView pahsbarWebView;
	private ProgressBar webProgressBar;
	private LinearLayout itemFrame;
	private ImageView closeBtn;
	private YoursItem selectedItem;
	private ItemDate itemDate;
	public ArrayList<Seat> selectedSeats;
	private boolean isLoadSamFragment;
	private int currentType;
	public PurchaseChooseSeatPashbarFragment() {

	}

	public static PurchaseChooseSeatPashbarFragment newInstance(YoursItem yoursItem,ItemDate date,int type) {
		PurchaseChooseSeatPashbarFragment instance = new PurchaseChooseSeatPashbarFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_YOURS_ITEM, yoursItem);
		bundle.putSerializable(EXTRA_DATE, date);
		bundle.putInt(EXTRA_TYPE, type);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_prochase_choose_seat_pahsbar, container,
				false);
		selectedItem = (YoursItem)getArguments().getSerializable(EXTRA_YOURS_ITEM);
		itemDate = (ItemDate)getArguments().getSerializable(EXTRA_DATE);
		currentType = getArguments().getInt(EXTRA_TYPE);
		return rootView;
	}

	
	
	
	@SuppressWarnings("deprecation")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.setScreenTitle(getString(R.string.chosse_seat_title));
		pahsbarWebView = (WebView)view.findViewById(R.id.pashbar_web_view_screen_s);
		itemFrame = (LinearLayout)view.findViewById(R.id.item_frame);
		webProgressBar = (ProgressBar)view.findViewById(R.id.web_progress_bar);
		closeBtn = (ImageView)view.findViewById(R.id.close_btn);
		closeBtn.setOnClickListener(this);
		
		
		pahsbarWebView.getSettings().setBuiltInZoomControls(true);
		pahsbarWebView.requestFocus();
		pahsbarWebView.getSettings().setJavaScriptEnabled(true);
		pahsbarWebView.getSettings().setUseWideViewPort(true);
		pahsbarWebView.getSettings().setLoadsImagesAutomatically(true);
		pahsbarWebView.setWebViewClient(new MyBrowser());
		//pahsbarWebView.setWebChromeClient(new MyWebChromeClient());
		pahsbarWebView.loadUrl(selectedItem.getExternalLink());
		
		
		//addItemFrameView ();
	}
	int counter = 0;
	private class MyBrowser extends WebViewClient {
		  
	      @Override
	      public boolean shouldOverrideUrlLoading(WebView view, String url) {
	         view.loadUrl(url);
	         return true;
	      }
	      
	      @Override
	      public void onPageFinished(WebView view, String url) {
			   webProgressBar.setVisibility(View.GONE);
			   
			   Log.e("urllog", "url - yours.co.il" + url);
			   if (currentType == TYPE_PASHBAR){
				   if (url.contains("http://www.yours.co.il/mofa_order.aspx?session_id=")){
					   counter++;
					   if (!isLoadSamFragment&&counter==1){
						   isLoadSamFragment = true;
						   counter = 0;
						   getSeatsBeforOpenningSamFragment();
					   }
				   } 
			   }else if (currentType == TYPE_CINEMA){
				   if (url.contains("yours.co.il")&&!url.contains("cinema-city.co.il")){
					   counter++;
					   if (!isLoadSamFragment&&counter==1){
						   isLoadSamFragment = true;
						   counter = 0;
						   getSeatsBeforOpenningSamFragment();
					   }
				   } 
			   }
			   
		    }
	   }
	
	/*private class MyWebChromeClient extends WebChromeClient {
	      
		  //display alert message in Web View
		  @Override
		     public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
		        
		         new AlertDialog.Builder(view.getContext())
		          .setMessage(message).setCancelable(true).show();
		         result.confirm();
		         return true;
		     }
		 
		 }*/
	
	private void addItemFrameView() {
		YoursItemView yoursItemView = new YoursItemView(selectedItem, null);
		itemFrame.addView(yoursItemView.getItemView());
	}

	
	
	private void openSamProchaseFragment (int clubPriceAmount,int regularPriceAmount,int samPrice
			,ArrayList<PreOrderItem> items){
		
		final SamTicket samTicket = new SamTicket();
		samTicket.setPlace(selectedItem.getItemName());
		
		ArrayList<RowTicket> tickets = new ArrayList<RowTicket>();
		
		for (int i = 0; i < items.size(); i++) {
			RowTicket rowTicket = new RowTicket();
			rowTicket.setRow(items.get(i).getRowNum());
			rowTicket.setSeat(items.get(i).getSeatNum());
			if (i < regularPriceAmount){
				rowTicket.setTicketType(getString(R.string.regular_price_type));
			}else{
				rowTicket.setTicketType(getString(R.string.club_price_type));
			}
			rowTicket.setMonth(Calendar.getInstance().get(Calendar.MONTH));
			rowTicket.setPrice(items.get(i).getPrice());
			tickets.add(rowTicket);
		}
		
		
		samTicket.setTickets(tickets);
		samTicket.setSamPrice(samPrice);
		
		PurchaseSumFragment samFrag = PurchaseSumFragment.newInstance
				(SamObject.TYPE_TICKET, samTicket,items.get(0).getTerminal(),items.get(0).getPelecardUserName()
						,items.get(0).getPelecadPass());
		samFrag.setYoursItem(selectedItem);
		getFragmentManager().popBackStack();
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
				samFrag, R.id.content_frame);
		
	}
	
	private void getSeatsBeforOpenningSamFragment (){
		String orderId = selectedItem.getSessionID();
		/*if (currentType == TYPE_CINEMA){
			orderId = selectedItem.getActionNumber()+"";
		}*/
		YoursApp.networkManager.preOrderPashbar(orderId, new NetworkCallback(){
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				Log.e("setorderseatslog", "result = " + response.getXmlContent());
				String result = response.getXmlContent();
				try {
					
					JSONObject jsonObject = new JSONObject(result);
					JSONArray jsonArray = new JSONArray(jsonObject.optString("PreOrder2Result"));
					
					
					ArrayList<PreOrderItem> items = new ArrayList<PreOrderItem>();
					for (int i = 0; i < jsonArray.length(); i++) {
						PreOrderItem temp = new PreOrderItem(new JSONObject(jsonArray.optString(i)));
						items.add(temp);
					}
					if (items.size() > 0){
						selectedItem.setActionNumber(items.get(0).getOrderNumber());
						openSamProchaseFragment(items.get(0).getDiscountCount(),
								items.get(0).getRegolarPriceCount(), 
								items.get(0).getTotalPrice(), items);
					}else{
						if (counter <= 3){
							getSeatsBeforOpenningSamFragment();
							counter++;
						}else{
							ToastUtil.toster(getString(R.string.general_network_error), false);
							getFragmentManager().popBackStack();
							getFragmentManager().popBackStack();
						}
						 
					}
					
				} catch (Exception e) {
					e.printStackTrace();
					if (counter <= 3){
						getSeatsBeforOpenningSamFragment();
						counter++;
					}else{
						ToastUtil.toster(getString(R.string.general_network_error), false);
						getFragmentManager().popBackStack();
						getFragmentManager().popBackStack();
					}
				} 
				
			}
			
			@Override
			public void onError() {
				super.onError();
				dismisProgressDialog();
				isLoadSamFragment = false;
				ToastUtil.toster(getString(R.string.general_network_error), false);
				getFragmentManager().popBackStack();
				getFragmentManager().popBackStack();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
	}
	
	
	public ArrayList<Seat> createSeatsBySeatsStringFormat (String seatsFormat){
		return new ArrayList<Seat>();
	}
	
	

	/*public void notifyServerAboutSelectedSeats () {
		showProgressDialog();
		String keyMenu  = "3";
		if (currentType == TYPE_CINEMA){
			keyMenu = "6";
		}
		String id = selectedItem.getId()+"";
		if (itemDate != null){
			id = itemDate.getId()+"";
		}
		
		YoursApp.networkManager.preOrder(this.selectedItem.getActionNumber()+"",keyMenu,"",
				id, this.selectedItem.getSubMenuId()+"", "",selectedItem.getSessionID(), new NetworkCallback(){
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				Log.e("setorderseatslog", "result = " + response.getXmlContent());
				String result = response.getXmlContent();
				try {
					
					JSONObject jsonObject = new JSONObject(result);
					JSONArray jsonArray = new JSONArray(jsonObject.optString("PreOrderResult"));
					JSONObject jsonResult = new JSONObject(jsonArray.optString(0));
					selectedItem.setActionNumber(jsonResult.optInt("ordernumber"));
					int clabPriceAmount = jsonResult.getInt("subsided");
					int regularPriceAmount = jsonResult.getInt("regularprice");
					int samPrice = jsonResult.getInt("total");
					openSamProchaseFragment(clabPriceAmount,regularPriceAmount,samPrice);
				} catch (Exception e) {
					ToastUtil.toster(result, false);
					e.printStackTrace();
					isLoadSamFragment = false;
				} 
				
			}
			
			@Override
			public void onError() {
				super.onError();
				dismisProgressDialog();
				ToastUtil.toster(getString(R.string.general_network_error), false);
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
	}*/


	
	@Override
	public void onClick(View v) {
		getFragmentManager().popBackStack();
		getFragmentManager().popBackStack();
	}
	

	private class PreOrderItem {
		private int rowNum;
		private int seatNum;
		private int price;
		private int discountCount;
		private int regolarPriceCount;
		private int totalPrice;
		private int orderNumber;
		private int terminal;
		private String pelecardUserName;
		private String pelecadPass;
		private PreOrderItem (JSONObject toParse){
			this.rowNum = toParse.optInt("ROW");
			this.seatNum = toParse.optInt("SEAT");
			this.price = toParse.optInt("PRICE");
			this.discountCount = toParse.optInt("subsided");
			this.regolarPriceCount = toParse.optInt("regularprice");
			this.totalPrice = toParse.optInt("total");
			this.orderNumber = toParse.optInt("ordernumber");
			this.terminal = toParse.optInt("PeleCarddTerminal");
			this.pelecardUserName = toParse.optString("PeleCarddUser","");
			this.pelecadPass = toParse.optString("PeleCarddPass","");
			
		}

		/**
		 * @return the pelecardUserName
		 */
		public String getPelecardUserName() {
			return pelecardUserName;
		}

		/**
		 * @param pelecardUserName the pelecardUserName to set
		 */
		public void setPelecardUserName(String pelecardUserName) {
			this.pelecardUserName = pelecardUserName;
		}

		/**
		 * @return the pelecadPass
		 */
		public String getPelecadPass() {
			return pelecadPass;
		}

		/**
		 * @param pelecadPass the pelecadPass to set
		 */
		public void setPelecadPass(String pelecadPass) {
			this.pelecadPass = pelecadPass;
		}

		/**
		 * @return the terminal
		 */
		public int getTerminal() {
			return terminal;
		}

		/**
		 * @param terminal the terminal to set
		 */
		public void setTerminal(int terminal) {
			this.terminal = terminal;
		}

		/**
		 * @return the rowNum
		 */
		public int getRowNum() {
			return rowNum;
		}

		/**
		 * @return the seatNum
		 */
		public int getSeatNum() {
			return seatNum;
		}

		
		/**
		 * @return the price
		 */
		public int getPrice() {
			return price;
		}

		/**
		 * @return the discountCount
		 */
		public int getDiscountCount() {
			return discountCount;
		}


		/**
		 * @return the regolarPriceCount
		 */
		public int getRegolarPriceCount() {
			return regolarPriceCount;
		}

		/**
		 * @return the totalPrice
		 */
		public int getTotalPrice() {
			return totalPrice;
		}

		/**
		 * @return the orderNumber
		 */
		public int getOrderNumber() {
			return orderNumber;
		}

		
		
		
		
	}

}
