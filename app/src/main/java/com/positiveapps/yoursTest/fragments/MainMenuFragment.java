/**
 * 
 */
package com.positiveapps.yoursTest.fragments;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;


import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.adapters.BannersPagerAdapter;
import com.positiveapps.yoursTest.animations.DepthPageTransformer;
import com.positiveapps.yoursTest.dialogs.DialogCallback;
import com.positiveapps.yoursTest.dialogs.DialogManager;
import com.positiveapps.yoursTest.fragments.BunnerFragment.OnBunnerClickListener;
import com.positiveapps.yoursTest.fragments.purchase.PurchaseChooseSeatPashbarFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.search.SearchCallback;
import com.positiveapps.yoursTest.search.SearchTask;
import com.positiveapps.yoursTest.ui.ViewPagerCustomDuration;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;



/**
 * @author natiapplications
 *
 */
public class MainMenuFragment extends BaseFragment implements OnClickListener,OnBunnerClickListener {
	
	
	
	private ViewPagerCustomDuration bannerPager;
	private BannersPagerAdapter bannerAdapter;
	
	private int pagerCounter;
	private boolean changeCounterMethod;
	private BannerAoutoChangeThread bannerAoutoChangeThread;
	
	private Button famillyBtn;
	private Button prochaseBtn;
	private Button funBtn;
	private Button hobyBtn;
	private Button searchBtn;
	private Button hotMonthBtn;
	private ProgressBar bunnersProgressBar;

	private ArrayList<YoursItem> bunners = new ArrayList<YoursItem>();
	
	public MainMenuFragment() {

	}

	public static MainMenuFragment newInstance() {
		MainMenuFragment instance = new MainMenuFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main_menu, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.setScreenTitle(null);
		
		famillyBtn = (Button) view.findViewById(R.id.familly_btn);
		funBtn = (Button) view.findViewById(R.id.fun_btn);
		hobyBtn = (Button) view.findViewById(R.id.hoby_btn);
		prochaseBtn = (Button) view.findViewById(R.id.prochase_btn);
		searchBtn = (Button) view.findViewById(R.id.search_btn);
		hotMonthBtn = (Button) view.findViewById(R.id.hot_month_btn);
		bunnersProgressBar = (ProgressBar)view.findViewById(R.id.banner_progress_bar);
		
		famillyBtn.setOnClickListener(this);
		funBtn.setOnClickListener(this);
		hobyBtn.setOnClickListener(this);
		prochaseBtn.setOnClickListener(this);
		searchBtn.setOnClickListener(this);
		hotMonthBtn.setOnClickListener(this);
		
		bannerPager = (ViewPagerCustomDuration)view.findViewById(R.id.pager);
		
		bunnersProgressBar.setVisibility(View.VISIBLE);
		bunners.clear();
		initBanners();
		
		
		
	}

	private void setUpBunnerPager () {
		bannerAdapter = new BannersPagerAdapter(getChildFragmentManager());
		bannerAdapter.setBunners(bunners,this);
		bannerPager.setAdapter(bannerAdapter);
		bannerPager.setScrollDurationFactor(5);
		bannerPager.setPageTransformer(true, new DepthPageTransformer());
		
		bannerPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						pagerCounter = position;
					}
				});
		if (bannerAoutoChangeThread != null){
			bannerAoutoChangeThread.cancel();
		}
		bannerAoutoChangeThread = new BannerAoutoChangeThread();
		bannerAoutoChangeThread.start();
	}
	
	
	
	@Override
	public void onClick(View v) {
		YoursItemsListFragment frag = YoursItemsListFragment.newInstance();
		switch (v.getId()) {
		case R.id.familly_btn:
			frag.setCategoryAndSubCategoryTypes(Category.CATEGORY_FAMILLY,
					0, "", false);
			break;
		case R.id.fun_btn:
			frag.setCategoryAndSubCategoryTypes(Category.CATEGORY_FUN,
					0, "", false);
			break;
		case R.id.prochase_btn:
			frag.setCategoryAndSubCategoryTypes(Category.CATEGORY_PROCHASES,
					0, "", false);
			break;
		case R.id.hoby_btn:
			frag.setCategoryAndSubCategoryTypes(Category.CATEGORY_HOBY,
					0, "", false);
			break;
		case R.id.hot_month_btn:
			frag.setCategoryAndSubCategoryTypes(Category.CATEGORY_HOT_MONTH,
					0, "", false);
			break;
		case R.id.search_btn:
			showSearchDialog();
			return;
		}
		FragmentsUtil.openFragment(getFragmentManager(), frag, R.id.content_frame);
	}
	
	private void showSearchDialog() {
		DialogManager.showSearchDialog(this, new DialogCallback(){
			
			@Override
			protected void onDialogButtonPressed(int buttonID,
					DialogFragment dialog, Object Extra) {
				super.onDialogButtonPressed(buttonID, dialog, Extra);
				String keyWord = (String)Extra;
				if (keyWord.isEmpty()){
					ToastUtil.toster(getString(R.string.no_key_word), false);
				}else if(keyWord.length() < 3){
					ToastUtil.toster(getString(R.string.no_key_word_minimum), false);
				}else{
					search(keyWord);
				}
			}
			
		});
		
	}
	
	@SuppressWarnings("unchecked")
	private void search (String keyWord){
		new SearchTask(keyWord, new SearchCallback() {
			@Override
			public void onSearchResult(ArrayList<YoursItem> result) {
				YoursItemsListFragment fragment = YoursItemsListFragment.newInstance();
				fragment.setYoursItemsArrayToDisplay(result);
				FragmentsUtil.openFragment(getFragmentManager(), fragment, R.id.content_frame);
			}
		}).execute();
	}




	class BannerAoutoChangeThread extends Thread {
		private final int DELAY_MILLIS = 5000;
		private boolean run;
		
		
		@Override
		public void run() {
			super.run();
			run = true;
			while (fragIsOn && run){
				try {
					Thread.sleep(DELAY_MILLIS);
					if (fragIsOn)
					   updateAppScreens();
				} catch (InterruptedException e) {}
				
			}
		}
		
		public void cancel(){
			run = false;
		}
		private void updateAppScreens () {
			MainActivity.mainInstance.runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					if (fragIsOn){
						if (changeCounterMethod){
							pagerCounter --;
						}else{
							pagerCounter ++;
						}
						
						if (pagerCounter == bunners.size()-1) {
							changeCounterMethod = true;
						}else if(pagerCounter == 0){
							changeCounterMethod = false; 
						}
						if (bannerPager != null){
							try {
								bannerPager.setCurrentItem(pagerCounter, true);
							} catch (Exception e) {}
						}
					}
					
				}
			});
		}
	}
	
	private void initBanners () {
		YoursApp.networkManager.getBanners(new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				try {
					android.util.Log.e("bunnerslog","response : " + response.getContent());
					JSONObject jsonObject = new JSONObject(response.getXmlContent());
					JSONArray jsonArray = new JSONArray(jsonObject.optString("GetAdvertResult"));
					for (int i = 0; i < jsonArray.length(); i++) {
						YoursItem temp = new YoursItem("פרסומות", 1, 0, new JSONObject(jsonArray.getString(i)));
						temp.setParentKeyMenu(temp.getKeyMenu());
						temp.setParentType(temp.getKeyMenu()-1);
						bunners.add(temp);
					}
					setUpBunnerPager();
					bunnersProgressBar.setVisibility(View.GONE);
				} catch (Exception e) {
					android.util.Log.e("bunnerslog","ex = " + e.getMessage());
				}
			}
		
		});
	}

	
	@Override
	public void onBunnerClick(YoursItem item, View v,int pos) {
		if (item == null){
			return;
		}
		android.util.Log.e("bannerlog", "pos = "  + bannerPager.getCurrentItem() + " item = " + bunners.get( bannerPager.getCurrentItem()));
		
		
		if(item.getStatus() == YoursItem.STATUS_EXTERNAL){
			FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
					PurchaseChooseSeatPashbarFragment.newInstance(bunners.get(bannerPager.getCurrentItem()),
							null,PurchaseChooseSeatPashbarFragment.TYPE_PASHBAR), R.id.content_frame);
			return;
		}
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(), 
				YoursItemDetailsFragment.newInstance(bunners.get(bannerPager.getCurrentItem())), R.id.content_frame);
		
	}

	


}
