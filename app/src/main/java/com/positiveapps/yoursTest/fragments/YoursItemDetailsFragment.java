/**
 * 
 */
package com.positiveapps.yoursTest.fragments;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.util.Linkify;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.fragments.purchase.PurchaseChooseAreaFragment;
import com.positiveapps.yoursTest.fragments.purchase.PurchaseChooseSeatFragment;
import com.positiveapps.yoursTest.fragments.purchase.PurchaseChooseSeatPashbarFragment;
import com.positiveapps.yoursTest.fragments.purchase.PurchaseMultyTimeFragment;
import com.positiveapps.yoursTest.fragments.purchase.PurchaseNoChooseSeatsFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.ItemDate;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.ui.CircleImageView;
import com.positiveapps.yoursTest.ui.LocationItemView;
import com.positiveapps.yoursTest.ui.LocationItemView.OnWazeItemClickListener;
import com.positiveapps.yoursTest.util.BitmapUtil;
import com.positiveapps.yoursTest.util.ContentProviderUtil;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class YoursItemDetailsFragment extends BaseFragment implements OnClickListener,OnWazeItemClickListener{
	
	public static final String 	EXTRA_YOURS_ITEM = "YoursItem";
	
	
	private ImageView itemImage;
	private Button orderBtn;
	private TextView itemName;
	private TextView itemDesc;
	private LinearLayout informationBar;
	private TextView timeOpen;
	private TextView originalPrice;
	private TextView clubPrice;
	private TextView aboutDealTxt;
	private TextView aboutItemTxtHeader;
	private TextView aboutPlaceTxtHeader;
	private TextView aboutAtractionTxt;
	private LinearLayout aboutPlaceContainer;
	private ImageView attachment;
	ArrayList<LinearLayout> boxes = new ArrayList<LinearLayout>();
	
	
	
	private YoursItem selectedItem;
//	private boolean isDataLoaded;

	public YoursItemDetailsFragment() {

	}

	public static YoursItemDetailsFragment newInstance(YoursItem yoursItem) {
		YoursItemDetailsFragment instance = new YoursItemDetailsFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_YOURS_ITEM, yoursItem);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = null;
		selectedItem = (YoursItem)getArguments().getSerializable(EXTRA_YOURS_ITEM);
		if (selectedItem.getStatus() == YoursItem.STATUS_BUNNER){
			rootView = inflater.inflate(R.layout.fragment_yours_item_details_bunner, container,
					false);
		}/*else if(selectedItem.getStatus() == YoursItem.STATUS_EXTERNAL){
			rootView = inflater.inflate(R.layout.fragment_yours_item_details_external, container,
					false);
		}*/else{
			rootView = inflater.inflate(R.layout.fragment_yours_item_details, container,
					false);
		}
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		MainActivity.setScreenTitle(selectedItem.getItemName().trim());
		FrameLayout imageFrame = (FrameLayout)view.findViewById(R.id.item_image_frame);
		Log.e("externallog", "status = " + selectedItem.getStatus());
		/*if (selectedItem.getStatus() == YoursItem.STATUS_EXTERNAL){
			return;
		}*/
		if (selectedItem.getStatus() != YoursItem.STATUS_BUNNER){
			try {
				imageFrame.setBackgroundResource(Category.circle_frame[selectedItem.getParentType()]);
			} catch (Exception e) {}
			
		}
		
		if (selectedItem.getStatus() == YoursItem.STATUS_BUNNER){
			itemImage  = (ImageView)view.findViewById(R.id.item_image);;
		}else{
			itemImage  = (CircleImageView)view.findViewById(R.id.item_image);;
		}
		
		
		orderBtn  = (Button)view.findViewById(R.id.order_btn);;
		itemName  = (TextView)view.findViewById(R.id.item_name);;
		itemDesc  = (TextView)view.findViewById(R.id.item_desc);;
		informationBar  = (LinearLayout)view.findViewById(R.id.information_bar);;
		timeOpen  = (TextView)view.findViewById(R.id.time_open_txt);;
		originalPrice  = (TextView)view.findViewById(R.id.original_price_txt);;
		clubPrice  = (TextView)view.findViewById(R.id.club_price_txt);;
		aboutDealTxt  = (TextView)view.findViewById(R.id.about_deal_txt);;
		aboutAtractionTxt  = (TextView)view.findViewById(R.id.about_atraction_txt);;
		aboutItemTxtHeader  = (TextView)view.findViewById(R.id.txt_header);;
		aboutPlaceTxtHeader  = (TextView)view.findViewById(R.id.txt_place_header);;
		aboutPlaceContainer  = (LinearLayout)view.findViewById(R.id.location_item_container);
		
		
		attachment = (ImageView)view.findViewById(R.id.attacment);
		boxes.add((LinearLayout)view.findViewById(R.id.about_deal_box));
		boxes.add((LinearLayout)view.findViewById(R.id.about_atraction_box));
		boxes.add((LinearLayout)view.findViewById(R.id.about_place_box));
		
		orderBtn.setOnClickListener(this);
		
		fillScreenData();
		performGetItemDetails ();	
		
	}

	
	
	private void performGetItemDetails() {
		showProgressDialog();
		YoursApp.networkManager.getItemDetails(selectedItem.getKeyMenu()+"",
				selectedItem.getSubMenuId()+"", selectedItem.getSubjectId()+"", new NetworkCallback(){
			
		
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
		
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				Log.e("itemdetailslog", "result = " + response.getXmlContent());
				//isDataLoaded = true;
				try {
					selectedItem.addFullDeatails(new JSONObject(response.getXmlContent()));
					ArrayList<ItemDate> dats = selectedItem.getDates();
					HashMap<String, ItemDate> fillter = new HashMap<String,ItemDate>();
					
					aboutPlaceContainer.removeAllViews();
					for (int i = 0; i < dats.size(); i++) {
						ItemDate temp = selectedItem.getDates().get(i);
						Log.e("cityadrreslog", "place - " + temp.getCity() + " city " +temp.getPlace());
						
						if (temp.getCity() == null){
							temp.setCity(temp.getPlace());
						}else{
							if (temp.getCity().trim().isEmpty()){
								temp.setCity(temp.getPlace());
							}
						}
						
						if (temp.getCity() == null){
							temp.setCity(temp.getAddress());
						}else{
							if (temp.getCity().trim().isEmpty()){
								temp.setCity(temp.getAddress());
							}
						}
						
						
						if (temp.getCity() == null){
							continue;
						}else{
							if (temp.getCity().trim().isEmpty()){
								continue;
							}
						}
						fillter.put(temp.getCity(), temp);
					}
					Set<String> keys = fillter.keySet();
					for (String key : keys) {
						ItemDate temp = fillter.get(key);
						LocationItemView locationItemView = new LocationItemView(temp, YoursItemDetailsFragment.this);
						aboutPlaceContainer.addView(locationItemView.getView());
					}
					
					String timeOpenContent = "";
					
					if (selectedItem.getDates().size() > 1){
						timeOpenContent = YoursApp.appContext.getString(R.string.multy_times);
					}else{
						if (selectedItem.getDates() != null && selectedItem.getDates().size() > 0){
							ItemDate temp = selectedItem.getDates().get(0);
							if (temp != null){
								timeOpenContent = temp.getDate() + "\n" + temp.getTime();
							}
						}
					}
					timeOpen.setText(timeOpenContent);
					if (fragIsOn) {
						try {
							fillScreenData();
						} catch (Exception e) {
						}
					}
					
					
				} catch (JSONException e) {
					Log.e("parserLog", "exp = " + e.getMessage());
				}
			}
			
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
				dismisProgressDialog();
				//isDataLoaded = true;
			}
			
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
				//isDataLoaded = true;
			}
			
		});
		
	}

	/**
	 * 
	 */
	@SuppressWarnings("deprecation")
	private void fillScreenData() {
		
		if (selectedItem.getStatus() != YoursItem.STATUS_BUNNER){
			BitmapUtil.loadImageIntoByUrl(selectedItem.getImageUrl(), itemImage, R.drawable.ic_action_picture,
					R.drawable.ic_action_picture, 100, 100, null);
		}else{
			BitmapUtil.loadImageIntoByUrl(selectedItem.getBannerImage(), itemImage, R.drawable.ic_action_picture,
					R.drawable.ic_action_picture, YoursApp.generalSettings.getScreenWidth(), YoursApp.generalSettings.getScreenHight()/3, null);
			itemImage.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					ContentProviderUtil.playMediaByUrl(YoursItemDetailsFragment.this, "image/*", selectedItem.getBannerImage());
					
				}
			});
		}
		
		
		if (selectedItem.getStatus() != YoursItem.STATUS_BUNNER) {
			aboutItemTxtHeader
					.setBackgroundDrawable(Category.CATEGORIES_DETAILS_BOX
							.getDrawable(selectedItem.getParentType()));
			aboutPlaceTxtHeader
					.setBackgroundDrawable(Category.CATEGORIES_DETAILS_BOX
							.getDrawable(selectedItem.getParentType()));
			orderBtn.setBackgroundDrawable(Category.CATEGORIES_ORDER_BUTTONS.getDrawable(selectedItem.getParentType()));
			informationBar.setBackgroundDrawable(Category.CATEGORIES_INFO_BARS.getDrawable(selectedItem.getParentType()));
		}
		
		
		
		itemName.setText(selectedItem.getItemName()); 
		
		if (selectedItem.getStatus() == YoursItem.STATUS_BUNNER){
			if (selectedItem.getItemOriginalPrice() == 0){
				itemDesc.setText(getString(R.string.coupone_text));
				itemDesc.setGravity(Gravity.CENTER);
				orderBtn.setVisibility(View.GONE);
			}else{
				orderBtn.setBackgroundDrawable(Category.CATEGORIES_ORDER_BUTTONS.getDrawable(selectedItem.getParentType()));
				Log.e("pricelog", selectedItem.getItemClubPrice()+"");
				itemDesc.setText(getString(R.string.club_price) + " " + selectedItem.getItemClubPrice()+" ש״ח");
				orderBtn.setVisibility(View.VISIBLE);
			}
		}else{
			itemDesc.setText(selectedItem.getLongDesc().replace(".<BR /><br />","\n"));
			Linkify.addLinks(itemDesc, Linkify.ALL);
		}
		
		
		//if (selectedItem.getSubCategoryName().contains("צימרים")||selectedItem.getSubCategoryName().contains("כפר נופש")){
		//	aboutAtractionTxt.setText(selectedItem.getItemDesc() + "\n" + 
		//			"האירוח מקנה אירוח ל2 לילות בסופ״שׁ");
		//}else{
		aboutAtractionTxt.setText(selectedItem.getItemDesc());
		
			
		
		String originalPriceTxt = String.valueOf((int)selectedItem.getItemOriginalPrice());
		String clubPriceTxt = String.valueOf((int)selectedItem.getItemClubPrice());
		if (selectedItem.getItemOriginalPrice() <= 0){
			originalPriceTxt = "--";
		}
		if(selectedItem.getItemClubPrice() <= 0){
			clubPriceTxt = "--";
		}
		originalPrice.setText(originalPriceTxt);
		clubPrice.setText(clubPriceTxt);
	}

	
	@Override
	public void onClick(View v) {
		Log.e("itemStatuslog", "status = " + selectedItem.getStatus());
		switch (v.getId()) {
		 
			case R.id.order_btn:
				if (selectedItem.getStatus() ==  YoursItem.STATUS_PASHBAR){
					FragmentsUtil.openFragment(getFragmentManager(),
							PurchaseMultyTimeFragment.newInstance(selectedItem), R.id.content_frame);
					return;
				}
				if (selectedItem.getStatus() == YoursItem.STATUS_EXTERNAL){
					
					FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
							PurchaseChooseSeatPashbarFragment.newInstance(selectedItem,
									selectedItem.getDates().get(0),
									PurchaseChooseSeatPashbarFragment.TYPE_PASHBAR), R.id.content_frame);
					return;
				}
				if (selectedItem.getStatus() == YoursItem.STATUS_SINGEL){
					
					if (selectedItem.isChoosable()){
						
						int areaCount = selectedItem.getAreasByPlaceAndDate(selectedItem.getDates().get(0).getPlace(),
								selectedItem.getDates().get(0).getDate()+selectedItem.getDates().get(0).getTime()).size();
						if (areaCount > 1){
							FragmentsUtil.openFragment(getFragmentManager(),
									PurchaseChooseAreaFragment.newInstance(selectedItem,selectedItem.getDates().get(0)), R.id.content_frame);
						}else{
							FragmentsUtil.openFragment(getFragmentManager(),
									PurchaseChooseSeatFragment.newInstance(selectedItem,
											selectedItem.getAreasByPlaceAndDate(selectedItem.getDates().get(0).getPlace(),
													selectedItem.getDates().get(0).getDate()+selectedItem.getDates().get(0).getTime()).get(0)
											,selectedItem.getDates().get(0)), R.id.content_frame);
						}
					}else{
						FragmentsUtil.openFragment(getFragmentManager(),
								PurchaseNoChooseSeatsFragment.newInstance(selectedItem,null), R.id.content_frame);
					}
					
				}else if(selectedItem.getStatus() == YoursItem.STATUS_MOLTIPLE){
					FragmentsUtil.openFragment(getFragmentManager(),
							PurchaseMultyTimeFragment.newInstance(selectedItem), R.id.content_frame);
				}else{
					FragmentsUtil.openFragment(getFragmentManager(),
							PurchaseNoChooseSeatsFragment.newInstance(selectedItem,null), R.id.content_frame);
				}
				
				break;
		}
	}

	
	@Override
	public void onWazeClickListener(ItemDate data, View v) {
		
		String fullAddress = data.getFullAddress();
		if (fullAddress != null  && !fullAddress.isEmpty()){
			ContentProviderUtil.openWaze(MainActivity.mainInstance,fullAddress);
		}else{
			ToastUtil.toster(getString(R.string.no_address_message), false);
		}
		
	}
	
	

	


}
