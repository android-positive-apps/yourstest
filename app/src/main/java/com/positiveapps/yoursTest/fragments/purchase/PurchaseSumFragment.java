/**
 * 
 */
package com.positiveapps.yoursTest.fragments.purchase;



import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Currency;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.PurchaseEvent;
import com.positiveapps.pelecardsdk.PelecardContainerActivity;
import com.positiveapps.pelecardsdk.fragments.PaymentFragment;
import com.positiveapps.pelecardsdk.object.PelecardConfiguration;
import com.positiveapps.pelecardsdk.object.PelecardDeal;
import com.positiveapps.pelecardsdk.object.PelecardResult;
import com.positiveapps.pelecardsdk.util.ToastUtil;
import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.SumTestActivity;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.dialogs.AppDialogButton;
import com.positiveapps.yoursTest.dialogs.DialogCallback;
import com.positiveapps.yoursTest.dialogs.DialogManager;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.fragments.MainMenuFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.SamGeneralItem;
import com.positiveapps.yoursTest.objects.SamObject;
import com.positiveapps.yoursTest.objects.SamTicket;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.ui.SamGeneralItemView;
import com.positiveapps.yoursTest.ui.SamTicketView;
import com.positiveapps.yoursTest.ui.YoursItemView;
import com.positiveapps.yoursTest.util.BackStackUtil;
import com.positiveapps.yoursTest.util.BitmapUtil;


/**
 * @author natiapplications
 *
 */
public class PurchaseSumFragment extends BaseFragment implements OnClickListener{
	
	
	public static final int MESSAGE_PAY_DONE = 1;
	public static final int MESSAGE_PAY_TIME_OUT = 2;
	
	private final long PAY_TIME_OUT_MILLIS = 300000;
	
	public static final String 	EXTRA_YOURS_ITEM = "YoursItem";
	public static final String 	EXTRA_SAM_TYPE = "SamType";
	public static final String 	EXTRA_SAM_OBJECT = "SamObject";
	public static final String EXTRA_TERMINAL = "Terminal";
	public static final String EXTRA_PELECARD_USER_NAME = "plecardUserName";
	public static final String EXTRA_PELECARD_PASS = "PelecardPass";
	private static final int REQUEST_PELECARD_ACTIVITY = 9000;
	
	private LinearLayout itemFrame;
	private LinearLayout samViewContainer;
	private TextView samPriceTxt;
	private Button continueBtn;
	
	private YoursItem selectedItem;
	private int samType;
	private SamObject samObject;
	private double samPrice;
	
	
	private boolean setOrderSent;
	private boolean setOrderBack;
	
	private static final int RESULT_TYPE_SUCCESS = 10;
	private static final int RESULT_TYPE_FAILE = 11;
	private static final int RESULT_TYPE_ERROR_1 = 12;
	private static final int RESULT_TYPE_ERROR_2 = 13;
	private static final int RESULT_TYPE_ERROR_3 = 14;
	private static final int RESULT_TYPE_TIME_OUT = 15;
	
	private int resultType;
	private int termianl;
	private String pelecardUserName = "";
	private String pelecardPass = "";
	
	@SuppressLint("HandlerLeak")
	private Handler paymentHandler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case MESSAGE_PAY_DONE:
				int result = msg.arg1;
				if (result == PaymentFragment.PAY_SUCCESS){
					PelecardResult pelecardResult = (PelecardResult) msg.obj;
					if (pelecardResult != null){
//						setOrder(pelecardResult);
					}

				}else if (result == PaymentFragment.PAY_FAILE){
					resultType = RESULT_TYPE_FAILE;
					PelecardResult pelecardResult = (PelecardResult) msg.obj;
					YoursApp.networkManager.payFaile(pelecardResult.getErrorCode(),
							pelecardResult.getDeal().getOrderNumber(), null);
//					crashlytiesTransReport(pelecardResult,false);
				}
				break;
			case MESSAGE_PAY_TIME_OUT:
				resultType = RESULT_TYPE_TIME_OUT;
				break;
			}
		};
	};


	private void crashlytiesTransReport(PelecardResult result,boolean success){
		try{
			Answers.getInstance().logPurchase(new PurchaseEvent()
					.putItemPrice(BigDecimal.valueOf(result.getDeal().getProductCost()))
					.putCurrency(Currency.getInstance("ILS"))
					.putItemName(result.getDeal().getProductName())
					.putItemType(result.getDeal().getBusinessName())
					.putItemId(result.getDeal().getOrderNumber())
					.putSuccess(success));
		}catch (Exception e){

		}
	}

	public PurchaseSumFragment() {

	}

	public static PurchaseSumFragment newInstance(int samType,SamObject samObject,int terminal,
			String pelecardUserName,String pelecardPass) {
		PurchaseSumFragment instance = new PurchaseSumFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(EXTRA_TERMINAL, terminal);
		bundle.putInt(EXTRA_SAM_TYPE, samType);
		bundle.putSerializable(EXTRA_SAM_OBJECT, samObject);
		bundle.putString(EXTRA_PELECARD_USER_NAME, pelecardUserName);
		bundle.putString(EXTRA_PELECARD_PASS, pelecardPass);
		instance.setArguments(bundle);
		return instance;
	}

	public void setYoursItem (YoursItem item){
		this.selectedItem = item;
	}
	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_prochase_sam, container,
				false);
		//selectedItem = (YoursItem)getArguments().getSerializable(EXTRA_YOURS_ITEM);
		samType = getArguments().getInt(EXTRA_SAM_TYPE);
		samObject = (SamObject) getArguments().getSerializable(EXTRA_SAM_OBJECT);
		termianl = getArguments().getInt(EXTRA_TERMINAL);
		pelecardUserName = getArguments().getString(EXTRA_PELECARD_USER_NAME);
		pelecardPass = getArguments().getString(EXTRA_PELECARD_PASS);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.setScreenTitle(getString(R.string.sam_order_title));
		itemFrame = (LinearLayout)view.findViewById(R.id.item_frame);
		samViewContainer = (LinearLayout)view.findViewById(R.id.sma_view_container);
		continueBtn = (Button)view.findViewById(R.id.continue_btn);
		samPriceTxt = (TextView)view.findViewById(R.id.sam_price_txt);
		continueBtn.setBackgroundDrawable(Category.CATEGORIES_CONTINUE_BUTTONS.getDrawable(selectedItem.getParentType()));
		continueBtn.setOnClickListener(this);
		addItemFrameView ();
		addSamView();
		startTimeOut();
	}

	
	private void addSamView() {
		View view = null;
		switch (samType) {
		case SamObject.TYPE_TICKET:
			SamTicket samTicket = (SamTicket)samObject;
			SamTicketView samTicketView = new SamTicketView(samTicket);
			view = samTicketView.getView();
			samPrice = samTicket.getSamPrice();
			break;
		case SamObject.TUPE_GENERAL_ITEM:
			SamGeneralItem saGeneralItem = (SamGeneralItem)samObject;
			SamGeneralItemView samGeneralItemView = new SamGeneralItemView(saGeneralItem);
			view = samGeneralItemView.getView();
			samPrice = saGeneralItem.getSamPrice();
			break;
		}
		/*LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 400);
		params.gravity = Gravity.CENTER;
		view.setLayoutParams(params);*/
		samViewContainer.addView(view);
		
		String samPriceString  = new DecimalFormat("##.##").format(samPrice);
		samPriceTxt.setText(samPriceTxt.getText().toString() + " " + samPriceString + " ש״ח");
	}

	
	private void addItemFrameView() {
		if (selectedItem.getStatus() == YoursItem.STATUS_BUNNER){
			View view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.banner_item_layout, null);
			ImageView imageView = (ImageView)view.findViewById(R.id.item_image);
			itemFrame.addView(view);
			BitmapUtil.loadImageIntoByUrl(selectedItem.getBannerImage(), imageView, R.drawable.ic_action_picture,
					R.drawable.ic_action_picture, YoursApp.generalSettings.getScreenWidth(), YoursApp.generalSettings.getScreenHight()/3, null);
			return;
		}
		YoursItemView yoursItemView = new YoursItemView(selectedItem, null);
		itemFrame.addView(yoursItemView.getItemView());
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.continue_btn:
				openPelecardSdk();
				break;
		}
	}
	
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		stop = true;
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == REQUEST_PELECARD_ACTIVITY) {
			Log.e("pelecardActivityResultLog", "currentResultType = " + resultType);
			if (setOrderSent) {
				if (setOrderBack) {
					checkResultType();
				} else {
					showProgressDialog();
					waitToSentOrderResult();
				}
			} else {
				checkResultType();
			}

			if (data.getStringExtra("test") != null){
				if (data.getStringExtra("test").equals("test")) {
					String cardNumber= data.getStringExtra("cardNumber");
					String cardValidity=data.getStringExtra("cardValidity");
					String cvv=data.getStringExtra("cvv");
					String productCost= data.getStringExtra("productCost");
					String businessName=data.getStringExtra("businessName");
					String userId=data.getStringExtra("userId");
					String userEmail=data.getStringExtra("userEmail");


					//todo open test function
					YoursApp.networkManager.setTestOrder(selectedItem.getActionNumber()+"","",productCost,"",userId,"",userEmail,"",
							"",selectedItem.getSubMenuId()+"","",cardValidity,cardNumber,cvv,"","","","",new NetworkCallback(){
								@Override
								public void onDataRecived(ResponseObject response, boolean isHasError, String erroDescription) {
									super.onDataRecived(response, isHasError, erroDescription);
									if (!isHasError){
										if (response.getXmlContent().equals("{\"RegisterPaymentResult\":\"\\\"error:0\\\"\"}")) {
											ToastUtil.toster(getActivity(), getString(com.positiveapps.pelecardsdk.R.string.thank_message), true);
											System.out.println("test= " + response.getXmlContent());
										}



									}else {
										ToastUtil.toster(getActivity(), getString(com.positiveapps.pelecardsdk.R.string.general_pelecard_error), true);
									}
								}

								@Override
								public void onError() {
									super.onError();
									ToastUtil.toster(getActivity(), getString(com.positiveapps.pelecardsdk.R.string.general_pelecard_error), true);


								}

							});

					Intent i = new Intent(getActivity(), SumTestActivity.class);
					startActivity(i);
				}
		}
			
		}
	}
	
	private void waitToSentOrderResult(){
		new Handler().postDelayed(new Runnable() {
			
			@Override
			public void run() {
				if (setOrderBack){
					dismisProgressDialog();
					checkResultType();
				}else{
					waitToSentOrderResult();
				}
			}
		}, 1000);
	}
	
	private void checkResultType(){
		switch (resultType) {
		case RESULT_TYPE_SUCCESS:
			showSuccessDialog();
			break;
		case RESULT_TYPE_FAILE:
			showErrorDialog("לצערנו אירעה שגיאה בעת ביצוע החיוב. כרטיסך לא חויב");
			break;
		case RESULT_TYPE_ERROR_1:
			showErrorDialog("לצערנו אירעה שגיאה בעת השלמת תהליך הרכישה. כרטיסך יזוכה במידה והתבצע חיוב");
			break;
		case RESULT_TYPE_ERROR_2:
			showErrorDialog("לצערנו כרטיסך נחסם אנא פנה למחלקת משאבי אנוש");
			break;
		case RESULT_TYPE_ERROR_3:
			showWorrningDialog();
			break;
		case RESULT_TYPE_TIME_OUT:
			showErrorDialog("מועד השלמת הפעולה תם. אנא התחל שוב");
			break;
		}
	}
	
	
	private void showErrorDialog(String message){
		try {
			DialogManager.showErrorDialog(PurchaseSumFragment.this,message, 
					new DialogCallback(){
				@Override
				protected void onDialogButtonPressed(int buttonID,
						DialogFragment dialog) {
					super.onDialogButtonPressed(buttonID, dialog);
					represhApplication();
				}
			});
		} catch (Exception e) {} catch (Error e){}
	}
	
	private void showWorrningDialog (){
		try {
			AppDialogButton[]  buttons = {new AppDialogButton(0, R.drawable.blue_dialog_btn_selector,"אישור")}; 
			
			DialogManager.showAppDialog(PurchaseSumFragment.this,
					"הודעה", "חבר יקר, הזמנת החודש בכמות ההזמנות המותרת על פי התקנון. תוכל לשוב ולהזמין בחודש הבא.",
					R.drawable.circle_blue, R.drawable.ic_action_about, buttons, true, new DialogCallback(){
				@Override
				protected void onDialogButtonPressed(int buttonID,
						DialogFragment dialog) {
					super.onDialogButtonPressed(buttonID, dialog);
					represhApplication();
				}
			});	
		} catch (Exception e) {}catch (Error e) {}
	}
	
	private void showSuccessDialog (){
		try {
			AppDialogButton[]  buttons = {new AppDialogButton(0, R.drawable.green_dialog_btn_selector,"אישור")}; 
			
			DialogManager.showAppDialog(PurchaseSumFragment.this,
					"ברכותינו", "העסקה בוצעה בהצלחה!"+"\n"+"פרטי העיסקה זמינים בהיסטוריית הרכישות - במידה ורכשת קופון, תוכל למצוא שם את מספר הקופון"
			+"\n"+"תודה רבה !",
					R.drawable.circle_green, R.drawable.ic_action_accept, buttons, true, new DialogCallback(){
				@Override
				protected void onDialogButtonPressed(int buttonID,
						DialogFragment dialog) {
					super.onDialogButtonPressed(buttonID, dialog);
					represhApplication();
				}
			});	
		} catch (Exception e) {}catch (Error e) {}
	}
	
	private void setOrder(final PelecardResult result){
		//showProgressDialog();
		
		int count = 0;
		if (samType == SamObject.TUPE_GENERAL_ITEM){
			SamGeneralItem samGeneralItem = (SamGeneralItem)samObject;
			count = samGeneralItem.getItems().size();
		}else if (samType == SamObject.TYPE_TICKET){
			SamTicket samTicket = (SamTicket)samObject;
			count = samTicket.getTickets().size();
		}
		setOrderSent = true;
		YoursApp.networkManager.setOrder(selectedItem.getActionNumber()+"",result, new NetworkCallback(){
			 
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				//dismisProgressDialog();
				
				YoursApp.networkManager.registerOrderInPushServer("", result, null);
				try {
					setOrderBack = true;
					JSONObject jsonObject = new JSONObject(response.getXmlContent());
					String error = jsonObject.optString("RegisterPaymentResult","");
					if (error.contains("error:1")){
						resultType = RESULT_TYPE_ERROR_3;
					}else if (error.contains("error:2")){
						resultType = RESULT_TYPE_ERROR_2;
					}else if (error.contains("error:3")){
						resultType = RESULT_TYPE_ERROR_3;
					}else{
						resultType = RESULT_TYPE_SUCCESS;
					}

//					crashlytiesTransReport(result,resultType == RESULT_TYPE_SUCCESS ? true:false);
				} catch (Exception e) {
					e.printStackTrace(); 
				}
			
			}
			
			@Override
			public void onError() {
				super.onError();
				setOrderBack = true;
			}
		
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				setOrderBack = true;
			}
		});
	}
	
	
	private void openPelecardSdk () {
		stop = true;
		long payTimeOutMillis = PAY_TIME_OUT_MILLIS - (counter*1000);
		PelecardConfiguration pelecardConfiguration = new PelecardConfiguration();
		
		if (this.termianl <= 0){
			this.termianl = 5782680;
		}
		if (this.pelecardUserName == null){
			this.pelecardUserName = "dolcevitasite";
		}
		if(this.pelecardPass ==  null){
			this.pelecardPass = "W4iLmsiA";
		}
		if (this.pelecardUserName.isEmpty()){
			this.pelecardUserName = "dolcevitasite";
		}
		if(this.pelecardPass.isEmpty()){
			this.pelecardPass = "W4iLmsiA";
		}
		pelecardConfiguration.setTerminalNumber(String.valueOf(this.termianl));
		pelecardConfiguration.setUserName(this.pelecardUserName);
		pelecardConfiguration.setPassword(this.pelecardPass);
		pelecardConfiguration.setPayTimeout(payTimeOutMillis);
		
		PelecardDeal pelecardDeal = new PelecardDeal(selectedItem.getItemName(), selectedItem.getImageUrl(),
				selectedItem.getItemName(), (float)samPrice);
		
		pelecardDeal.setSubMenuId(selectedItem.getSubMenuId());
		pelecardDeal.setSessionId(selectedItem.getSessionID());
		pelecardDeal.setOrderNumber(selectedItem.getActionNumber()+"");
		Intent intent = new Intent(getActivity(),PelecardContainerActivity.class);
		//PelecardContainerActivity.pelecardConfiguration = pelecardConfiguration;
		//PelecardContainerActivity.pelecardDeal = pelecardDeal;
		Bundle bundle = new Bundle();
		bundle.putSerializable(PelecardContainerActivity.EXTRA_CONFIGURATION, pelecardConfiguration);
		bundle.putSerializable(PelecardContainerActivity.EXTRA_DEAL_BUSINESS, pelecardDeal);
		intent.putExtras(bundle);
		PelecardContainerActivity.setPayHandler(paymentHandler);
		startActivityForResult(intent, REQUEST_PELECARD_ACTIVITY);
	}
	

	int counter;
	boolean stop;
	private void startTimeOut () {
		counter = 0;
		stop = false;
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(!stop){
					counter++;
					if (counter*1000>=PAY_TIME_OUT_MILLIS){
						try {
							getActivity().runOnUiThread(new Runnable() {
								@Override
								public void run() {
									if (!stop){
										stop = true;
										showErrorDialog("מועד השלמת הפעולה תם. אנא התחל שוב");
									}
								}
							});
						} catch (Exception e) {
							// TODO: handle exception
						} catch (Error e) {
							// TODO: handle exception
						}
						
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
		
	}
	
	private void represhApplication () {
		try {
			getFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
			FragmentTransaction transition = getFragmentManager()
					.beginTransaction();
			transition.replace(R.id.content_frame, MainMenuFragment.newInstance());
			transition.commitAllowingStateLoss();
		} catch (Exception e) {
			try {
				BackStackUtil.popToBackStack(MainMenuFragment.class.getSimpleName(), getFragmentManager());
			} catch (Exception e2) {}
		} catch (Error e) {
			try {
				BackStackUtil.popToBackStack(MainMenuFragment.class.getSimpleName(), getFragmentManager());
			} catch (Exception e2) {}
		}
		
	}
	


}
