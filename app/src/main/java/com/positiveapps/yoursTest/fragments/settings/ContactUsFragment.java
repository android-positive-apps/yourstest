/**
 * 
 */
package com.positiveapps.yoursTest.fragments.settings;


import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.util.TextUtil;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class ContactUsFragment extends BaseFragment implements OnClickListener{
	
	
	
	private Button sendMessageBtn;
	private EditText fullNameEt;
	private EditText mailEt;
	private EditText phoneEt;
	private EditText messageContentEt;
	
	private boolean isFillDetails;
	
	public ContactUsFragment() {

	}

	public static ContactUsFragment newInstance() {
		ContactUsFragment instance = new ContactUsFragment();
		
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings_contact_us, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		sendMessageBtn = (Button)view.findViewById(R.id.send_message_btn);
		fullNameEt = (EditText)view.findViewById(R.id.full_name_et);
		mailEt = (EditText)view.findViewById(R.id.mail_et);
		phoneEt = (EditText)view.findViewById(R.id.phone_et);
		messageContentEt = (EditText)view.findViewById(R.id.message_content_et);
		sendMessageBtn.setOnClickListener(this);
		if (!isFillDetails){
			fillUserDetails();
		}
				
	}
	
	private void fillUserDetails() {
		fullNameEt.setText(YoursApp.userProfil.getUserFullName(" "));
		mailEt.setText(YoursApp.userProfil.getUserEmail());
		phoneEt.setText(YoursApp.userProfil.getUserPhone());
		isFillDetails = true;
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.send_message_btn:
				if (TextUtil.validation(fullNameEt,mailEt,phoneEt,messageContentEt)){
					performSendMessage ();
				}else{
					ToastUtil.toster(getString(R.string.fields_empty), false);
				}
				break;
		}
	}

	/**
	 * 
	 */
	private void performSendMessage() {
		showProgressDialog();
		YoursApp.networkManager.sendContactAsMessage(fullNameEt.getText().toString(),
				mailEt.getText().toString(), phoneEt.getText().toString(), messageContentEt.getText().toString(),
				"Yours1@s-on.co.il", new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
		
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				ToastUtil.toster(getString(R.string.after_send_message_text), false);
				getActivity().finish();
			}
	
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
				dismisProgressDialog();
			}
	
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
					
			
		});
	}
	


}
