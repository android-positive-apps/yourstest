/**
 * 
 */
package com.positiveapps.yoursTest.fragments.settings;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.objects.HistoryProchase;
import com.positiveapps.yoursTest.util.DateUtil;


/**
 * @author natiapplications
 *
 */
public class HistoryDetailsFragment extends BaseFragment{
	
	public static final String 	EXTRA_HISTORY_PROCHASE = "HistoryProchase";
	
	
	
	private TextView orderNumber;
	private TextView couponNumberTxt;
	private TextView orderDate;
	private TextView confirmNumber;
	private TextView creditCardNumber;
	private TextView singlePrice;
	private TextView samProducts;
	private TextView samPrice;
	private TextView productDetails;
	private TextView buyerDetails;
	
	
	private HistoryProchase selectedHistory;
	
	
	
//	private boolean isDataLoaded;

	public HistoryDetailsFragment() {

	}

	public static HistoryDetailsFragment newInstance(HistoryProchase historyProchase) {
		HistoryDetailsFragment instance = new HistoryDetailsFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_HISTORY_PROCHASE, historyProchase);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = null;
		selectedHistory = (HistoryProchase)getArguments().getSerializable(EXTRA_HISTORY_PROCHASE);
		
		rootView = inflater.inflate(R.layout.fragment_settings_history_details, container,
					false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		orderNumber = (TextView) view.findViewById(R.id.order_number);
		couponNumberTxt = (TextView) view.findViewById(R.id.coupon_confirm_number);
		orderDate = (TextView) view.findViewById(R.id.order_date);
		confirmNumber = (TextView) view.findViewById(R.id.confirm_number);
		creditCardNumber = (TextView) view.findViewById(R.id.credit_card_number);
		singlePrice = (TextView) view.findViewById(R.id.single_price);
		samProducts = (TextView) view.findViewById(R.id.sam_product);
		samPrice = (TextView) view.findViewById(R.id.sam_price);
		productDetails = (TextView) view.findViewById(R.id.product_details);
		buyerDetails = (TextView) view.findViewById(R.id.buyer_details);
		
		
		fillScreenData();
	
	}

	
	
	
	/**
	 * 
	 */
	@SuppressWarnings("deprecation")
	private void fillScreenData() {
		
		orderNumber.setText(orderNumber.getText().toString() + " " + selectedHistory.getOrderId());
		orderDate.setText(orderDate.getText().toString() + " " + DateUtil.getServerMillisAsDateString(selectedHistory.getBuy_date()));
		confirmNumber.setText(confirmNumber.getText().toString() + " " + selectedHistory.getConfirmNumber());
		creditCardNumber.setText(creditCardNumber.getText().toString() + " " + selectedHistory.getCreditNumber());
		
		
		if (selectedHistory.getSamPrice() > 0 && selectedHistory.getSinglePrice() > 0){
			String samProduct = selectedHistory.getSamPrice()/selectedHistory.getSinglePrice()+"";
			samProducts.setText(samProduct);
			singlePrice.setText(selectedHistory.getSinglePrice()+"");
			samPrice.setText(selectedHistory.getSamPrice()+"");
		}else if (selectedHistory.getSamPrice() > 0 && selectedHistory.getSinglePrice() <=0){
			singlePrice.setText(selectedHistory.getSamPrice() + "");
			samProducts.setText("1");
			samPrice.setText(selectedHistory.getSamPrice() + "");
		}else if (selectedHistory.getSamPrice() <= 0 && selectedHistory.getSinglePrice() > 0){
			singlePrice.setText(selectedHistory.getSinglePrice() + "");
			samProducts.setText("1");
			samPrice.setText(selectedHistory.getSinglePrice() + "");
		}else{
			singlePrice.setText("0");
			samProducts.setText("1");
			samPrice.setText("0");
		}
		
		
		productDetails.setText(selectedHistory.getItemDetails());
		buyerDetails.setText(selectedHistory.getBuyerDetails());
		if (selectedHistory.getCouponNumber() != null &&! selectedHistory.getCouponNumber().isEmpty()){
			couponNumberTxt.setVisibility(View.VISIBLE);
			couponNumberTxt.setText(couponNumberTxt.getText().toString() + " " + selectedHistory.getCouponNumber());
		}else{
			couponNumberTxt.setVisibility(View.GONE);
		}
	}

	
	
	

	


}
