/**
 * 
 */
package com.positiveapps.yoursTest.fragments;



import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.util.AppUtil;
import com.positiveapps.yoursTest.util.BackStackUtil;
import com.positiveapps.yoursTest.util.DialogUtil;


/**
 * @author Nati Gabay
 *
 */
public class BaseFragment extends Fragment{
	
	protected ProgressDialog progressDialog;
	public boolean fragIsOn;
	protected String screenName;
	
	public boolean onBackPressed () {
		return false;
	}
	
	public void showProgressDialog(String message) {
		this.progressDialog = 
				DialogUtil.showProgressDialog(getActivity(), message);
	}
	
	public void showProgressDialog() {
		try {
			this.progressDialog = 
			         DialogUtil.showProgressDialog(getActivity(), 
			        		 getString(R.string.dialog_deafult_message));
		} catch (Exception e) {}
	}
	
	public void dismisProgressDialog() {
		DialogUtil.dismisDialog(this.progressDialog);
	}
	
	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		fragIsOn = true;
		MainActivity.currentFragment = this;
		BackStackUtil.addToBackStack(getClass().getSimpleName());
		AppUtil.setTextFonts(getActivity(),view);
		
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		fragIsOn = false;
		BackStackUtil.removeFromBackStack(getClass().getSimpleName());
	}

}
