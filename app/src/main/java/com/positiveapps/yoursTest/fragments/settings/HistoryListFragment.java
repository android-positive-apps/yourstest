/**
 * 
 */
package com.positiveapps.yoursTest.fragments.settings;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.adapters.HistoryListAdapter;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.HistoryProchase;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.TextUtil;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class HistoryListFragment extends BaseFragment implements OnItemClickListener{
	
	public static final int HISTORY_TYPE_PROCHASES = 1;
	public static final int HISTORY_TYPE_CANCELED = 2;
	
	public static final String EXTRA_TYPE = "HistoryType";
	
	private ListView historyList;
	private HistoryListAdapter adapter;
	private ArrayList<HistoryProchase> historyProchases = new ArrayList<HistoryProchase>();
	
	private int currentHistoryType;

	public HistoryListFragment() {

	}

	public static HistoryListFragment newInstance(int type) {
		HistoryListFragment instance = new HistoryListFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(EXTRA_TYPE, type);
		instance.setArguments(bundle);		
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_settings_prochase_history, container,
				false);
		currentHistoryType = getArguments().getInt(EXTRA_TYPE);
		return rootView;
	}

	
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		historyList = (ListView)view.findViewById(R.id.history_list);
		historyList.setOnItemClickListener(this);
		if (historyProchases.size() > 0){
			adapter = new HistoryListAdapter(historyProchases);
			historyList.setAdapter(adapter);
			return;
		}
		performGetHistory();
	}

	
	/**
	 * 
	 */
	private void performGetHistory() {
		showProgressDialog();
		YoursApp.networkManager.getHestory("key", new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				Log.e("loginlog", "result - " + response.getXmlContent());
				dismisProgressDialog();
				try {
					JSONObject jsonObject = new JSONObject(response.getXmlContent());
					JSONArray jsonArray = new JSONArray(jsonObject.optString("GetOrderHistoryResult"));
					for (int i = 0; i < jsonArray.length(); i++) {
						HistoryProchase temp = new HistoryProchase(new JSONObject(jsonArray.optString(i)));
						historyProchases.add(temp);
					}
					Collections.sort(historyProchases, new Comparator<HistoryProchase>() {

						@Override
						public int compare(HistoryProchase lhs,
								HistoryProchase rhs) {
							String parseDate = TextUtil.getNumberFromString(lhs.getBuy_date());
							if (parseDate.isEmpty()){
								parseDate = "0";
							}
							long millis = Long.parseLong(parseDate);
							Long a = millis;
							String parseDateB = TextUtil.getNumberFromString(rhs.getBuy_date());
							if (parseDateB.isEmpty()){
								parseDateB = "0";
							}
							long millisB = Long.parseLong(parseDateB);
							
							Long b = millisB;
							
							return b.compareTo(a);
						}
					});
					adapter = new HistoryListAdapter(historyProchases);
					historyList.setAdapter(adapter);
				} catch (JSONException e) {}
				
			}
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
				dismisProgressDialog();
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
		
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
				HistoryDetailsFragment.newInstance(historyProchases.get(arg2)), R.id.settings_container);
		
	}


}
