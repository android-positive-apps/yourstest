/**
 * 
 */
package com.positiveapps.yoursTest.fragments.purchase;



import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.objects.Area;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.ItemDate;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.ui.AreaView;
import com.positiveapps.yoursTest.ui.YoursItemView;
import com.positiveapps.yoursTest.ui.AreaView.OnAreaClickListener;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class PurchaseChooseAreaFragment extends BaseFragment implements OnClickListener,OnAreaClickListener{
	
	public static final String 	EXTRA_YOURS_ITEM = "YoursItem";
	public static final String 	EXTRA_DATE = "Date";
	
	
	
	
	private LinearLayout itemFrame;
	private LinearLayout areaContainer;
	private ImageView stageArea;
	
	
	
	
	private TextView selectedAreaTxt;
	private Button continueBtn;
	
	
	private Area selectedArea;
	private YoursItem selectedItem;
	private ItemDate itemDate;

	public PurchaseChooseAreaFragment() {

	}

	public static PurchaseChooseAreaFragment newInstance(YoursItem yoursItem,ItemDate date) {
		PurchaseChooseAreaFragment instance = new PurchaseChooseAreaFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_YOURS_ITEM, yoursItem);
		bundle.putSerializable(EXTRA_DATE, date);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_prochase_choose_area, container,
				false);
		selectedItem = (YoursItem)getArguments().getSerializable(EXTRA_YOURS_ITEM);
		itemDate = (ItemDate)getArguments().getSerializable(EXTRA_DATE);
		return rootView;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
	    MainActivity.setScreenTitle(getString(R.string.choose_area_title));
		itemFrame = (LinearLayout)view.findViewById(R.id.item_frame);
		areaContainer = (LinearLayout)view.findViewById(R.id.area_container);
		stageArea = (ImageView)view.findViewById(R.id.state_area);
		
		selectedAreaTxt = (TextView)view.findViewById(R.id.selected_area_txt);
		continueBtn = (Button)view.findViewById(R.id.continue_btn);
		
		continueBtn.setBackgroundDrawable(Category.CATEGORIES_CONTINUE_BUTTONS.
				getDrawable(selectedItem.getParentType()));
		
		selectedAreaTxt.setText("");
		
		continueBtn.setOnClickListener(this);
		stageArea.setOnClickListener(this);
		
		addItemFrameView ();
		addAreasIntoContainer ();
		
	}

	
	
	/**
	 * 
	 */
	private void addAreasIntoContainer() {
		
		ArrayList<Area> areas = selectedItem.getAreasByPlaceAndDate(itemDate.getPlace(),itemDate.getDate()+itemDate.getTime());
		if (areas != null){
			for (int i = 0; i < areas.size(); i++) {
				AreaView areaView = new AreaView(areas.get(i), this);
				areaContainer.addView(areaView.getView());
			} 
		}
		
	}

	private void addItemFrameView() {
		YoursItemView yoursItemView = new YoursItemView(selectedItem, null);
		itemFrame.addView(yoursItemView.getItemView());
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.continue_btn:
				openChooseSeatFragment();
				break;
		}
	}
	
	private void updateSelecteAreaTxt (String name) {
		selectedAreaTxt.setText(name);
	}
	
	private void openChooseSeatFragment () {
		if (selectedArea == null){
			ToastUtil.toster( "בחר איזור תחילה!", true);
			return;
		}
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(), 
				PurchaseChooseSeatFragment.newInstance(selectedItem, selectedArea,itemDate), R.id.content_frame);
	}

	
	@Override
	public void onAreaClick(Area item, View view) {
		selectedArea = item;
		updateSelecteAreaTxt(item.getAreaName());
	}
	

	


}
