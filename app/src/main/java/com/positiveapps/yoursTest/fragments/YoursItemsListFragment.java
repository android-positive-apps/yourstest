/**
 * 
 */
package com.positiveapps.yoursTest.fragments;

import java.util.ArrayList;

import org.json.JSONObject;


import android.annotation.SuppressLint;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.fragments.purchase.PurchaseChooseSeatPashbarFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.SubCategory;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.ui.YoursItemView;
import com.positiveapps.yoursTest.ui.YoursItemView.OnYoursItemViewClickListener;
import com.positiveapps.yoursTest.ui.YoursItemsListHeaderView;
import com.positiveapps.yoursTest.ui.YoursItemsListHeaderView.onShowMoreClickListenr;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;

/**
 * @author natiapplications
 *
 */
public class YoursItemsListFragment extends BaseFragment implements OnClickListener,OnYoursItemViewClickListener
,onShowMoreClickListenr{
	
	
	private ScrollView itemsScroll;
	private LinearLayout itemsContainer;
	private TextView waitTextView;
	
	private int currentCategoryType;
	private int currentSubCategoryType;
	private String currentSubCategoryName;
	private ArrayList<YoursItem> yoursItems;
	
	private boolean fillter;
	private boolean refreshByArray;
	private boolean isFilltered;

	public YoursItemsListFragment() {

	}

	public static YoursItemsListFragment newInstance() {
		YoursItemsListFragment instance = new YoursItemsListFragment();
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_yours_items_list, container,
				false);
		
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		
		itemsScroll = (ScrollView) view.findViewById(R.id.items_scroll);
		itemsContainer = (LinearLayout) view.findViewById(R.id.items_container);
		waitTextView = showPleasWaitTxt();
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				if (refreshByArray){
					MainActivity.setScreenTitle(getString(R.string.search));
					refreshListByArray();
					return;
				}
				if (fillter){
					
					refreshListByFillter();
				}else{
					MainActivity.setScreenTitle(Category.CATEGORIES_NAMES[currentCategoryType]);
					refreshListShowAll();
				}
			}
		}, 500);
		
		
	}
	

	
	
	public void setCategoryAndSubCategoryTypes(int categoryType, int subCategoryType,String subCategoryName ,boolean fillter){
		this.fillter = fillter;
		this.currentCategoryType = categoryType;
		this.currentSubCategoryType = subCategoryType;
		this.currentSubCategoryName = subCategoryName;
		refreshByArray = false;
	}
	public void setYoursItemsArrayToDisplay (ArrayList<YoursItem> toDisplay){
		refreshByArray = true;
		this.yoursItems = toDisplay;
	}
	
	public void refreshListShowAll (){
		waitTextView = showPleasWaitTxt();
		boolean isHasItems = false;
		ArrayList<Category> categories = YoursApp.categoriesArray;
		for (int i = 0; i < categories.size(); i++) {
			if (categories.get(i).getCategoryType() == currentCategoryType){
				ArrayList<SubCategory> subCategories = categories.get(i).getSubCategories();
				for (int j = 0; j < subCategories.size(); j++) {
					boolean showArraw = true;
					if (subCategories.get(j).getParentType() == Category.CATEGORY_HOT_MONTH/*||
							subCategories.get(j).getParentType() == Category.CATEGORY_PROCHASES*/){
						showArraw = false;
					}
					if (subCategories.get(j).getItems().size() <= 2){
						showArraw = false;
					}
					Log.e("showArrawlog", "show  ? = " + showArraw +" type = "+ subCategories.get(j).getParentType());
					YoursItemsListHeaderView yoursItemsListHeaderView = new YoursItemsListHeaderView(subCategories.get(j), showArraw,this);
					itemsContainer.addView(yoursItemsListHeaderView.getItemView());
					int itemIter = subCategories.get(j).getItems().size();
					if (itemIter > 2&&subCategories.get(j).getParentType() != Category.CATEGORY_HOT_MONTH/*&&
							subCategories.get(j).getParentType() != Category.CATEGORY_PROCHASES*/){
						itemIter = 2;
					}
					for (int k = 0; k <itemIter; k++) {
						YoursItemView yoursItemView = new YoursItemView(subCategories.get(j).getItems().get(k), this);
						if (fragIsOn){
							itemsContainer.addView(yoursItemView.getItemView());
							isHasItems = true;
						}
						     
					}
					if (isHasItems){
						waitTextView.setVisibility(View.GONE);
					}
					
				}
			}
		}
		if (!isHasItems){
			if (fragIsOn)
			waitTextView.setText(getString(R.string.no_result));
		}
	}
	
	public void refreshListByFillter () {
		waitTextView = showPleasWaitTxt();
		boolean isHasItems = false;
		ArrayList<Category> categories = YoursApp.categoriesArray;
		for (int i = 0; i < categories.size(); i++) {
			if (categories.get(i).getCategoryType() == currentCategoryType){
				ArrayList<SubCategory> subCategories = categories.get(i).getSubCategories();
				for (int j = 0; j < subCategories.size(); j++) {
					if (subCategories.get(j).getSubCategoryMenuId() == currentSubCategoryType&&
							currentSubCategoryName.equalsIgnoreCase(subCategories.get(j).getSubCategoryName())){
						MainActivity.setScreenTitle(subCategories.get(j).getSubCategoryName().trim());
						YoursItemsListHeaderView yoursItemsListHeaderView = new YoursItemsListHeaderView(subCategories.get(j), false,null);
						itemsContainer.addView(yoursItemsListHeaderView.getItemView());
						int itemIter = subCategories.get(j).getItems().size();
						
						/*for (int k = 0; k <itemIter; k++) {
							YoursItemView yoursItemView = new YoursItemView(subCategories.get(j).getItems().get(k), this);
							if (fragIsOn){
								isHasItems = true;
								itemsContainer.addView(yoursItemView.getItemView());
							}
						}*/
						
						new DrawItemRunnable(subCategories.get(j).getItems()).run();
						/*if (isHasItems){
							waitTextView.setVisibility(View.GONE);
						}*/
					}
				}
			}
		}
		if (!isHasItems){
			if (fragIsOn)
			waitTextView.setText(getString(R.string.no_result));
		}
	}
	
	private class DrawItemRunnable implements Runnable {
		private ArrayList<YoursItem> toDraw;
		private int maxItems;
		private int counter;
		public DrawItemRunnable (ArrayList<YoursItem> toDraw){
			this.toDraw = toDraw;
			this.maxItems = toDraw.size();
			this.counter = 0;
		}
		@Override
		public void run() {
			YoursItemView yoursItemView = new YoursItemView(toDraw.get(counter), YoursItemsListFragment.this);
			if (fragIsOn){
				itemsContainer.addView(yoursItemView.getItemView());
				TranslateAnimation ta = new TranslateAnimation(-YoursApp.generalSettings.getScreenWidth(), 0, 0, 0);
				ta.setDuration(50);
				yoursItemView.getItemView().startAnimation(ta);
				waitTextView.setVisibility(View.GONE);
				
				counter++;
				if (counter < maxItems){
					new Handler().postDelayed(this, 10);
				}
			}
		}
		
	}
	
	public void refreshListByArray() {
		waitTextView = showPleasWaitTxt();
		if (yoursItems == null||yoursItems.size() == 0){
			waitTextView.setText(getString(R.string.no_result));
			return;
		}
		boolean isAdd = false;
		for (int i = 0; i < yoursItems.size(); i++) {
			YoursItemView yoursItemView = new YoursItemView(yoursItems.get(i), YoursItemsListFragment.this);
			if (fragIsOn){
				isAdd = true;
				View view = yoursItemView.getItemView();
				itemsContainer.addView(view);
				//animateItems(view, yoursItems.get(i), i);
			}
		}
		if (isAdd){
			Log.e("isaadlog", "isAdd ! " + yoursItems.size());
			if (fragIsOn)
			waitTextView.setVisibility(View.GONE);
		}
		
	}
	
	private void animateItems (View v,final YoursItem temp,int position){
		if (!temp.isAnimated()){
			Animation animation = AnimationUtils.loadAnimation
					(getActivity(), R.anim.pop_right_in);
			int duration = position*100;
			duration -= position*80;
			if (duration > 1000){
				duration = position*10;
			}
			animation.setDuration(duration);
	        v.startAnimation(animation);
	        animation.setAnimationListener(new Animation.AnimationListener() {
				@Override
				public void onAnimationStart(Animation animation) {}
				@Override
				public void onAnimationRepeat(Animation animation) {}
				@Override
				public void onAnimationEnd(Animation animation) {
					temp.setAnimated(true);
				}
			});
		}
	}
	
	
	
	private TextView showPleasWaitTxt (){
		itemsContainer.removeAllViews();
		TextView textView = new TextView(MainActivity.mainInstance);
		textView.setTextSize(MainActivity.mainInstance.getResources().getDimension(R.dimen.regular_small));
		textView.setTextColor(Color.RED);
		textView.setText(MainActivity.mainInstance.getString(R.string.deafult_dialog_messgae));
		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT);
		params.gravity = Gravity.CENTER;
		textView.setLayoutParams(params);
		if(fragIsOn)
		   itemsContainer.addView(textView);
		return textView;
	}	
	
	
	@Override
	public boolean onBackPressed() {
		
		if (isFilltered){
			setCategoryAndSubCategoryTypes(currentCategoryType, 0,"", false);
			refreshListShowAll();
			isFilltered = false;
			return true;
		}
		return false;
	}
	
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		
		}
	}
	
	
	@Override
	public void onYoursItemClick(YoursItem item, View view) {
		
		if (item.getItemName().contains("סינמה סיטי")){
			getSinemaLink(item);
			return;
		}
		if(item.getStatus() == YoursItem.STATUS_EXTERNAL){
			FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
					PurchaseChooseSeatPashbarFragment.newInstance(item,
							null,PurchaseChooseSeatPashbarFragment.TYPE_PASHBAR), R.id.content_frame);
			return;
		}
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(), 
				YoursItemDetailsFragment.newInstance(item), R.id.content_frame);
	}

	
	@Override
	public void onShowMoreClick(SubCategory subCategory, View view) {
		setCategoryAndSubCategoryTypes(currentCategoryType, subCategory.getSubCategoryMenuId(),
				subCategory.getSubCategoryName(), true);
		refreshListByFillter();
		isFilltered = true;
	}

	
	private void getSinemaLink(final YoursItem selectedItem) {
		showProgressDialog();
		
		YoursApp.networkManager.getSinemaSityLink("", new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				Log.e("pahsbarlog", "res = " + response.toString());
				try {
					JSONObject jsonObject = new JSONObject(response.getXmlContent());
					String phasbarLink = jsonObject.optString("GetRequestCinemaCityResult").replace("\"", "");

					selectedItem.setExternalLink(phasbarLink);
					selectedItem.setSessionID(phasbarLink,PurchaseChooseSeatPashbarFragment.TYPE_CINEMA);
					FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
							PurchaseChooseSeatPashbarFragment.newInstance(selectedItem,
									null,PurchaseChooseSeatPashbarFragment.TYPE_CINEMA), R.id.content_frame);
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}
			
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
			}
			
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
		
	}
	


}

