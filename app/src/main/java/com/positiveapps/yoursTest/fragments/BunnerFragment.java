/**
 * 
 */
package com.positiveapps.yoursTest.fragments;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.util.BitmapUtil;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * @author natiapplications
 *
 */
public class BunnerFragment extends Fragment implements OnClickListener{
	
	public static final String EXTRA_BANNER = "Bunner";
	public static final String EXTRA_POSITION = "Position";
	
	private YoursItem banner;
	private int position;
	private OnBunnerClickListener onBunnerClickListener;
	
	public BunnerFragment() {

	}

	public static BunnerFragment newInstance(YoursItem banner,int position) {
		BunnerFragment instance = new BunnerFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_BANNER, banner);
		bundle.putInt(EXTRA_POSITION, position);
		instance.setArguments(bundle);
		return instance;
	}
	
	public void setOnBunnerClickListener (OnBunnerClickListener listener){
		this.onBunnerClickListener = listener;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.banner_fragment, container,
				false);
		banner = (YoursItem)getArguments().getSerializable(EXTRA_BANNER);
		position = getArguments().getInt(EXTRA_POSITION);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		ImageView bannerImage = (ImageView)view.findViewById(R.id.banner_image);
	//	BitmapUtil.loadImageIntoByUrl(banner.getImageUrl(), bannerImage, R.drawable.baner, R.drawable.baner, 600, 300, null);
		BitmapUtil.loadOrginalImageIntoBtUrl(banner.getImageUrl(), bannerImage, R.drawable.baner, R.drawable.baner);
		view.setOnClickListener(this);
		
		
	}

	
	@Override
	public void onClick(View v) {
		if (onBunnerClickListener != null) {
			onBunnerClickListener.onBunnerClick(banner, v,position);
		}
	}
	
	public interface OnBunnerClickListener {
		public void onBunnerClick(YoursItem item,View v,int position);
	}

}
