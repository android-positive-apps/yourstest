/**
 * 
 */
package com.positiveapps.yoursTest.fragments.purchase;



import java.util.ArrayList;
import java.util.Calendar;

import org.json.JSONArray;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.dialogs.DialogManager;
import com.positiveapps.yoursTest.fragments.BaseFragment;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.ItemDate;
import com.positiveapps.yoursTest.objects.RowGeneralItem;
import com.positiveapps.yoursTest.objects.SamGeneralItem;
import com.positiveapps.yoursTest.objects.SamObject;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.ui.YoursItemView;
import com.positiveapps.yoursTest.util.BitmapUtil;
import com.positiveapps.yoursTest.util.FragmentsUtil;
import com.positiveapps.yoursTest.util.ToastUtil;


/**
 * @author natiapplications
 *
 */
public class PurchaseNoChooseSeatsFragment extends BaseFragment implements OnClickListener{
	
	public static final String 	EXTRA_YOURS_ITEM = "YoursItem";
	public static final String 	EXTRA_ITEM_DATE = "ItemDate";
	
	private LinearLayout itemFrame;
	private Button addticketBtn;
	private Button minusTicketBtn;
	private Button continueBtn;
	private EditText ticketNumbertEt;
	
	
	private int ticketCounter;
	private YoursItem selectedItem;
	private ItemDate itemDate;
	
	private int currentPrice;
	private int currentClubPrice;

	public PurchaseNoChooseSeatsFragment() {

	}

	public static PurchaseNoChooseSeatsFragment newInstance(YoursItem yoursItem,ItemDate itemDate) {
		PurchaseNoChooseSeatsFragment instance = new PurchaseNoChooseSeatsFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(EXTRA_YOURS_ITEM, yoursItem);
		bundle.putSerializable(EXTRA_ITEM_DATE, itemDate);
		instance.setArguments(bundle);
		return instance;
	}

	@SuppressLint("Recycle")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_prochase_no_chooser, container,
				false);
		selectedItem = (YoursItem)getArguments().getSerializable(EXTRA_YOURS_ITEM);
		itemDate = (ItemDate)getArguments().getSerializable(EXTRA_ITEM_DATE);
		
		if (itemDate == null){
			currentPrice = (int) selectedItem.getItemOriginalPrice();
			currentClubPrice = (int) selectedItem.getItemClubPrice();
		}else{
			currentClubPrice = itemDate.getClubPrice();
			currentPrice = (int) itemDate.getPrice();
		}
		ticketCounter = 1;
		return rootView;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onViewCreated(view, savedInstanceState);
		MainActivity.setScreenTitle(getString(R.string.choose_sam_tickets));
		itemFrame = (LinearLayout)view.findViewById(R.id.item_frame);
		addItemFrameView ();
		
		
		addticketBtn = (Button)view.findViewById(R.id.add_ticket_btn);
		minusTicketBtn = (Button)view.findViewById(R.id.minus_ticket_btn);
		continueBtn = (Button)view.findViewById(R.id.continue_btn);
		ticketNumbertEt = (EditText)view.findViewById(R.id.ticket_numbers_et);
		ticketNumbertEt.setText(String.valueOf(ticketCounter));
		
		addticketBtn.setBackgroundDrawable(Category.CATEGORIES_ADD_NUTTONS.getDrawable(selectedItem.getParentType()));
		minusTicketBtn.setBackgroundDrawable(Category.CATEGORIES_MINUS_BUTTONS.getDrawable(selectedItem.getParentType()));
		continueBtn.setBackgroundDrawable(Category.CATEGORIES_CONTINUE_BUTTONS.getDrawable(selectedItem.getParentType()));
		
		addticketBtn.setOnClickListener(this);
		minusTicketBtn.setOnClickListener(this);
		continueBtn.setOnClickListener(this);
		
		ticketNumbertEt.addTextChangedListener(new OnTicketNumberETChangeListener());		
	}

	
	
	private void addItemFrameView() {
		
		if (selectedItem.getStatus() == YoursItem.STATUS_BUNNER){
			View view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.banner_item_layout, null);
			ImageView imageView = (ImageView)view.findViewById(R.id.item_image);
			itemFrame.addView(view);
			BitmapUtil.loadImageIntoByUrl(selectedItem.getBannerImage(), imageView, R.drawable.ic_action_picture,
					R.drawable.ic_action_picture, YoursApp.generalSettings.getScreenWidth(), YoursApp.generalSettings.getScreenHight()/3, null);
			return;
		}
		
		YoursItemView yoursItemView = new YoursItemView(selectedItem, null);
		itemFrame.addView(yoursItemView.getItemView());
		
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.continue_btn:
				
				notifyServerAboutOrder();
				break;
			case R.id.add_ticket_btn:
				ticketCounter++;
				Log.e("maxorderlog", "maxorder = " + selectedItem.getMaxOrder());
				if (selectedItem.getMaxOrder() > 0){
					if (ticketCounter > selectedItem.getMaxOrder()){
						ticketCounter = selectedItem.getMaxOrder();
					}
				}
				
				updateTicketNumberEt();
				break;
			case R.id.minus_ticket_btn:
				ticketCounter--;
				if (ticketCounter <1){
					ticketCounter = 1;
				}
				updateTicketNumberEt();
				break;
		}
	}
	
	private void updateTicketNumberEt () {
		ticketNumbertEt.setText(String.valueOf(ticketCounter));
	}
	
	private void openSamProchaseFragment (int clabPriceAmount,int samPrice,int terminal,
			String userName,String pelecardPass){
		SamGeneralItem samGeneralItem = new SamGeneralItem();
		int iter = Integer.parseInt(ticketNumbertEt.getText().toString());
		
		ArrayList<RowGeneralItem> items = new ArrayList<RowGeneralItem>();
		if (/*selectedItem.getStatus() == YoursItem.STATUS_BUNNER*/false){
			for (int i = 0; i < iter; i++) {
				RowGeneralItem row = new RowGeneralItem();
				row.setName(selectedItem.getItemName());
				row.setTicketType(getString(R.string.club_price_type));
				row.setMonth(Calendar.getInstance().get(Calendar.MONTH));
				row.setPrice(currentClubPrice);
				items.add(row);
			}
		}else{
			for (int i = 0; i < clabPriceAmount; i++) {
				RowGeneralItem row = new RowGeneralItem();
				row.setName(selectedItem.getItemName());
				row.setTicketType(getString(R.string.club_price_type));
				row.setMonth(Calendar.getInstance().get(Calendar.MONTH));
				row.setPrice(currentClubPrice);
				items.add(row);
			}
			
			for (int i = clabPriceAmount; i < iter; i++) {
				RowGeneralItem row = new RowGeneralItem();
				row.setName(selectedItem.getItemName());
				row.setTicketType(getString(R.string.regular_price_type));
				row.setMonth(Calendar.getInstance().get(Calendar.MONTH));
				row.setPrice(currentPrice);
				items.add(row);
			}
			
		}
		
		samGeneralItem.setItems(items);
		samGeneralItem.setSamPrice(samPrice);
		PurchaseSumFragment samFrag = PurchaseSumFragment.newInstance(SamObject.TUPE_GENERAL_ITEM,
				samGeneralItem,terminal,userName,pelecardPass);
		samFrag.setYoursItem(selectedItem);
		FragmentsUtil.openFragmentRghitToLeft(getFragmentManager(),
				samFrag, R.id.content_frame);
	}
	
	
	private void notifyServerAboutOrder () {
		showProgressDialog();
		int keyOrder = 0;
		if (selectedItem.getStatus() == YoursItem.STATUS_BUNNER || selectedItem.getParentType() == Category.CATEGORY_PROCHASES){
			keyOrder = YoursItem.KEY_ORDER_BUNNER;
		}else{
			keyOrder = YoursItem.KEY_ORDER_GENERAL;
		}
		String id = selectedItem.getId()+"";
		if (itemDate != null){
			id = itemDate.getId()+"";
		}
		
		YoursApp.networkManager.preOrder("",selectedItem.getKeyMenu()+"",ticketNumbertEt.getText().toString(),
				id, selectedItem.getSubMenuId()+"", "",selectedItem.getSessionID(), new NetworkCallback(){
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				ResponseObject responseObject = new ResponseObject(toParse);
				return responseObject;
			}
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				dismisProgressDialog();
				
				
				Log.e("setorderseatslog", "result = " + response.getXmlContent());
				String result = response.getXmlContent();
				String content = "";
				try {
					JSONObject jsonObject = new JSONObject(result);
					content = jsonObject.optString("PreOrderResult");
					JSONArray jsonArray = new JSONArray(content);
					JSONObject jsonResult = new JSONObject(jsonArray.optString(0));
					selectedItem.setActionNumber(jsonResult.optInt("ordernumber"));
					int clabPriceAmount = jsonResult.optInt("subsided");
					int samPrice = jsonResult.optInt("total");
					int terminal = jsonResult.optInt("PeleCarddTerminal");
					String userName = jsonResult.optString("PeleCarddUser");
					String pelecardPass = jsonResult.optString("PeleCarddPass");
					openSamProchaseFragment(clabPriceAmount,samPrice,terminal,userName,pelecardPass);
				} catch (Exception e) {
					showErrorDialog(content);
				}
				
			}
			
			@Override
			public void onError() {
				super.onError();
				dismisProgressDialog();
				ToastUtil.toster( getString(R.string.general_network_error), false);
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				dismisProgressDialog();
			}
		});
	}
	
	

	
	protected void showErrorDialog(String content) {
		if (content != null && !content.isEmpty()){
			content = content.replace("error:", "");
		}else{
			content = getString(R.string.general_network_error);
		}
		DialogManager.showErrorDialog(this, content, null);
	}



	class OnTicketNumberETChangeListener implements TextWatcher{

		
		@Override
		public void afterTextChanged(Editable s) {}

		
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			String currentNumber = ticketNumbertEt.getText().toString();
			if(currentNumber.isEmpty()){
				ticketCounter = 1;
				updateTicketNumberEt();
				return;
			}else if (currentNumber.length() == 1 && currentNumber.equalsIgnoreCase("0")){
				ticketCounter = 1;
				updateTicketNumberEt();
				return;
			}else{
				ticketCounter = Integer.parseInt(currentNumber);
				if (selectedItem.getMaxOrder() > 0){
					if (ticketCounter > selectedItem.getMaxOrder()){
						ticketCounter = selectedItem.getMaxOrder();
					}
				}
				
			}
			
		}
		
	}


}
