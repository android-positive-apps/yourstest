/**
 * 
 */
package com.positiveapps.yoursTest.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.google.android.gms.gcm.GoogleCloudMessaging;



/**
 * @author Nati Application
 *
 */
public class GcmIntentService extends IntentService {
	
	
	public static final int NOTIFICATION_ID = 1;
	
	
	NotificationCompat.Builder builder;
	
	

	public GcmIntentService() {
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		String messageType = gcm.getMessageType(intent);
		Log.e("receive message 2","********************"+ messageType +"*************************");
		Log.e("receive message 2","extra is null ? "+ (extras == null) +"*************************");
		try {
			
			if (extras != null){
				handelNotification(extras);
				
				
			}
		}catch(Exception e){
			
		}
		
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void handelNotification (Bundle extras){
		
		
	}
	
	

	

}
