/**
 * 
 */
package com.positiveapps.yoursTest.dialogs;

import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


/**
 * @author natiapplications
 *
 */
public class SearchDialog extends BaseDialogFragment implements OnClickListener{
	
	public static final String DIALOG_NAME = "SearchDialog";
	
	
	private Fragment parent;
	
	
	private EditText searchEt;
	private Button searchBtn;
	
	private DialogCallback callback;
	
	/**
	 * 
	 * @param description
	 * @param credits
	 * @param callback
	 */
	public SearchDialog(Fragment parent,DialogCallback callback){
		//this.mDescription = description;
		this.callback = callback;
		this.parent = parent;
	}
	

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_search, container, false);
                      
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        
        searchEt = (EditText)view.findViewById(R.id.search_et);
        searchBtn = (Button)view.findViewById(R.id.search_btn);
        
        searchBtn.setOnClickListener(this);
 
        return view;
    }


  

    private void dismissDialog(){
    	
    	this.dismiss();
    }
    
    

	@Override
	public void onClick(View v) {
				
    	dismissDialog();
    	InputMethodManager imm = (InputMethodManager)YoursApp.appContext.getSystemService(
    		      Context.INPUT_METHOD_SERVICE);
    		imm.hideSoftInputFromWindow(searchEt.getWindowToken(), 0);
    	if(callback != null){
    		callback.onDialogButtonPressed(v.getId(), this,searchEt.getText().toString());
    	}
	}


	

	
}

