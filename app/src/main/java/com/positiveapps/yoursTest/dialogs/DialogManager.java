/**
 * 
 */
package com.positiveapps.yoursTest.dialogs;


import android.support.v4.app.Fragment;

/**
 * @author Maor
 *
 */
public class DialogManager {



	
	public static void showDialogChooser(Fragment parentFragment, String[] optionsText,
			DialogCallback callback){
		
		CooserDialog dialog = new CooserDialog(parentFragment,optionsText,callback);
		dialog.show(parentFragment.getFragmentManager(), CooserDialog.DIALOG_NAME);
	}
	
	public static void showAppDialog(Fragment parent,String title,
			String message,int iconBg, int icon,AppDialogButton[] buttons,boolean cancelable,
			DialogCallback callback){
		
		AppDialog dialog = new AppDialog(parent, title, message, iconBg, icon, buttons, callback);
		dialog.setCancelable(cancelable);
		dialog.show(parent.getFragmentManager(), AppDialog.DIALOG_NAME);
	}
	
	public static void showErrorDialog(Fragment parentFragment ,String desc,
			DialogCallback callback){
		
		ErrorDialog dialog = new ErrorDialog(parentFragment,desc,callback);
		
		dialog.show(parentFragment.getFragmentManager(), ErrorDialog.DIALOG_NAME);
	}
	
	public static void showSearchDialog(Fragment parentFragment,DialogCallback callback){
		
		SearchDialog dialog = new SearchDialog(parentFragment,callback);
		
		dialog.show(parentFragment.getFragmentManager(), ErrorDialog.DIALOG_NAME);
	}
	
}
