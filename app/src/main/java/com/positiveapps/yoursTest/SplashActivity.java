package com.positiveapps.yoursTest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.google.analytics.tracking.android.EasyTracker;
import com.positiveapps.yoursTest.network.CheckForUpdateTask;
import com.positiveapps.yoursTest.network.CheckForUpdateTask.CheckForUpdateCallback;
import com.positiveapps.yoursTest.util.AppUtil;

public class SplashActivity extends Activity implements CheckForUpdateCallback {

	
	private boolean activityIsOn;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeFullScreenActivity(this);
		setContentView(R.layout.activity_splash);
		
		
		activityIsOn = true;
		
	}
	
	
	@Override
	protected void onResume() {
		super.onResume();
		new CheckForUpdateTask(this).check();
	}
	
	@Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		activityIsOn = false;
	}
	
	private void openLoginActivity() {
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (activityIsOn) {
					startActivity(new Intent(SplashActivity.this, LoginActivity.class));
					finish();
				}
			}

		}, 2000);
	}
	
	private void openMainActivity() {
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				if (activityIsOn) {
					startActivity(new Intent(SplashActivity.this, MainActivity.class));
					finish();
				}
			}

		}, 1000);
	}
	
	class WaitToDataInitializerThread extends Thread {
		
		@Override
		public void run() {
			super.run();
			while (!YoursApp.initializerFinished()){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			YoursApp.printAllCategoriesData();
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					openMainActivity();
				}
			});
			
		}
	}

	

	
	
	private void checkForLogin (){
		String userId = YoursApp.userProfil.getUserNumber();
		if (userId == null || userId.isEmpty()){
			openLoginActivity();
		}else{
			YoursApp.createCategories();
			new WaitToDataInitializerThread().start();
		}
		
	}


	@Override
	public void onCheck(boolean success, int result, String oldVersion,
						String newVersion) {
		if (success){
			if (result != CheckForUpdateTask.RESULT_NOT_UPDATES_FOUND){
				CheckForUpdateTask.showNeedUpdateDialog(this,
						result == CheckForUpdateTask.RESULT_MUST_UPDATE ? true:false,this);
			}else{
				checkForLogin();
			}
		}else{
			checkForLogin();
		}
	}
	@Override
	public void onUpdateDialogButtonClicked(int buttonType) {
		switch (buttonType) {
		case CheckForUpdateTask.BUTTON_TYPE_UPDATE:
			finish();
			break;
		case CheckForUpdateTask.BUTTON_TYPE_NOT_NOW:
			checkForLogin();
			break;
		case CheckForUpdateTask.BUTTON_TYPE_CLOSE_APP:
			finish();
			break;
		}
		
	}
	
	
	
	
	
}
