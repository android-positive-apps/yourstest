package com.positiveapps.yoursTest;

import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.positiveapps.yoursTest.network.NetworkCallback;
import com.positiveapps.yoursTest.network.ResponseObject;
import com.positiveapps.yoursTest.objects.UserProfile;
import com.positiveapps.yoursTest.util.AppUtil;
import com.positiveapps.yoursTest.util.DialogUtil;
import com.positiveapps.yoursTest.util.TextUtil;
import com.positiveapps.yoursTest.util.ToastUtil;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

public class LoginActivity extends Activity implements OnClickListener{

	
	private EditText memberNumberEt;
	private EditText passwordEt;
	private Button loginBtn;
	private LinearLayout forgotPassBtn;
	private ProgressDialog dialog;
	private boolean activityIsOn;
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeFullScreenActivity(this);
		setContentView(R.layout.activity_login);
		activityIsOn = true;
		
		memberNumberEt = (EditText)findViewById(R.id.et_member_number);
		passwordEt = (EditText)findViewById(R.id.et_password);
		loginBtn = (Button)findViewById(R.id.btn_login);
		forgotPassBtn = (LinearLayout)findViewById(R.id.forgot_pass_btn);
		
		loginBtn.setOnClickListener(this);
		forgotPassBtn.setOnClickListener(this);
	}


	@Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityIsOn = false;
	}

	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_login:
			if (TextUtil.validation(memberNumberEt,passwordEt)){
				performLogin();
			}else{
				ToastUtil.toster(getString(R.string.fields_empty), false);
			}
			break;
		case R.id.forgot_pass_btn:
			openForgotPassActivity();
			break;
		}
	}


	/**
	 * 
	 */
	private void openForgotPassActivity() {
		startActivity(new Intent(this,ForgotPassActivity.class));
	}


	/**
	 * 
	 */
	private void performLogin() {
		dialog = DialogUtil.showProgressDialog(this, getString(R.string.deafult_dialog_messgae));
		YoursApp.networkManager.userLogin(memberNumberEt.getText().toString(), passwordEt.getText().toString(), new NetworkCallback(){
			
			@Override
			public ResponseObject parseResponse(JSONObject toParse) {
				UserProfile responseObject = new UserProfile(toParse);
				return responseObject;
			}
			
			
			@Override
			public void onDataRecived(ResponseObject response,
					boolean isHasError, String erroDescription) {
				super.onDataRecived(response, isHasError, erroDescription);
				
				if (!response.isHasError()){
					YoursApp.createCategories();
					new WaitToDataInitializerThread().start();
				}else{
					DialogUtil.dismisDialog(dialog);
					ToastUtil.toster(response.getErrorDesc(), false);
				}
			}
			
			@Override
			public void onError() {
				super.onError();
				ToastUtil.toster(getString(R.string.general_network_error), false);
				DialogUtil.dismisDialog(dialog);
			}
			
			@Override
			public void networkUnavailable(String message) {
				super.networkUnavailable(message);
				DialogUtil.dismisDialog(dialog);
			}
		});
		
	}



	/**
	 * 
	 */
	private void openMainActivity() {
		if (activityIsOn){
			DialogUtil.dismisDialog(dialog);
			ToastUtil.toster(getString(R.string.wellcome) + " " + YoursApp.userProfil.getUserFullName(" "), false);
			startActivity(new Intent(this,MainActivity.class));
			finish();
		}
	}
	
    class WaitToDataInitializerThread extends Thread {
		
		@Override
		public void run() {
			super.run();
			while (!YoursApp.initializerFinished()){
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			YoursApp.printAllCategoriesData();
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					openMainActivity();
				}
			});
			
		}
	}

}
