/**
 * 
 */
package com.positiveapps.yoursTest.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import android.util.Log;

/**
 * @author Nati Gabay
 *
 */
public class DateUtil {
	
	
	private static String[] months = {"January","February","March","April",
		"May","June","July","August","September","October","November","December"};
	
	
	
	public static String getCurrentTimeAsString () {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - hh:mm:ss");
		Log.e("timelog", "currentTime = " + calendar.getTimeInMillis());
		return sdf.format(calendar.getTime());
	}
	
	public static String getCurrentTimeAsServerString () {
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		Log.e("timelog", "currentTime = " + calendar.getTimeInMillis());
		return sdf.format(calendar.getTime());
	}
	
	public static String getDateAsStringByMilli (String dilimeter,long milli,boolean time) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf= null;
		if (time){
			sdf = new SimpleDateFormat("dd"+dilimeter+"MM"+dilimeter+"yyyy");
		}else{
			sdf = new SimpleDateFormat("dd"+dilimeter+"MM"+dilimeter+"yyyy hh:mm:ss" );
		}
		
		return sdf.format(calendar.getTime());
	}
	
	public static String getTimeAsStringByMilli (long milli) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(milli);
		SimpleDateFormat sdf = new SimpleDateFormat("hh"+":"+"mm");
		return sdf.format(calendar.getTime());
	}
	
	
	
	public static String getMonthName (int month){
		return months[month-1];
	}
	
	public static String getTimeAsString (int hour, int mintue){
		String h = hour+"";
		String m = mintue+"";
		if (hour < 10){
			h = "0"+h;
		}
		if (mintue < 10){
			m = "0"+m;
		}
		String time = h + ":" + m;
		return time;
	}
	
	public static String getDateAsString (int day, int month, int yeers){
		String d = day+"";
		String m = month+"";
		String y = yeers+"";
		if (day < 10){
			d = "0" + d;
		}
		if (month < 10){
			m = "0"+ m;
		}
		String date = d+"/"+m+"/"+y;
		return date;
	}
	
	public static long getDateInMilli(int day, int month, int yeers){
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, yeers);
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.DAY_OF_MONTH, day);
		
		return calendar.getTimeInMillis();
	}
	
	
	public static long getTimeInMilli(int hower, int seconds){
		long h = hower*360000;
		long s = seconds*60000;
		return h+s;
	}
	
	public static int[] getBestDate (String date){
		
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH)+1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		
		String curretnSetDate[] = date.split("/");
		if (curretnSetDate.length == 3){
			year = Integer.parseInt(curretnSetDate[2]);
			month = Integer.parseInt(curretnSetDate[1])-1;
			day = Integer.parseInt(curretnSetDate[0]);
		}
		
		int[] result = {day,month,year};
		return result;
	}
	
	public static int[] getBestTime (String time){
		Calendar calendar = Calendar.getInstance();
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		
		String[] curretnSetTime = time.split(":");
		if (curretnSetTime.length == 2){
			hour = Integer.parseInt(curretnSetTime[0]);
			minute = Integer.parseInt(curretnSetTime[1]);
		}
		int[] result = {hour,minute};
		return result;
	}
	
	public static long getCurrentDateInMilli () {
		Calendar calendar = Calendar.getInstance();
		return calendar.getTimeInMillis();
	}
	
	public static String getDateDescriptionByDate (Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String month = months[calendar.get(Calendar.MONTH)];
		
		return month + " " + calendar.get(Calendar.DAY_OF_MONTH)
				+ "." + calendar.get(Calendar.YEAR);
	}
	
	public static String getRangBetweenTwoDatesDescription (Date date1,Date date2) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");

		String from = sdf.format(date1);
		String to = sdf.format(date2);
		return from + " - " + to;
	}
	
	public static String getRangBetweenTwoDates (Date date1,Date date2) {
		long from = date1.getTime();
		long to = date2.getTime();
		long disTance = to - from;
		if (disTance <=0){
			return String.valueOf(0);
		}
		long result = disTance/60000;
		return String.valueOf(result);
	}
	
	public static long getDateMillisByServerDate(String serverDate) {
		long result = 0;
		String[] date = serverDate.split(" ");
		if (date.length > 0){
			String[] jastDate = date[0].split("-");
			int years = Integer.parseInt(jastDate[0]);
			int munts = Integer.parseInt(jastDate[1]);
			int days = Integer.parseInt(jastDate[2]);
			Calendar calendar = Calendar.getInstance();
			calendar.set(years, munts, days);
			result = calendar.getTimeInMillis();
		}
		return result;
	}
	
	public static int getDaysOfServerDate(String serverDate) {
		int result = 0;
		String[] date = serverDate.split(" ");
		if (date.length > 0){
			String[] jastDate = date[0].split("-");
			result = Integer.parseInt(jastDate[2]);
		}
		return result;
	}
	
	public static String getServerDateAsString (String serverDate) {
		if (serverDate.isEmpty()){
			return "";
		}
		String[] dateAndTime = serverDate.split("T");
		if (dateAndTime.length == 1){
			return serverDate;
		}else{
			return dateAndTime[0] + " " + dateAndTime[1];
		}
	}
	
	public static String getServerMillisAsDateString (String serverMillis){
		String parseDate = TextUtil.getNumberFromString(serverMillis);
		if (parseDate.isEmpty()){
			return "0";
		}
		long millis = Long.parseLong(parseDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(calendar.getTime());
	}
	
	public static String getServerMillisAsTimeString (String serverMillis){
		String parseDate = TextUtil.getNumberFromString(serverMillis);
		if (parseDate.isEmpty()){
			return "0";
		}
		long millis = Long.parseLong(parseDate);
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis*1000);
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
		return sdf.format(calendar.getTime());
	}
	

}
