package com.positiveapps.yoursTest.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.YoursApp;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;








public class BitmapUtil {
	
	
	/** CONSTANTS **/
	
	// name of the folder that file saved in
	private static  final String IMAGES_FLODER_NAME = "/Image";
	
	// name of the file
	private final String IMAGE_NAME = "image.jpeg";
	public static final String IMAGE_EXTENATION = "jpeg";
	
	
	/** CLASS METHODS **/


	
	@SuppressWarnings("finally")
	public static String getImageAsStringEncodedBase64(Bitmap bitmap) {
		if (bitmap == null){
			return "";
		}
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
		byte[] byteArray = stream.toByteArray();
		try {
			stream.close();
		} catch (IOException e) {}finally{
			return encodeImageTobase64(byteArray);
		}
	}
	
	public  Bitmap decodeBase64(String input) 
	{
	    byte[] decodedByte = Base64.decode(input, 0);
	    if (decodedByte.length == 0){
	    	return null;
	    }
	    return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length); 
	}
	
	private static String encodeImageTobase64(byte[] imageBytes) {
	       
		String encodedString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
		String test= "";
		Log.e("base64", encodedString);
		try {
			test = new String(encodedString.getBytes(),"UTF-8");
		} catch (UnsupportedEncodingException e1) {}
		return test;
	}
	
	
	/*
	 * automatically generates path of the image file
	 * */
	public static Uri createUriPathForStoringImage(String imageName) {

		String root = Environment.getExternalStorageDirectory()
				.getAbsolutePath();

		// add with image folder name
		File f = new File(root + IMAGES_FLODER_NAME);
		f.mkdirs();
		String image_fath = DateUtil.getCurrentTimeAsString() + imageName + "." + IMAGE_EXTENATION;

		// create the file after complete creating of the absolute file path
		File Image_file = new File(f, image_fath);
		
		// convert file to uri format
		Uri imageUri = Uri.fromFile(Image_file);
		return imageUri;

	}
	
	public static String cretatStringPathForStoringImage (String imageName){
		
		String root = Environment.getExternalStorageDirectory()
				.getAbsolutePath();

		// add with image folder name
		File f = new File(root + IMAGES_FLODER_NAME);
		f.mkdirs();
		String image_fath = DateUtil.getCurrentTimeAsString() + imageName + "." + IMAGE_EXTENATION;

		// create the file after complete creating of the absolute file path
		File Image_file = new File(f, image_fath);
		return Image_file.getAbsolutePath();
	}

	
	
	
	public Bitmap loadImgaeIntoByUri(Uri uri,ImageView into,int scale,int simpleSize,int deafult) {

		// option that resize the bitmap
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
		
		// create bitmap by image uri with the option
		Bitmap image = BitmapFactory.decodeFile(uri.getPath(), options);
		if (into != null)
		    setImageInto(image, into, scale, deafult);
		return image;
	}
	
	public static Bitmap loadImgaeIntoBySelectedUri(Uri selectedUri,ImageView into,int scale,int simpleSize,int deafult) {

		// option that resize the bitmap
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
				
		// create bitmap by image uri with the option
	    Bitmap image = BitmapFactory.decodeFile(getPathOfImagUri(selectedUri), options);
	    if (into != null)
		    setImageInto(image, into, scale, deafult);
	    return image;
	}
	
	public static  Bitmap loadImageIntoByStringPath(String path,ImageView into,int scale,int simpleSize,int deafult) {

		// option that resize the bitmap
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = simpleSize;
				
		// create bitmap by image uri with the option
	    Bitmap image = BitmapFactory.decodeFile(path, options);
	    if (into != null)
		    setImageInto(image, into, scale, deafult);
		return image;
	}
	
	
	public static void setImageInto (Bitmap image,ImageView into,int scale,int deafult) {
		if (image != null){
	    	
			image = crupAndScale(image, scale);
			into.setImageBitmap(image);
	    }else{
	    	into.setImageResource(deafult);
	    }
	}
	
	public static  Bitmap crupAndScale (Bitmap source,int scale){
		
		int factor = source.getHeight() <= source.getWidth() ? source.getHeight(): source.getWidth();
		int longer = source.getHeight() >= source.getWidth() ? source.getHeight(): source.getWidth();
		int x = source.getHeight() >= source.getWidth() ?0:(longer-factor)/2;
		int y = source.getHeight() <= source.getWidth() ?0:(longer-factor)/2;
		source = Bitmap.createBitmap(source, x, y, factor, factor);
		source = Bitmap.createScaledBitmap(source, scale, scale, false);
		return source;
		
	}
	
	public static  String getPathOfImagUri (Uri selectedUri){
		String[] filePathColumn = { MediaStore.Images.Media.DATA };
		Cursor cursor = YoursApp.appContext.getContentResolver().query(selectedUri,
				filePathColumn, null, null, null);
		cursor.moveToFirst();
		int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
		String picturePath = cursor.getString(columnIndex);
		cursor.close();
		return picturePath;
	}
	


	/*
	 * rotate image
	 * */
	public static Bitmap rotate(Uri imageUri,Bitmap image,int degree) {
		int oriantation = getCameraPhotoOrientation(imageUri);
		if (oriantation  == 0){
			return image;
		}
		int w = image.getWidth();
		int h = image.getHeight();

		Matrix mtx = new Matrix();
		mtx.postRotate(degree);

		return Bitmap.createBitmap(image, 0, 0, w, h, mtx, true);
	}

	
	/*
	 * 
	 * check if image needed rotating
	 * */
	public static int getCameraPhotoOrientation(Uri imageUri) {
		int rotate = 0;
		try {
			YoursApp.appContext.getContentResolver().notifyChange(imageUri, null);

			ExifInterface exif = new ExifInterface(imageUri.getPath());
			int orientation = exif.getAttributeInt(
					ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_NORMAL);

			switch (orientation) {
			case ExifInterface.ORIENTATION_ROTATE_270:
				rotate = 270;
				break;
			case ExifInterface.ORIENTATION_ROTATE_180:
				rotate = 180;
				break;
			case ExifInterface.ORIENTATION_ROTATE_90:
				rotate = 90;
				break;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return rotate;
	}
	
	public static void loadImageAndSaveItOnStorage (final String imageName,final String url,final LoadImageCallback callback){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
			Bitmap bitmap = null;
			 try {
				String fileName = cretatStringPathForStoringImage(imageName); 
				bitmap = BitmapFactory.decodeStream(new URL(url).openConnection().getInputStream());
				if (bitmap == null){
					return;
				}
				FileOutputStream out = new FileOutputStream(fileName);
			    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); 
			    if (callback != null){
			    	callback.onLoadImage(true, bitmap);
			    }
			} catch (Exception e) {
				if (callback != null){
			    	callback.onLoadImage(false, bitmap);
			    }
			}
			}
		}).start();
	}
	
	public static void loadImageIntoByUrl(String url, final ImageView target, 
			final int imageForPlaceHolder, final int imageWhenError, final int newWidth, final int newHeight, 
			final Callback callback){

    	if(url == null || url.isEmpty()){
    		url = "no_image";
    	}
    	
    	final String newUrl = url;
    	
		Picasso.with(MainActivity.mainInstance)
		.load(newUrl)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.resize(newWidth, newHeight)
		.into(target, new Callback() {
			
			@Override
			public void onSuccess() {
				if(callback != null){
					callback.onSuccess();
				}
				
				loadImage( newUrl, target, imageForPlaceHolder, imageWhenError, newWidth, newHeight);
			}
			
			@Override
			public void onError() {
				
			}
		});
    }
	
	public static void loadOrginalImageIntoBtUrl( String url, 
			ImageView target, int imageForPlaceHolder, int imageWhenError){
		
		Picasso.with(MainActivity.mainInstance)
		.load(url)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.into(target);
		
	}
	
	private static void loadImage( String url, 
			ImageView target, int imageForPlaceHolder, int imageWhenError, 
			int newWidth, int newHeight){
		
		Picasso.with(MainActivity.mainInstance)
		.load(url)
		.placeholder(imageForPlaceHolder)
		.error(imageWhenError)
		.resize(newWidth, newHeight)
		.into(target);
		
	}
	
	public interface LoadImageCallback {
		public void onLoadImage (boolean success,Bitmap image);
	}

}
