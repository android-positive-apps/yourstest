/**
 * 
 */
package com.positiveapps.yoursTest.util;
import java.util.List;


import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.SettingsActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


/**
 * @author natiapplications
 *
 */
public class NotificationUtil {
	
	public static final int NOTIFICATION_ID = 1;
	
	public static void sendChatNotification(String groupId,boolean addPendingIntent) {
		
		int counter  = 0;/*YoursApp.appPreference.getUserProfil().getNotificationCounter();
		counter++;
		YoursApp.appPreference.getUserProfil().setNotificationCount(counter);*/
		
		NotificationManager mNotificationManager = (NotificationManager) 
				YoursApp.appContext.getSystemService(Context.NOTIFICATION_SERVICE);

		

		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
				YoursApp.appContext).setSmallIcon(R.drawable.ic_launcher)
				.setContentTitle(YoursApp.appContext.getString(R.string.notification_title))
				.setStyle(new NotificationCompat.BigTextStyle().bigText
						(YoursApp.appContext.getString(R.string.notification_title)
								.replace("*", counter+"")))
				.setContentText(YoursApp.appContext.getString(R.string.notification_title)
						.replace("*", counter+"")).
								setSound( RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

		Intent intent = null;
		Log.e("notificationloglog", "is add pending intent ? " + addPendingIntent);
		if (addPendingIntent){
			intent = new Intent(YoursApp.appContext, MainActivity.class);
			intent.putExtra(MainActivity.EXTRA_FROM_NOTIFICATION, true);
		}else{
			intent = new Intent(YoursApp.appContext, SettingsActivity.class);
		}
		
		PendingIntent contentIntent = PendingIntent.getActivity(YoursApp.appContext, 0,
				intent,Intent.FLAG_ACTIVITY_NEW_TASK);
		mBuilder.setContentIntent(contentIntent);
		mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
	}
	
	public static void setBadge(Context context, int count) {
	    String launcherClassName = getLauncherClassName(context);
	    if (launcherClassName == null) {
	        return;
	    }
	    Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
	    intent.putExtra("badge_count", count);
	    intent.putExtra("badge_count_package_name", context.getPackageName());
	    intent.putExtra("badge_count_class_name", launcherClassName);
	    context.sendBroadcast(intent);
	}

	public static String getLauncherClassName(Context context) {

	    PackageManager pm = context.getPackageManager();

	    Intent intent = new Intent(Intent.ACTION_MAIN);
	    intent.addCategory(Intent.CATEGORY_LAUNCHER);

	    List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
	    for (ResolveInfo resolveInfo : resolveInfos) {
	        String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
	        if (pkgName.equalsIgnoreCase(context.getPackageName())) {
	            String className = resolveInfo.activityInfo.name;
	            return className;
	        }
	    }
	    return null;
	}

}
