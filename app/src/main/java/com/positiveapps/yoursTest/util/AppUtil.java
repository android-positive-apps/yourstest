/**
 * 
 */
package com.positiveapps.yoursTest.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.services.LocationService;

/**
 * @author Nati Gabay
 *
 */
public class AppUtil {
	
	
	
	/**
	 * get the application version name
	 * 
	 * @param context - context of current application
	 * @return the application version name
	 */
	public  static String getApplicationVersion(Context context) {
		String appVersion = "";
		try {
			PackageManager manager = context.getPackageManager();
			PackageInfo info = manager.getPackageInfo(
					context.getPackageName(), 0);
			appVersion =  info.versionName;
		} catch (Exception e) {
		}
		return appVersion;
	}
	
	
	/**
	 * set up activity to display on full screen mood
	 * 
	 * @param activity - activity to set up
	 */
	public static void makeFullScreenActivity (Activity activity){
		activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
	}
	
	/**
	 * set up activity to display on full screen mood
	 * 
	 * @param activity - activity to set up
	 */
	public static void makeNoTitleActivity (Activity activity){
		activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		/*activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
	}
	

	
    public static void setTextFonts (Activity activity,View view){
		
		ArrayList<View> childs = new ArrayList<View>();
		
		if (view instanceof TextView){
			String font = (String) view.getTag();
			Log.e("fontLog", "font = " + font);
			if (font != null){
				try {
					TextView tv = (TextView)view;
					Typeface typeface = Typeface.createFromAsset(activity.getAssets(), font);
					tv.setTypeface(typeface);
				} catch (Exception e) {}
					
			}
		}
		if (view instanceof Button){
			String font = (String) view.getTag();
			Log.e("fontLog","font = " + font);
			if (font != null){
				Button btn = (Button)view;
				Typeface typeface = Typeface.createFromAsset(activity.getAssets(), font);
				btn.setTypeface(typeface);
			}
		}
		if (view instanceof EditText){
			String font = (String) view.getTag();
			Log.e("fontLog","font = " + font);
			if (font != null){
				EditText et = (EditText)view;
				Typeface typeface = Typeface.createFromAsset(activity.getAssets(), font);
				et.setTypeface(typeface);
			}
		}
		if (view instanceof ViewGroup){
			ViewGroup group = (ViewGroup)view;
			for(int i=0; i<group.getChildCount(); ++i) {
			    View nextChild = group.getChildAt(i);
			    childs.add(nextChild);
			}
			if (childs.size() > 0){
				for (int i = 0; i < childs.size(); i++) {
					setTextFonts(activity,childs.get(i));
				}
			}else{
				return;
			}
		}
		
	}
    
    public static void stopLocationService (Context context) {
		try {
			context.stopService(new Intent(context,LocationService.class));
		} catch (Exception e) {}
	}
    
    
    public static void startLocationService(Context context)
	{
		Log.e("locationService", "ceck location service");
	    try {
			context.stopService(new Intent(context,LocationService.class));
		} catch (Exception e) {}

	    Intent mServiceIntent = new Intent(context, LocationService.class);
	    context.startService(mServiceIntent);
	}
    

    
    
    public static String cleanText (String toClean){
    	String result = "";
    	try {
    		for (int i = 0; i < toClean.length(); i++) {
        		char temp = toClean.charAt(i);
        		if (temp >= '0' && temp <='9'){
        			result += temp;
        		}
    			
    		}
		} catch (Exception e) {}
    	return result;
    }
    
    public static void linkifyReadMore (TextView toLink,String toSet) {
    	/*Pattern pattern = Pattern.compile("read more");
    	toLink.setText(toSet);
    	Linkify.addLinks(toLink,pattern, "");*/
    	String text = "<font color=#708090>"+toSet+"</font> <font color=#82d0d4>read more</font>";
    	toLink.setText(Html.fromHtml(text));
    }
    
    public static void printKeyHash(Context context){
	    // Add code to print out the key hash
	    try {
	        PackageInfo info = context.getPackageManager().getPackageInfo(
	                "com.positiveapps.skitogo", 
	                PackageManager.GET_SIGNATURES);
	        for (Signature signature : info.signatures) {
	            MessageDigest md = MessageDigest.getInstance("SHA");
	            md.update(signature.toByteArray());
	            Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
	            }
	    } catch (NameNotFoundException e) {
	        Log.d("KeyHash:", e.toString());
	    } catch (NoSuchAlgorithmException e) {
	        Log.d("KeyHash:", e.toString());
	    }
	}
    
  
    
    public static void saveScreenDimention(Activity activity) {

		WindowManager w = activity.getWindowManager();
		Display d = w.getDefaultDisplay();
		DisplayMetrics metrics = new DisplayMetrics();
		d.getMetrics(metrics);
		// since SDK_INT = 1;
		int widthPixels = metrics.widthPixels;
		int heightPixels = metrics.heightPixels;
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 14 && Build.VERSION.SDK_INT < 17)
		try {
		    widthPixels = (Integer) Display.class.getMethod("getRawWidth").invoke(d);
		    heightPixels = (Integer) Display.class.getMethod("getRawHeight").invoke(d);
		} catch (Exception ignored) {
		}
		// includes window decorations (statusbar bar/menu bar)
		if (Build.VERSION.SDK_INT >= 17)
		try {
		    Point realSize = new Point();
		    Display.class.getMethod("getRealSize", Point.class).invoke(d, realSize);
		    widthPixels = realSize.x;
		    heightPixels = realSize.y;
		} catch (Exception ignored) {
		}

		YoursApp.generalSettings.setScreenWidth(widthPixels);
		YoursApp.generalSettings.setScreenHight(heightPixels);
		
	}
    
    
  
    
    

}
