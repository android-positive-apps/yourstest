/**
 * 
 */
package com.positiveapps.yoursTest.util;



import java.io.File;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.util.Log;

/**
 * @author natiapplications
 *
 */
public class ContentProviderUtil {
	
	
	public static void openLocationChooser (Activity activity,String lat,String lon) {
		Uri location = Uri.parse("geo:0,0?q="+lat + "," + lon);
		 Intent intent = new Intent(Intent.ACTION_VIEW,location);
        activity.startActivity(intent);
	}
	
	public static void openSMSScreen (Activity activity,String message,String address) {
        Intent intent = new Intent("android.intent.action.VIEW");
        Uri data = Uri.parse("sms:"+address);
        intent.setData(data);
        intent.putExtra("address", address);
        intent.putExtra("sms_body", message);
       
        
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
	}
	
	
	public static void openGallery (Fragment fragment,int requestCode){
		Intent pickPhoto = new Intent(Intent.ACTION_PICK,
		           android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		
		fragment.startActivityForResult(pickPhoto , requestCode);
	}
	
	public static void openCamera (Fragment fragment){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.startActivity(takePicture);
	}
	
	public static void openCameraForResult (Fragment fragment,int requestCode){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		fragment.startActivityForResult(takePicture,requestCode);
	}
	
	public static void openCameraForResultWithUri (Fragment fragment,int requestCode,Uri uri){
		Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		takePicture.setData(uri);
		fragment.startActivityForResult(takePicture,requestCode);
	}
	
	public static void openImageInGallery (Activity activity,String phth) {
		Intent intent = null;
		intent = new Intent(android.content.Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.parse("file://"+phth), "image/*");
		activity.startActivity(intent);
	}
	
	public static void mackCallPhone (Fragment fragment,String phone) {
		
		Uri callUri = Uri.parse("tel://" + phone);
		Intent callIntent = new Intent(Intent.ACTION_CALL,callUri);
		callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_USER_ACTION);
		fragment.startActivity(callIntent);
	}
	
	public static void openGPSSettings (Fragment fragment) {
		fragment.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettings (Activity activiy) {
		activiy.startActivity(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS));
	}
	
	public static void openGPSSettingsForResult (Activity activity,int requestCode) {
		activity.startActivityForResult(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS),requestCode);
	}
	
	public static void openGPSSettingsForResult (Fragment fragment,int requestCode) {
		fragment.startActivityForResult(new Intent(
				Settings.ACTION_LOCATION_SOURCE_SETTINGS),requestCode);
	}
	
	public static void openBrowser (Fragment fragment,String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		fragment.getActivity().startActivity(browserIntent);
	}
	
	public static void openBrowser (Activity activity,String url){
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
		activity.startActivity(browserIntent);
	}
	
	public static void openWaze (Activity activity,String address) {
		
		try
		{
			Uri location = Uri.parse("waze://?q="+address);
			Intent intent = new Intent(Intent.ACTION_VIEW,location);
			activity.startActivity(intent);
		}
		catch ( Exception ex  )
		{
		   Intent intent =
		    new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
		   activity.startActivity(intent);
		}
	}
	
	public static void playMediaByUrl (Fragment fragment,String type,String url){
		if (url == null){
			return;
		}
		Log.i("onitemclicklog", "url = " + url);
		try {
			Intent intent = new Intent();
			intent.setAction(Intent.ACTION_VIEW);
			intent.setDataAndType(Uri.parse(url),type);
			fragment.startActivity(intent);
		} catch (Exception e) {
			try {
				Intent intent = new Intent();
				intent.setAction(Intent.ACTION_VIEW);
				Uri uri = Uri.fromFile(new File(url));
				intent.setDataAndType(uri,type);
				fragment.startActivity(intent);
			} catch (Exception e2) {
				ToastUtil.toster("Can not playing this file. file path:" + url, true);
			}
			
		}
	}

}
