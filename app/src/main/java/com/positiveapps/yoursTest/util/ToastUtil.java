package com.positiveapps.yoursTest.util;

import com.positiveapps.yoursTest.YoursApp;

import android.widget.Toast;

public class ToastUtil {
	
	public static void toster (String message, boolean length){
		try {
			if (length){
				Toast.makeText(YoursApp.appContext, message, Toast.LENGTH_LONG).show();
			}else{
				Toast.makeText(YoursApp.appContext, message, Toast.LENGTH_SHORT).show();
			}
		} catch (Exception e) {
		}
	}
	
	
}
