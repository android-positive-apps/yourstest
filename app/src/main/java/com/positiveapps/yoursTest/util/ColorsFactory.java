/**
 * 
 */
package com.positiveapps.yoursTest.util;

import com.positiveapps.yoursTest.R;

/**
 * @author natiapplications
 *
 */
public class ColorsFactory {
	
	public static final int[] famillyColors = {R.color.family_1,R.color.family_2,R.color.family_3,R.color.family_4,R.color.family_5,R.color.family_6
	,R.color.family_1,R.color.family_2,R.color.family_3,R.color.family_4,R.color.family_5,R.color.family_6};
	public static final int[] funColors = {R.color.fun_1,R.color.fun_2,R.color.fun_3,R.color.fun_4,R.color.fun_5,R.color.fun_6
	,R.color.fun_1,R.color.fun_2,R.color.fun_3,R.color.fun_4,R.color.fun_5,R.color.fun_6};
	public static final int[] hobyColors = {R.color.hoby_1,R.color.hoby_2,R.color.hoby_3,R.color.hoby_4,R.color.hoby_5,R.color.hoby_6,
			R.color.hoby_1,R.color.hoby_2,R.color.hoby_3,R.color.hoby_4,R.color.hoby_5,R.color.hoby_6};
	public static final int[] prochaseColor = {R.color.prochase_1,R.color.prochase_2,R.color.prochase_3,R.color.prochase_4,R.color.prochase_5,R.color.prochase_6,
			R.color.prochase_1,R.color.prochase_2,R.color.prochase_3,R.color.prochase_4,R.color.prochase_5,R.color.prochase_6};
	public static final int[] monthColor = {R.color.month_1,R.color.month_2,R.color.month_3,R.color.month_4,R.color.month_5,R.color.month_6,
			R.color.month_1,R.color.month_2,R.color.month_3,R.color.month_4,R.color.month_5,R.color.month_6};

	
}
