/**
 * 
 */
package com.positiveapps.yoursTest.util;

import java.util.regex.Pattern;

import com.positiveapps.yoursTest.YoursApp;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Build;
import android.provider.Settings.Secure;
import android.util.Patterns;

/**
 * @author Nati Gabay
 *
 */
public class DeviceUtil {
	
	public static String getUserEmail(){
		String possibleEmail = "";
		Pattern emailPattern = Patterns.EMAIL_ADDRESS; 
		Account[] accounts = AccountManager.get(YoursApp.appContext).getAccounts();
		for (Account account : accounts) {
		    if (emailPattern.matcher(account.name).matches()) {
		    	possibleEmail = account.name;
		    }
		}
		return possibleEmail;
	}
	
	public static String getDeviceUDID() {

		return Secure.getString(YoursApp.appContext.getContentResolver(),
				Secure.ANDROID_ID);
	}
	
	public static String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		if (model.startsWith(manufacturer)) {
			return capitalize(model);
		} else {
			return capitalize(manufacturer) + " " + model;
		}
	}

	public static String getDeviceVersion () {
		
		String myVersion = "";
		if (android.os.Build.VERSION.RELEASE != null){
			myVersion = android.os.Build.VERSION.RELEASE;
		}
		return myVersion;
	}
	
	private static String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	} 
	
	

}
