package com.positiveapps.yoursTest;

import com.google.analytics.tracking.android.EasyTracker;
import com.positiveapps.yoursTest.fragments.settings.AboutFragment;
import com.positiveapps.yoursTest.fragments.settings.ContactUsFragment;
import com.positiveapps.yoursTest.fragments.settings.EulaFragment;
import com.positiveapps.yoursTest.fragments.settings.HistoryListFragment;
import com.positiveapps.yoursTest.fragments.settings.UpdateProfileFragment;
import com.positiveapps.yoursTest.util.AppUtil;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class SettingsActivity extends FragmentActivity {
	
	
	public static final int SETTINGS_TYPE_ABOUT = 1;
	public static final int SETTINGS_TYPE_CONTACT_US =2;
	public static final int SETTINGS_TYPE_EULA = 3;
	public static final int SETTINGS_TYPE_UPDATE_DETAILS =4;
	public static final int SETTINGS_TYPE_PROCHASES_HISTORY =5;
	public static final int SETTINGS_TYPE_CANCEL_HISTORY =6;
	
	
	public static final String EXTRA_TYPE = "ExtraType";
	
	private int currentSettingsType;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AppUtil.makeNoTitleActivity(this);
		setContentView(R.layout.activity_settings);
		currentSettingsType = getIntent().getExtras().getInt(EXTRA_TYPE);
		openAporperateFragment();
	}

	@Override
	  public void onStart() {
	    super.onStart();
	    EasyTracker.getInstance(this).activityStart(this);  // Add this method.
	  }

	  @Override
	  public void onStop() {
	    super.onStop();
	    EasyTracker.getInstance(this).activityStop(this);  // Add this method.
	  }
	
	private void openAporperateFragment() {
		switch (currentSettingsType) {
		case SETTINGS_TYPE_ABOUT:
			getSupportFragmentManager().beginTransaction().replace(R.id.settings_container, AboutFragment.newInstance())
			.commit();
			break;
		case SETTINGS_TYPE_CONTACT_US:
			getSupportFragmentManager().beginTransaction().replace(R.id.settings_container, ContactUsFragment.newInstance())
			.commit();
			break;
		case SETTINGS_TYPE_EULA:
			getSupportFragmentManager().beginTransaction().replace(R.id.settings_container, EulaFragment.newInstance())
			.commit();
			break;
		case SETTINGS_TYPE_UPDATE_DETAILS:
			getSupportFragmentManager().beginTransaction().replace(R.id.settings_container, UpdateProfileFragment.newInstance())
			.commit();
			break;
		case SETTINGS_TYPE_PROCHASES_HISTORY:
			getSupportFragmentManager().beginTransaction().replace(R.id.settings_container, HistoryListFragment.newInstance(
					HistoryListFragment.HISTORY_TYPE_PROCHASES))
			.commit();
			break;
		case SETTINGS_TYPE_CANCEL_HISTORY:
			getSupportFragmentManager().beginTransaction().replace(R.id.settings_container, HistoryListFragment.newInstance(
					HistoryListFragment.HISTORY_TYPE_CANCELED))
			.commit();
			break;
		}
	}

	
}
