/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.view.View;
import android.widget.LinearLayout;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.SamGeneralItem;

/**
 * @author natiapplications
 *
 */
public class SamGeneralItemView {
	
	private SamGeneralItem data;
	private View view;
	
	public SamGeneralItemView (SamGeneralItem data){
		this.data = data;
		buildView();
	}

	
	private void buildView() {
		
		view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.sam_type_general_item_view, null);
		LinearLayout tableContaner = (LinearLayout)view.findViewById(R.id.table_container);
		
		for (int i = 0; i < data.getItems().size(); i++) {
			RowGeneralItemView rowGeneralItemView = new RowGeneralItemView(data.getItems().get(i));
			tableContaner.addView(rowGeneralItemView.getView());
		}
		
	}
	
	public double getSamPrice (){
		return data.getSamPrice();
	}
	
	public View getView (){
		return view;
	}

}
