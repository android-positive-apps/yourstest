/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.Area;

/**
 * @author natiapplications
 *
 */
public class AreaView   {
	
	
	private Area data;
	private View view;
	private OnAreaClickListener onAreaClickListener;
	
	
	public AreaView  (Area data,OnAreaClickListener listener){
		this.data = data;
		this.onAreaClickListener = listener;
		buildView();
	}

	/**
	 * 
	 */
	private void buildView() {
		view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.area_layout, null);
		ImageView areaBg = (ImageView)view.findViewById(R.id.area_bg);
		TextView areaName = (TextView)view.findViewById(R.id.area_name);
		areaBg.setImageResource(data.getAreaColor());
		areaName.setText(data.getAreaName());
		view.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (onAreaClickListener != null){
					onAreaClickListener.onAreaClick(data, view);
				}
			}
		});
		
	}
	
	public View getView (){
		return view;
	}
	
	public interface OnAreaClickListener {
		public void onAreaClick (Area item,View view);
	}

}
