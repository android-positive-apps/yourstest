/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.SettingsActivity;
import com.positiveapps.yoursTest.YoursApp;


/**
 * @author natiapplications
 *
 */
public class ProfilView implements OnClickListener {
	
	
	
	
	public View getView() {
		View view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.profile_layout, null);
		FrameLayout  updateDetailsItem = (FrameLayout)view.findViewById(R.id.update_details_item);
		FrameLayout  prochasesHistory = (FrameLayout)view.findViewById(R.id.prochase_history_item);
		FrameLayout  cancelHistory = (FrameLayout)view.findViewById(R.id.cancel_history_item);
		FrameLayout  logoutItem = (FrameLayout)view.findViewById(R.id.logout);
		
		TextView userNameTxt = (TextView)view.findViewById(R.id.txt_user_name);
		TextView userMailTxt = (TextView)view.findViewById(R.id.txt_user_mail);
		TextView userPhoneTxt = (TextView)view.findViewById(R.id.txt_user_phone);
		TextView userAddressTxt = (TextView)view.findViewById(R.id.txt_user_address);
		TextView userTelephoneTxt = (TextView)view.findViewById(R.id.txt_user_telephone);
		TextView userMemberNumber = (TextView)view.findViewById(R.id.txt_user_number);
		TextView userZipCode = (TextView)view.findViewById(R.id.txt_user_zip_code);
		TextView userStatus = (TextView)view.findViewById(R.id.txt_user_status);
	
		
		userNameTxt.setText(YoursApp.userProfil.getUserFullName(" "));
		userMailTxt.setText(YoursApp.userProfil.getUserEmail());
		userPhoneTxt.setText(YoursApp.userProfil.getUserPhone());
		userAddressTxt.setText(YoursApp.userProfil.getUserFullAddress(" "));
		userTelephoneTxt.setText(YoursApp.userProfil.getUserTelephone());
		userMemberNumber.setText(YoursApp.userProfil.getUserNumber());
		userZipCode.setText(YoursApp.userProfil.getUserZipCode());
		userStatus.setText(YoursApp.userProfil.getUserStatusAsString());
		
		updateDetailsItem.setOnClickListener(this);
		prochasesHistory.setOnClickListener(this);
		cancelHistory.setOnClickListener(this);
		logoutItem.setOnClickListener(this);
		
		return view;
		
	}

	
	@Override
	public void onClick(View v) {
		int settingsType = 0;
		switch (v.getId()) {
		case R.id.update_details_item:
			settingsType = SettingsActivity.SETTINGS_TYPE_UPDATE_DETAILS;
			break;
		case R.id.prochase_history_item:
			settingsType = SettingsActivity.SETTINGS_TYPE_PROCHASES_HISTORY;
			break;
		case R.id.cancel_history_item:
			settingsType = SettingsActivity.SETTINGS_TYPE_CANCEL_HISTORY;
			break;
		case R.id.logout:
			YoursApp.userProfil.setIdentUser("");
			YoursApp.userProfil.setUserAddress("");
			YoursApp.userProfil.setUserCity("");
			YoursApp.userProfil.setUserEmail("");
			YoursApp.userProfil.setUserFirstNaem("");
			YoursApp.userProfil.setUserId("");
			YoursApp.userProfil.setUserLastName("");
			YoursApp.userProfil.setUserNumber("");
			YoursApp.userProfil.setUserPass("");
			YoursApp.userProfil.setUserPhone("");
			YoursApp.userProfil.setUserTelephone("");
			YoursApp.userProfil.setUserZipCode("");
			MainActivity.mainInstance.finish();
			return;
		
		}
		Intent intent = new Intent(MainActivity.mainInstance,SettingsActivity.class);
		intent.putExtra(SettingsActivity.EXTRA_TYPE, settingsType);
		MainActivity.mainInstance.startActivity(intent);
		
	}

}
