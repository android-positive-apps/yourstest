/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.ItemDate;

/**
 * @author natiapplications
 *
 */
public class LocationItemView {
	
	private ItemDate data;
	private View view;
	private OnWazeItemClickListener listener;
	
	public LocationItemView (ItemDate data,OnWazeItemClickListener listener){
		this.data = data;
		this.listener = listener;
		buildView();
	}

	
	

	/**
	 * 
	 */
	private void buildView() {
		view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.list_item_location_item, null);
		TextView city = (TextView)view.findViewById(R.id.city);
		TextView place = (TextView)view.findViewById(R.id.place);
		final ImageView waze = (ImageView)view.findViewById(R.id.waze);
		
		city.setText(data.getCity());
		place.setText(data.getPlace());
		waze.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				if (listener != null){
					listener.onWazeClickListener(data, waze);
				}
			}
		});
	
	}
	
	public View getView (){
		return view;
	}
	
	public interface OnWazeItemClickListener {
		public void onWazeClickListener (ItemDate data,View v);
	}

}
