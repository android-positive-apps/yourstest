/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.view.View;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.RowGeneralItem;

/**
 * @author natiapplications
 *
 */
public class RowGeneralItemView {
	
	
	private RowGeneralItem data;
	private View view;
	
	
	public RowGeneralItemView (RowGeneralItem data){
		this.data = data;
		buildView();
	}


	/**
	 * 
	 */
	private void buildView() {
		view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.row_general_item, null);
		
		TextView seatTxt = (TextView)view.findViewById(R.id.name);
		TextView ticketTypeTxt = (TextView)view.findViewById(R.id.ticket_type);
		TextView monthUseTxt = (TextView)view.findViewById(R.id.month_use);
		TextView priceTxt = (TextView)view.findViewById(R.id.price);
		
		
		seatTxt.setText(String.valueOf(data.getName()));
		ticketTypeTxt.setText(data.getTicketType());
		monthUseTxt.setText(String.valueOf(data.getMonth()));
		priceTxt.setText(String.valueOf((int)data.getPrice()));
		
	}
	
	public View getView (){
		return view;
	}

}
