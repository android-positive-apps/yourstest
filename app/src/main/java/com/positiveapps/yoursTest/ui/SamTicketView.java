/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.SamTicket;

/**
 * @author natiapplications
 *
 */
public class SamTicketView {
	
	private SamTicket data;
	private View view;
	
	public SamTicketView (SamTicket data){
		this.data = data;
		buildView();
	}

	
	private void buildView() {
		
		view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.sam_type_tickets_view, null);
		TextView place = (TextView)view.findViewById(R.id.place_txt);
		LinearLayout tableContaner = (LinearLayout)view.findViewById(R.id.table_container);
		
		place.setText(data.getPlace());
		for (int i = 0; i < data.getTickets().size(); i++) {
			RowTicketView rowTicketView = new RowTicketView(data.getTickets().get(i));
			tableContaner.addView(rowTicketView.getView());
		}
		
	}
	
	public double getSamPrice (){
		return data.getSamPrice();
	}
	
	public View getView (){
		return view;
	}

}
