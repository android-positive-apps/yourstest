/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.SubCategory;

/**
 * @author natiapplications
 *
 */
public class YoursItemsListHeaderView implements OnClickListener{
	
	
	private SubCategory data;
	private View itemView;
	private boolean displayShowMoreArraow;
	private onShowMoreClickListenr listener;
	
	public YoursItemsListHeaderView (SubCategory data,boolean displayShowMoreArrow,onShowMoreClickListenr listenr){
		this.data = data;
		this.displayShowMoreArraow = displayShowMoreArrow;
		this.listener = listenr;
		buildView();
	}

	/**
	 * 
	 */
	private void buildView() {
		
		itemView = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.yours_item_header_layout, null);
		ImageView bar = (ImageView)itemView.findViewById(R.id.bar);
		TextView subCategoryName = (TextView)itemView.findViewById(R.id.bar_name);
		TextView showMore = (TextView)itemView.findViewById(R.id.txt_show_more);
		
		subCategoryName.setText(data.getSubCategoryName());
		if (data.getParentType() == Category.CATEGORY_HOT_MONTH){
			subCategoryName.setText("מבצעי החודש");
		}
		if (displayShowMoreArraow){
			showMore.setVisibility(View.VISIBLE);
			showMore.setOnClickListener(this);
		}else{
			showMore.setVisibility(View.GONE);
		}
		
	}
	
	public View getItemView () {
		return itemView;
	}
	
	public interface onShowMoreClickListenr{
		public void onShowMoreClick(SubCategory subCategory,View view);
	}

	
	@Override
	public void onClick(View v) {
		
		if (listener != null){
			listener.onShowMoreClick(data, itemView);
		}
		
	}
	
	

}
