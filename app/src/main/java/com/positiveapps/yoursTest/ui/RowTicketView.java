/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.view.View;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.objects.RowTicket;

/**
 * @author natiapplications
 *
 */
public class RowTicketView {
	
	
	private RowTicket data;
	private View view;
	
	
	public RowTicketView (RowTicket data){
		this.data = data;
		buildView();
	}


	/**
	 * 
	 */
	private void buildView() {
		view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.row_ticket, null);
		TextView rowTxt = (TextView)view.findViewById(R.id.row);
		TextView seatTxt = (TextView)view.findViewById(R.id.seat);
		TextView ticketTypeTxt = (TextView)view.findViewById(R.id.ticket_type);
		TextView monthUseTxt = (TextView)view.findViewById(R.id.month_use);
		TextView priceTxt = (TextView)view.findViewById(R.id.price);
		
		rowTxt.setText(String.valueOf(data.getRow()));
		seatTxt.setText(String.valueOf(data.getSeat()));
		ticketTypeTxt.setText(data.getTicketType());
		monthUseTxt.setText(String.valueOf(data.getMonth()));
		priceTxt.setText(String.valueOf((int)data.getPrice()));
		
	}
	
	public View getView (){
		return view;
	}

}
