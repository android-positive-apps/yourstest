/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.SettingsActivity;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.util.AppUtil;

import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.TextView;

/**
 * @author natiapplications
 *
 */
public class SettingsView implements OnClickListener {
	
	
	
	
	public View getView() {
		View view = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.settings_layout, null);
		FrameLayout  contactUsItem = (FrameLayout)view.findViewById(R.id.contact_us_item);
		FrameLayout  eulaItem = (FrameLayout)view.findViewById(R.id.eula_item);
		FrameLayout  aboutItem = (FrameLayout)view.findViewById(R.id.about_item);
		TextView versionNumber = (TextView)view.findViewById(R.id.version_txt);
		
		versionNumber.setText(versionNumber.getText().toString() + " " + AppUtil.getApplicationVersion(YoursApp.appContext));
		
		contactUsItem.setOnClickListener(this);
		eulaItem.setOnClickListener(this);
		aboutItem.setOnClickListener(this);
		return view;
		
	}

	
	@Override
	public void onClick(View v) {
		int settingsType = 0;
		switch (v.getId()) {
		
		case R.id.contact_us_item:
			settingsType = SettingsActivity.SETTINGS_TYPE_CONTACT_US;
			break;
		case R.id.eula_item:
			settingsType = SettingsActivity.SETTINGS_TYPE_EULA;
			break;
		case R.id.about_item:
			settingsType = SettingsActivity.SETTINGS_TYPE_ABOUT;
			break;
		
		}
		Intent intent = new Intent(MainActivity.mainInstance,SettingsActivity.class);
		intent.putExtra(SettingsActivity.EXTRA_TYPE, settingsType);
		MainActivity.mainInstance.startActivity(intent);
		
	}

}
