/**
 * 
 */
package com.positiveapps.yoursTest.ui;

import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveapps.yoursTest.MainActivity;
import com.positiveapps.yoursTest.R;
import com.positiveapps.yoursTest.YoursApp;
import com.positiveapps.yoursTest.objects.Category;
import com.positiveapps.yoursTest.objects.YoursItem;
import com.positiveapps.yoursTest.util.BitmapUtil;
import com.positiveapps.yoursTest.util.ColorsFactory;
import com.positiveapps.yoursTest.util.DateUtil;

/**
 * @author natiapplications
 *
 */
public class YoursItemView implements OnClickListener{
	
	private YoursItem data;
	private View itemView;
	private OnYoursItemViewClickListener listener;
	
	public YoursItemView (YoursItem data,OnYoursItemViewClickListener listener){
		this.data = data;
		this.listener = listener;
		buildView();
	}

	/**
	 * 
	 */
	private void buildView() {
		
		itemView = MainActivity.mainInstance.getLayoutInflater().inflate(R.layout.list_item_yours_item, null);
		final CircleImageView itemImage = (CircleImageView)itemView.findViewById(R.id.item_image);
		FrameLayout imageFrame = (FrameLayout)itemView.findViewById(R.id.item_image_frame);
		ImageView itemDecor = (ImageView)itemView.findViewById(R.id.item_attachment);
		TextView attachmentText = (TextView)itemView.findViewById(R.id.item_attacment_text);
		TextView itemName = (TextView)itemView.findViewById(R.id.item_name);
		TextView itemLocation = (TextView)itemView.findViewById(R.id.item_location);
		TextView itemLength = (TextView)itemView.findViewById(R.id.item_length);
		TextView itemType = (TextView)itemView.findViewById(R.id.item_type);
		TextView itemPrice = (TextView)itemView.findViewById(R.id.item_original_price);
		TextView itemClubPrice = (TextView)itemView.findViewById(R.id.item_current_price);
		TextView detailsArrow = (TextView)itemView.findViewById(R.id.details_arrow);
		
		BitmapUtil.loadImageIntoByUrl(data.getImageUrl(), itemImage, R.drawable.ic_action_picture, R.drawable.ic_action_picture,
				100, 100, null);
		itemName.setText(data.getItemName().trim());
		
		int nameLines = itemName.getLineCount();
		boolean showDesc = false;
		
		
		
		
		if (data.getStatus() == YoursItem.STATUS_MOLTIPLE){
			itemLocation.setText(MainActivity.mainInstance.getString(R.string.multy_dates_and_places_txt));
			itemLength.setText(data.getItemDesc()+"");
		}else{
			
			String s = data.getItemLocation().trim();
			if (s.isEmpty()){
				s = data.getItemDesc();
				if (nameLines > 1){
					itemLocation.setMaxLines(1);
				}
				showDesc = true;
			}
			if (data.getStatus() == YoursItem.STATUS_BUNNER&&s.isEmpty()){
				s = MainActivity.mainInstance.getString(R.string.banner_desc);
			}
			itemLocation.setText(s);
			
			
			String date = DateUtil.getServerDateAsString(data.getDateLength());
			if (date.contains("0001")||date.contains("1900")||date.contains("Date")){
				if (showDesc){
					itemLength.setVisibility(View.GONE);
				}else if(!data.getItemLocation().trim().isEmpty()&&!data.getItemDesc().trim().isEmpty()){
					date = data.getItemDesc().trim();
				}else{
					date = MainActivity.mainInstance.getString(R.string.no_short_desc);
				}
			}
			/*if (date.contains("Date")){
				String d = TextUtil.getNumberFromString(date);
				long dl = Long.parseLong(d);
				Calendar c = Calendar.getInstance();
				c.setTimeInMillis(dl/10);
				date = new SimpleDateFormat("dd/MM/yyyy - hh:mm").format(c.getTime());
			}*/
			itemLength.setText(date);
			Log.e("itemlocationlog",data.getItemName() +": ==== location - " + data.getItemLocation().trim().isEmpty());
			if (data.getItemLocation().trim().isEmpty() && data.getItemDesc().trim().isEmpty() &&
					data.getDateLength().trim().isEmpty()){
				itemLength.setText(MainActivity.mainInstance.getString(R.string.no_short_desc));
			}
		}
		
		
		
		itemType.setText(data.getItemKind().trim());
		String price = (int)data.getItemOriginalPrice()+"";
		if (price.equalsIgnoreCase("0")){
			price = "...";
		}
		String clubPrice = (int)data.getItemClubPrice()+"";
		if (clubPrice.equalsIgnoreCase("0")){
			clubPrice = "...";
		}
		itemPrice.setText(price);
		itemClubPrice.setText(clubPrice);
		
		itemView.setOnClickListener(this);
		setItemColor(imageFrame,itemPrice,itemClubPrice,detailsArrow);
		
	}
	
	/**
	 * 
	 */
	private void setItemColor(FrameLayout imageFrame,TextView... bottomTexts) {
		
		int[] colors = null;
		int bgResourse = 0;
		switch (data.getParentType()) {
		case Category.CATEGORY_FAMILLY:
			colors = ColorsFactory.famillyColors;
			bgResourse = R.drawable.circle_blue;
			break;
		case Category.CATEGORY_PROCHASES:
			colors = ColorsFactory.prochaseColor;
			bgResourse = R.drawable.circle_payper;
			break;
		case Category.CATEGORY_FUN:
			colors = ColorsFactory.funColors;
			bgResourse = R.drawable.circle_cry;
			break;
		case Category.CATEGORY_HOBY:
			colors = ColorsFactory.hobyColors;
			bgResourse = R.drawable.circle_green;
			break;
		case Category.CATEGORY_HOT_MONTH:
			colors = ColorsFactory.monthColor;
			bgResourse = R.drawable.circle_orange;
			break;
		}
		imageFrame.setBackgroundResource(bgResourse);
		for (int i = 0; i < bottomTexts.length; i++) {
			bottomTexts[i].setBackgroundColor(YoursApp.appContext.getResources().getColor(colors[i]));
		}
		
	}

	
	public View getItemView () {
		return itemView;
	}
	
	public interface OnYoursItemViewClickListener{
		public void onYoursItemClick(YoursItem item,View view);
	}

	
	@Override
	public void onClick(View v) {
		if (listener != null){
			listener.onYoursItemClick(data, itemView);
		}
		
	}

}
